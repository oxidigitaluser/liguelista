<?php

if( !defined( 'ABSPATH' ) ) exit; // exit if direct access

/**
 * Class responsible for adding the fields to the posts pages
 * and to applying the filter on the query
 * and to handle ajax requests used by the plugin
 * all of the above using hooks, hence the name, Hooks.
 */
class WPAdminFiltersHooks extends WPAdminFilters
{
    function __construct()
    {
        parent::__construct();

        add_action( 'restrict_manage_posts', array( $this, 'add_posts_filters_fields' ) );

        add_filter( 'parse_query', array( $this, 'apply_filters' ), 999 );

        add_action( 'wp_ajax_wp_admin_filters_get_terms', array( $this, 'ajax_get_terms' ) );
        add_action( 'wp_ajax_wp_admin_filters_get_posts', array( $this, 'ajax_get_posts' ) );
    }

    /*============================================================================================
        Fields Display
    =============================================================================================*/

    /**
     * Gets the filters for the current page and display them
     * if any
     */
    function add_posts_filters_fields()
    {
        global $typenow;
        
        $post_type = $typenow;

        $filters = $this->get_filters_for( 'post_type', $post_type );

        if( empty( $filters ) )
            return;

        foreach( $filters as $filter )
        {
            $this->display_field( $filter );
        }

    }

    /**
     * Decides which field type to display
     * Currently not so much supported
     */
    function display_field( $filter )
    {
        switch( $filter->type )
        {
            // If taxonomy, display a select2
            case 'taxonomy':
                $this->select2_field( $filter );
            break;

            default:
                $this->text_field( $filter );
        }
    }

    /**
     * Outputs the text field for filtering
     */
    function text_field( $filter )
    {
        $url = $this->ajax_url( $filter );
        $picker_type = $this->picker_type( $filter ) ;// whether or not to init date picker for this
        $value = !empty( $_GET[ 'wp_admin_filters' ][ $filter->id ] )? $_GET[ 'wp_admin_filters' ][ $filter->id ] : '';
        require $this->dir . 'templates/fields/text_field.php';
    }

    /**
     * Outputs the select2 for filtering
     * decided by display_field above
     */
    function select2_field( $filter )
    {
        $taxonomy = $filter->type_value;

        $terms = get_terms( $taxonomy );

        if( empty( $terms ) || is_wp_error( $terms ) )
            return;

        $options = array();

        foreach( $terms as $t )
        {
            $o = new stdClass;

            $o->value = $t->slug;
            $o->label = $t->name;

            $options[] = $o;
        }

        $url = $this->ajax_url( $filter );

        $value = !empty( $_GET[ 'wp_admin_filters' ][ $filter->id ] )? $_GET[ 'wp_admin_filters' ][ $filter->id ] : '';

        require $this->dir . 'templates/fields/select2.php';
    }

    /*============================================================================================
        Apply Filter Query
    =============================================================================================*/
    function apply_filters( $q )
    {

        if( !isset( $_REQUEST[ 'wp_admin_filters' ] ) )
            return $q;

        $filters = $_REQUEST[ 'wp_admin_filters' ];

        if( !is_array( $filters ) )
            return $q;

        foreach( $filters as $key => $value )
        {
            if( empty( $value ) )
                continue;

            // Get the filter options
            $filter = $this->get_filter( $key );

            $filter_type = $filter->type;

            $filter_type_value = $filter->type_value;

            $filter_comparison = !empty( $filter->comparison ) ? $filter->comparison : null;

            $filter_comparison_type = !empty( $filter->comparison_type ) ? $filter->comparison_type : null;

            $value = utf8_decode( $value );

            switch( $filter_type )
            {
                // Custom field, alter the meta query
                case 'custom_field':

                    $meta_query = array(
                        'key' => $filter_type_value,
                        'value' => $value
                    );

                    if( !is_null( $filter_comparison ) )
                        $meta_query[ 'compare' ] = $filter_comparison;

                    if( !is_null( $filter_comparison_type ) )
                        $meta_query[ 'type' ] = $filter_comparison_type;

                    // We need to append to meta_query
                    $q->query_vars[ 'meta_query' ][] = $meta_query;
                break;

                // Taxonomy query and altering the tax_query
                case 'taxonomy':

                    $tax_query = array(
                        'taxonomy' => $filter_type_value,
                        'terms' => $value,
                        'field' => 'slug',
                        'include_children' => false
                    );

                    // The filter comparison must be set for multiple choices
                    if( !is_null( $filter_comparison ) )
                    {
                        $tax_query[ 'operator' ] = $filter_comparison;

                        if( strpos( $value, ',' ) )
                        {
                            $tax_query[ 'terms' ] = array_map( 'utf8_decode', explode( ',', $value ) );
                        }

                    }

                    $q->query_vars[ 'tax_query' ][] = $tax_query;

                break;

                // Just check for the field as is.
                case 'post_field':
                    $value = utf8_encode( $value );
                    $q->set( $filter_type_value, $value );
                break;
            }
        }

        if( empty( $_GET[ 's' ] ) )
            $q->is_search = false;

        return $q;

    }

    /*============================================================================================
        Ajax Handlers
    =============================================================================================*/
    function ajax_get_terms()
    {

        // Get filter id from request
        $filter_id = intval( $_REQUEST[ 'id' ] );

        // Validate the nonce and die if not valid
        check_ajax_referer( 'ajax-admin-filters' . $filter_id, 'wp-admin-filters', true );

        // Search term
        $q = $_REQUEST[ 'q' ];

        // Taxonomy
        $taxonomy = $_REQUEST[ 'taxonomy' ];

        // Get the terms with searching the name
        $terms = get_terms( $taxonomy, array(
            'hide_empty' => true,
            'search' => $q,
            'fields' => 'all'
        ) );

        // Return init
        $terms_options = array();

        // Prepare the terms
        foreach( $terms as $t )
        {
            $terms_options[] = array(
                'id' => $t->slug,
                'text' => $t->name
            );
        }

        // json encode the terms, echo, and die
        echo json_encode( $terms_options );
        die();

    }

    function ajax_get_posts()
    {

        // Get filter id from request
        $filter_id = intval( $_REQUEST[ 'id' ] );

        // Validate the nonce and die if not valid
        check_ajax_referer( 'ajax-admin-filters' . $filter_id, 'wp-admin-filters', true );

        // Search term
        $q = $_REQUEST[ 'term' ];

        // Get the filter options
        $filter = $this->get_filter( $filter_id );

        $filter_type = $filter->type;

        $filter_type_value = $filter->type_value;

        $filter_comparison = !empty( $filter->comparison ) ? $filter->comparison : null;

        $filter_comparison_type = !empty( $filter->comparison_type ) ? $filter->comparison_type : null;

        // The meta query to be used
        $meta_query = array(
            'key' => $filter_type_value,
            'value' => $q
        );

        if( !is_null( $filter_comparison ) )
            $meta_query[ 'compare' ] = $filter_comparison;

        if( !is_null( $filter_comparison_type ) )
            $meta_query[ 'type' ] = $filter_comparison_type;

        // Posts
        $posts = get_posts( array(
            'post_type' => $filter->display_on,
            'meta_query' => array(
                $meta_query
            )
        ) );

        $posts_options = array();

        foreach( $posts as $p )
        {
            $posts_options[] = array(
                'label' => $p->post_title,
                'id' => get_edit_post_link( $p->ID, '' ),
                'value' => $p->post_title
            );
        }

        // JSON Encode and good bye
        echo json_encode( $posts_options );
        die();

    }
}

?>