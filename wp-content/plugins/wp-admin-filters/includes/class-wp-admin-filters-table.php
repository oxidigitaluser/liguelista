<?php

if( !defined( 'ABSPATH' ) ) exit; // exit if direct access

/*===================================================================================================
Thanks to http://plugins.svn.wordpress.org/custom-list-table-example/tags/1.3/list-table-example.php
=====================================================================================================*/

class WPAdminFiltersTable extends WP_List_Table
{
	function __construct(){
        
        global $status, $page;
                
        // Set parent defaults
        parent::__construct( array(
            'singular'  => 'Admin Filter',     //singular name of the listed records
            'plural'    => 'Admin Filters',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );

    }

    function column_default( $item, $column_name )
    {
        return $item->{ $column_name };
    }

    function column_label( $item )
    {
        global $wp_admin_filters;

        $actions = array(
            'edit'      => sprintf( '<a href="%s&filter=%s">Edit</a>', $wp_admin_filters->page_url( 'edit' ), $item->id ),
            'delete'    => sprintf( '<a href="%s&filter=%s" class="delete_filter">Delete</a>', $wp_admin_filters->page_url( 'delete' ), $item->id ),
        );

        //Return the title contents
        return sprintf('%1$s <span style="color:silver">(id:%2$s)</span>%3$s',
            /*$1%s*/ $item->label,
            /*$2%s*/ $item->id,
            /*$3%s*/ $this->row_actions( $actions )
        );
    }

    function get_columns()
    {
        return array
        (
            'label' => 'Filter Label',
            'display_on' => 'Displayed on',
            'type' => 'Type'
        );
    }

    function prepare_items()
    {
        global $wpdb, $wp_admin_filters;

        $per_page = 20;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();

        $this->_column_headers = array( $columns, $hidden, $sortable );

        $current_page = $this->get_pagenum();

        $this->items = $wp_admin_filters->get_filters( $current_page, $per_page );

        $total_items = $wp_admin_filters->total_filters_count();

        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil( $total_items/$per_page )   //WE have to calculate the total number of pages
        ) );
    }
}

?>