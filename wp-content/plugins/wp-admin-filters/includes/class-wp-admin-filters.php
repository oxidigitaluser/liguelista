<?php

if( !defined( 'ABSPATH' ) ) exit; // exit if direct access

class WPAdminFilters
{

    public $dir;
    public $url;

    private $taxonomies;
    private $taxonomies_terms;

    protected $screen;

    function __construct()
    {
        global $wpdb;

        $this->dir = plugin_dir_path( dirname( __FILE__ ) );
        $this->url = plugin_dir_url( dirname( __FILE__ ) );
        $this->table = $wpdb->prefix . 'wp_admin_filters';
        $this->taxonomies = array();
        $this->taxonomies_terms = array();

        add_action( 'admin_init', array( $this, 'load_wp_list_table' ) );

        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );

        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_front' ) );

    }

    /*==========================================
        Plugin Functions
    ===========================================*/

    function enqueue( $hook )
    {
        if( !$this->load_assets() )
            return;

        // CSS
        wp_enqueue_style( 'wp-admin-filters', $this->url . 'assets/css/wp-admin-filters.css' );

        // Javascript
        wp_enqueue_script( 'wp-admin-filters-jquery-validation', $this->url . 'assets/js/jquery.validate.min.js', array( 'jquery' ), 1.3, false );

        // Required Data
        $taxonomies = $this->get_taxonomies();
        $taxonomies_terms = $this->get_taxonomies_terms();
        $post_fields = $this->get_post_fields();
        $comparisons = $this->get_comparisons();
        $comparisons_types = $this->get_comparisons_types();

        wp_register_script( 'wp-admin-filters', $this->url . 'assets/js/wp-admin-filters.js', array( 'jquery', 'wp-admin-filters-jquery-validation' ), 1, true );
        wp_localize_script( 'wp-admin-filters', 'wp_admin_filters', array(
            'taxonomies' => $taxonomies,
            'taxonomies_terms' => $taxonomies_terms,
            'post_fields' => $post_fields,
            'comparisons' => $comparisons,
            'comparisons_types' => $comparisons_types
        ) );
        wp_enqueue_script( 'wp-admin-filters' );

    }

    function enqueue_front( $hook )
    {
        if( !$this->load_assets_front() )
            return;

        // Select2 Styles and Scripts
        wp_enqueue_style( 'wp-admin-filters-select2', $this->url . 'assets/css/select2.css' );
        wp_enqueue_script( 'wp-admin-filters-select2', $this->url . 'assets/js/select2.min.js', array( 'jquery' ), 3.52, true );

        // Date time picker
        wp_enqueue_style( 'wp-admin-filters-datetime-picker', $this->url . 'assets/css/jquery.datetimepicker.css' );
        wp_enqueue_script( 'wp-admin-filters-datetime-picker', $this->url . 'assets/js/jquery.datetimepicker.js', array( 'jquery' ), 1, true );

        // Plugin front style
        wp_enqueue_style( 'wp-admin-filters-front', $this->url . 'assets/js/wp-admin-filters-front.css' );

        // Plugin js
        wp_enqueue_script( 'wp-admin-filters-front', $this->url . 'assets/js/wp-admin-filters-front.js', array( 'jquery' ), 1, true );
        wp_enqueue_script( 'jquery-ui-autocomplete' );
    }

    function load_wp_list_table()
    {
        // Make sure WP_List_Table class is loaded
        if( !class_exists( 'WP_List_Table' ) )
        {
            require_once  $this->dir . 'vendor/class-wp-list-table.php';
        }
    }

    /*==========================================
        Database Functions
    ===========================================*/
    function get_filters( $page = 1, $limit = 10 )
    {
        global $wpdb;

        $offset = ( $page - 1 ) * $limit;

        $results = $wpdb->get_results( 'SELECT * FROM ' . $this->table . ' LIMIT ' . $offset . ',' . $limit );

        return $results;
    }

    function save_filter( $data )
    {
        global $wpdb;

        $wpdb->replace(
            $this->table,
            $data
        );
    }

    function delete_filter( $id )
    {
        global $wpdb;

        $wpdb->delete(
            $this->table,
            array( 'id' => $id )
        );
    }

    function get_filter( $id )
    {
        global $wpdb;

        return $wpdb->get_row( 'SELECT * FROM ' . $this->table . ' WHERE id=' . $id );
    }

    function get_filters_for( $object_type = 'post_type', $display_on = 'post' )
    {
        global $wpdb;

        return $wpdb->get_results( 'SELECT * FROM ' . $this->table . ' WHERE object_type LIKE "' . $object_type . '" AND display_on LIKE "' . $display_on . '"'  );
    }

    function total_filters_count()
    {
        global $wpdb;

        $count = $wpdb->get_var( 'SELECT COUNT(id) FROM ' . $this->table );

        return $count;
    }

    /*==========================================
        Helper Functions
    ===========================================*/

    function get_post_fields()
    {
        return array(
            'p' => 'Post ID',
            'author_name' => 'Author',
            's' => 'Post Title and Content',
            'comment_status' => 'Comment Status',
            'name' => 'Slug',
            'guid' => 'GUID'
        );
    }

    function get_comparisons()
    {
        return array(
            'custom_field' =>  array(
                '=', '!=', '>', '>=', '<', '<=', 'LIKE', 'NOT LIKE', 'IN', 'NOT IN', 'BETWEEN', 'NOT BETWEEN', 'EXISTS',
                'NOT EXISTS'
            ),
            'taxonomy' => array(
                'IN', 'NOT IN', 'AND'
            )
        );
    }

    function get_comparisons_types()
    {
        return array(
            'custom_field' =>  array(
                'NUMERIC', 'BINARY', 'CHAR', 'DATE', 'DATETIME', 'DECIMAL', 'SIGNED', 'TIME', 'UNSIGNED'
            )
        );
    }

    function get_taxonomies()
    {
        if( !empty( $this->taxonomies ) )
            return $this->taxonomies;

        $taxes = get_taxonomies( null, 'objects' );

        foreach( $taxes as $key => $t )
        {
            $this->taxonomies[ $key ][ 'label' ] = $t->labels->name;
            $this->taxonomies[ $key ][ 'post_type' ] = $t->object_type;
        }

        return $this->taxonomies;
    }

    function get_taxonomies_terms()
    {
        if( !empty( $this->taxonomies_terms ) )
            return $this->taxonomies_terms;

        if( empty( $this->taxonomies ) )
            $this->get_taxonomies();

        foreach( $this->taxonomies as $key => $data )
        {
            $terms = get_terms( $key, array( 'hide_empty' => false, 'fields' => 'id=>slug' ) );

            $this->taxonomies_terms[ $key ] = $terms;
        }

        return $this->taxonomies_terms;
    }

    function get_post_types_options()
    {
        $post_types = get_post_types( null, 'objects' );

        $options = array();

        foreach( $post_types as $key => $p )
        {
            $options[ $key ] = $p->labels->name;
        }

        return $options;
    }

    function page_url( $action )
    {

        $actions = array( 'add', 'edit', 'create', 'delete', 'update' );

        if( !in_array( $action, $actions ) )
        {
            $action = 'list';
        }

        $url = admin_url( 'admin.php' );
        $url = add_query_arg( array(
            'page' => 'wp-admin-filters',
            'action' => $action
        ), $url );

        return $url;
    }

    function ajax_url( $filter )
    {

        if( !$filter->ajax )
            return '';
        
        // Generate base url (basically an admin ajax request)
        $base_url = admin_url( 'admin-ajax.php' );

        // Which type of data are we looking for? (for now, only terms)
        $data_type = ( $filter->type == 'taxonomy' ) ? 'terms' : 'posts' ;

        // type key to be used in query args
        $type_key = ( $filter->type == 'taxonomy' ) ? 'taxonomy' : 'custom_field';

        // generate url
        $url = add_query_arg( array(
            $type_key => $filter->type_value,
            'id' => $filter->id,
            'action' => 'wp_admin_filters_get_' . $data_type
        ), $base_url );

        // Security nonce
        $url = wp_nonce_url( $url, 'ajax-admin-filters' . $filter->id, 'wp-admin-filters' );

        // Return
        return $url;

    }

    function picker_type( $filter )
    {
        $picker_type = 'none';

        if( $filter->comparison_type == 'DATETIME' )
        {
            $picker_type = 'datetime';
        }

        if( $filter->comparison_type == 'DATE' )
        {
            $picker_type = 'date';
        }

        return $picker_type;
    }

    function before_content( $title = null )
    {
        require_once $this->dir . 'templates/before_content.php';
    }

    function after_content()
    {
        require_once $this->dir . 'templates/after_content.php';
    }

    function load_assets()
    {
        $screen = get_current_screen();
        
        return $this->screen == $screen->id;
    }

    function load_assets_front()
    {
        global $typenow, $wpdb;

        $filters_count = $wpdb->get_var( 'SELECT COUNT(id) FROM ' . $this->table . ' WHERE display_on="' . $typenow . '" AND object_type="post_type"' );
        
        return $filters_count != 0;
    }

    function log( $var )
    {
        $data = '[ ' . date( 'Y-m-d H:i:s' ) . ' ] ';
        $data .= var_export( $var, true );
        $data .= PHP_EOL;
        
        file_put_contents( $this->dir . 'logs.txt', $data, FILE_APPEND );
    }

    /*==========================================
        Static Functions
    ===========================================*/
    static function installation()
    {
        global $wpdb;

        $table = $wpdb->prefix . 'wp_admin_filters';

        $charset_collate = $wpdb->get_charset_collate();

        $table_query = "CREATE TABLE IF NOT EXISTS `$table` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `display_on` varchar(255) NOT NULL COMMENT 'post type to display on',
                      `type` varchar(255) NOT NULL COMMENT 'taxonomy custom_field post_field',
                      `type_value` varchar(255),
                      `object_type` varchar(255),
                      `comparison` varchar(255),
                      `comparison_type` varchar(255),
                      `label` varchar(255) NOT NULL,
                      `ajax` boolean NOT NULL,
                      `multiple` boolean NOT NULL,
                      PRIMARY KEY (`id`)
                    ) $charset_collate AUTO_INCREMENT=1;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        dbDelta( $table_query );
    }

}

?>