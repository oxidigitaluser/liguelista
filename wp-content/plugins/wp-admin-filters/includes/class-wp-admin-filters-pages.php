<?php

if( !defined( 'ABSPATH' ) ) exit; // exit if direct access

/**
 * This class is responsible for the admin pages of the plugin
 * it contains a router function where you should start debugging
 * it handles adding, editing, and deleting filters.
 * the database functions are available in the parent class though
 * maybe I will move them to a separate class later.
 */
class WPAdminFiltersPages extends WPAdminFilters
{
    private $types;
    private $gump;

    function __construct()
    {
        parent::__construct();

        $this->types = array(
            'taxonomy' => 'Taxonomy',
            'custom_field' => 'Custom Field',
            'post_field' => 'Post Field'
        );

        $this->gump = null;

        add_action( 'admin_menu', array( $this, 'menu' ) );

    }

    function menu()
    {
        $this->screen = add_menu_page( 'WP Admin Filters', 'Admin Filters', 'manage_options', 'wp-admin-filters', array( $this, 'router' ) );
    }

    function router()
    {
        if( isset( $_REQUEST[ 'action' ] ) )
        {
            switch( $_REQUEST[ 'action' ] )
            {
                case 'edit':
                    $this->edit_page();
                break;
                case 'add':
                    $this->add_page();
                break;
                case 'create':
                    $this->create_filter();
                    break;
                case 'delete':
                    $this->remove_filter();
                break;
                case 'update':
                    $this->update_filter();
                break;
                default:
                    $this->list_page();
                break;
            }
        }
        else
        {
            $this->list_page();
        }
    }

    function list_page()
    {
        require_once $this->dir . 'includes/class-wp-admin-filters-table.php';

        $filters_table = new WPAdminFiltersTable;

        $filters_table->prepare_items();

        require_once $this->dir . 'templates/list.php';
    }

    function add_page( $errors = null )
    {
        $post_types = $this->get_post_types_options();

        $this->before_content( 'Add New Filter' );
        require_once $this->dir . 'templates/add.php';
        $this->after_content();
    }

    function edit_page( $errors = null )
    {

        $post_types = $this->get_post_types_options();

        $obj = $this->get_filter( intval( $_REQUEST[ 'filter' ] ) );

        if( empty( $obj ) )
        {
            $this->list_page();
            return;
        }

        $this->before_content( 'Edit filter' );
        require_once $this->dir . 'templates/edit.php';
        $this->after_content();
    }

    function create_filter()
    {
        $is_valid = $this->validate();

        $nonce = wp_verify_nonce( $_REQUEST[ 'wp_admin_filter' ], 'create-wp-admin-filter' );

        $errors = false;

        if( !$nonce )
        {
            $errors = 'Timeout occurred.';
        }

        if( !$is_valid )
        {
            $errors = $this->gump->get_readable_errors( true );
        }

        if( !empty( $errors ) )
        {
            $this->add_page( $errors );
            return;
        }

        $data = array(
            'display_on' => $_POST[ 'display_on' ],
            'type' => $_POST[ 'filter_type' ],
            'type_value' => $_POST[ 'filter_type_value' ],
            'label' => $_POST[ 'filter_label' ],
            'object_type' => 'post_type',
            'comparison' => $_POST[ 'filter_comparison' ],
            'comparison_type' => $_POST[ 'filter_comparison_type' ],
            'multiple' => !empty( $_POST[ 'filter_multiple' ] ),
            'ajax' => !empty( $_POST[ 'filter_ajax' ] )
        );

        $this->save_filter( $data );

        $this->list_page();

    }

    function update_filter()
    {
        $is_valid = $this->validate( true );

        $nonce = wp_verify_nonce( $_REQUEST[ 'wp_admin_filter' ], 'edit-wp-admin-filter' );

        $errors = false;

        if( !$nonce )
        {
            $errors = 'Timeout occurred.';
        }

        if( !$is_valid )
        {
            $errors = $this->gump->get_readable_errors( true );
        }

        if( !empty( $errors ) )
        {
            $this->edit_page( $errors );
            return;
        }

        $data = array(
            'display_on' => $_POST[ 'display_on' ],
            'type' => $_POST[ 'filter_type' ],
            'type_value' => $_POST[ 'filter_type_value' ],
            'label' => $_POST[ 'filter_label' ],
            'object_type' => 'post_type',
            'comparison' => $_POST[ 'filter_comparison' ],
            'comparison_type' => $_POST[ 'filter_comparison_type' ],
            'ajax' => !empty( $_POST[ 'filter_ajax' ] ),
            'multiple' => !empty( $_POST[ 'filter_multiple' ] ),
            'id' => $_POST[ 'id' ]
        );

        $this->save_filter( $data );

        $this->list_page();
    }

    function remove_filter()
    {
        if( isset( $_REQUEST[ 'filter' ] ) && is_numeric( $_REQUEST[ 'filter' ] ) )
        {
            $this->delete_filter( $_REQUEST[ 'filter' ] );
        }

        $this->list_page();
    }

    function validate( $id = false )
    {

        require_once $this->dir . 'vendor/gump.class.php';
        
        $rules = array(
            'filter_label' => 'required|max_len,100|min_len,3',
            'display_on' => 'required',
            'filter_type' => 'required',
            'filter_type_value' => 'required',
            'filter_comparison' => 'required'
        );

        if( $id )
        {
            $rules[ 'id' ] = 'numeric';
        }

        if( is_null( $this->gump ) )
            $this->gump = new GUMP;

        $is_valid = $this->gump->validate( $_POST, $rules );

        return $is_valid === true;
    }
}

?>