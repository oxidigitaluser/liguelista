<?php if( !defined( 'ABSPATH' ) ) exit; // exit if direct access ?>

<?php if( !is_null( $errors ) ): ?>
    <div class="error">
        <p>Validation Error Occurred. Please fix them and try again.</p>
        <p><?php echo $errors; ?></p>
    </div>
<?php endif; ?>
<form action="<?php echo $this->page_url( 'create' ); ?>" method="POST" class="wp_admin_filter">
    <?php wp_nonce_field( 'create-wp-admin-filter', 'wp_admin_filter', null, true ); ?>
    
    <label class="wp_admin_filter" for="filter_label">Filter Label</label>
    <input type="text" name="filter_label" size="30" value="" id="filter_label" placeholder="Filter Label" autocomplete="off">

    <label class="wp_admin_filter" for="display_on">Display On</label>
    <select name="display_on" id="display_on">
        <option value="">Select Post Type</option>
        <?php foreach( $post_types as $key => $label ): ?>
            <option value="<?php echo $key; ?>"><?php echo $label; ?></option>
        <?php endforeach; ?>
    </select>

    <label class="wp_admin_filter" for="filter_type">Filter Type</label>
    <select name="filter_type" id="filter_type">
        <option value="">Select a type</option>
        <?php foreach( $this->types as $key => $label ): ?>
            <option value="<?php echo $key; ?>"><?php echo $label; ?></option>
        <?php endforeach; ?>
    </select>
    
    <label class="wp_admin_filter" for="filter_type_value">Filter Type Value</label>
    <input type="text" name="filter_type_value" id="filter_type_value" />

    <label class="wp_admin_filter" for="filter_comparison">Comparison</label>
    <input type="text" name="filter_comparison" id="filter_comparison" placeholder="Comparison" />

    <label class="wp_admin_filter" for="filter_comparison_type">Comparison Type</label>
    <input type="text" name="filter_comparison_type" id="filter_comparison_type" placeholder="Comparison Type" />

    <label class="wp_admin_filter box taxonomy custom_field dependent" for="filter_ajax">
        <input type="checkbox" name="filter_ajax" id="filter_ajax" />
        Ajax? <span class="description">When you have a lot of terms and need not to load them at once, but only what you need.</span>
    </label>

    <label class="wp_admin_filter box taxonomy dependent" for="filter_multiple">
        <input type="checkbox" name="filter_multiple" id="filter_multiple" />
        Multi? <span class="description">You can filter by more than one term per request when this is checked.</span>
    </label>
    
    <input type="submit" class="button button-primary wp_admin_filter" value="Save" />

</form>