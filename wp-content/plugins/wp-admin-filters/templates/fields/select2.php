<?php if( !defined( 'ABSPATH' ) ) exit; // exit if direct access ?>
<?php if( !$filter->ajax ): ?>
    <select <?php echo !empty( $filter->multiple )? 'multiple="multiple"' : ''; ?> name="wp_admin_filters[<?php echo $filter->id; ?>]" class="wp_admin_filters_select" data-placeholder="<?php echo $filter->label; ?>">
        <option></option>
        <?php 
            if( $options ):
                foreach( $options as $o ):
                    ?>
                        <option value="<?php echo $o->value; ?>" <?php selected( $o->value, $value, true ); ?>><?php echo $o->label; ?></option>
                    <?php
                endforeach;
            endif;
        ?>
    </select>
<?php else: ?>
    <input <?php echo !empty( $filter->multiple )? 'multiple="multiple"' : ''; ?> type="hidden" name="wp_admin_filters[<?php echo $filter->id; ?>]" class="wp_admin_filters_select" data-placeholder="<?php echo $filter->label; ?>" data-url="<?php echo $url; ?>" value="<?php echo $value; ?>" />
<?php endif; ?>