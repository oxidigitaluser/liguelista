<?php if( !defined( 'ABSPATH' ) ) exit; // exit if direct access ?>
<div class="wrap">
    
    <h2 class="title">
        <span class="wp_admin_filters">Admin Filters List</span>
        <a href="<?php echo $this->page_url( 'add' ); ?>" class="add-new-h2">Add Filter</a>
    </h2>
    
    <div>
        <?php $filters_table->display(); ?>
    </div>
    
</div>