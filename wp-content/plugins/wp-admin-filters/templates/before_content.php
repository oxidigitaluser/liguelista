<?php if( !defined( 'ABSPATH' ) ) exit; // exit if direct access ?>
<div class="wrap">
    
    <?php if( !empty( $title ) ): ?>
        <h2 class="title"><?php echo $title; ?></h2>
    <?php endif; ?>
    
    <div id="poststuff">
        <div id="post-body" class="metabox-holder columns-2">
            <!-- Sidebar container -->
            <div id="postbox-container-1" class="postbox-container">
                <!-- Box -->
                <div class="postbox">
                    <h3>
                        <span>Help & Support</span>
                    </h3>
                    <div class="inside">
                        <p>
                            For any help and assistance please send an email to <a href="mailto:contact@galalaly.me?subject=WFTT">contact@galalaly.me</a>. Please allow up to 7 days (or whatever is mentioned in the confirmation email).
                        </p>
                    </div>
                </div>
                <!-- End box -->
                <!-- Box -->
                <div class="postbox">
                    <h3>
                        <span>Other Plugins</span>
                    </h3>
                    <div class="inside">
                        <p>
                            If you like this plugin, you might like the following plugins as well
                        </p>
                        <ul>
                            <li>
                                <a href="http://codecanyon.net/item/sorting-woocommerce-pro/5771321" target="_blank">
                                    Sorting WooCommerce Pro
                                </a>
                            </li>
                            <li>
                                <a href="http://codecanyon.net/item/woocommerce-custom-taxonomy-reports/8210455" target="_blank">
                                    Custom Taxonomy Reports
                                </a>
                            </li>
                            <li>
                                <a href="https://wordpress.org/plugins/woocommerce-custom-currencies/" target="_blank">
                                    Custom Currencies
                                </a>
                            </li>
                            <li>
                                <a href="http://codecanyon.net/item/woocommerce-orders-progress-bars/7292949" target="_blank">
                                    Orders Progress Bars
                                </a>
                            </li>
                            <li>
                                <a href="http://store.galalaly.me/product/woocommerce-internal-order-status/" target="_blank">
                                    Internal Order Status
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- End box -->
            </div>
            <!-- End sidebar -->
            <!-- Main Content -->
            <div id="postbox-container-2" class="postbox-container">
                <div class="postbox">
                    <div class="inside">