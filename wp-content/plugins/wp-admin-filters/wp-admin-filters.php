<?php

/*
Plugin Name: Wordpress Admin Filters
Description: Provides customizable admin filters for post types.
Plugin URI: http://www.galalaly.me
Author: Galal Aly
Author URI: http://www.galalaly.me
Version: 1.0
*/

/*

    Copyright (C) 2014  Galal Aly  contact@galalaly.me

*/


if( !defined( 'ABSPATH' ) ) exit; // exit if direct access

$wp_admin_filters_path = plugin_dir_path( __FILE__ );

// Main class for all files
require_once  $wp_admin_filters_path . 'includes/class-wp-admin-filters.php';

// Pages
require_once  $wp_admin_filters_path . 'includes/class-wp-admin-filters-pages.php';
// Hooks
require_once  $wp_admin_filters_path . 'includes/class-wp-admin-filters-hooks.php';

// Create database table on installation
register_activation_hook( __FILE__, array( 'WPAdminFilters', 'installation' ) );

// Create global vars
$wp_admin_filters = new WPAdminFilters;
$wp_admin_filters_pages = new WPAdminFiltersPages;
$wp_admin_filters_hooks = new WPAdminFiltersHooks;

?>