
/*------------------------------------------------------------------------------------------------
    TYPE VALUE FUNCTIONS
-------------------------------------------------------------------------------------------------*/
function type_value_textbox()
{
    html = '<input type="text" name="filter_type_value" id="filter_type_value" />';
    jQuery( '#filter_type_value' ).replaceWith( html );
}

function type_value_taxonomy_selectbox()
{
    html = '<select name="filter_type_value" id="filter_type_value">';
    html += '<option value="">Select a taxonomy</option>';

    post_type = jQuery( '#display_on' ).val();

    for( var key in wp_admin_filters.taxonomies )
    {
        if( wp_admin_filters.taxonomies[ key ][ 'post_type' ].indexOf( post_type ) !== -1 )
            html += '<option value="' + key + '">' + wp_admin_filters.taxonomies[ key ][ 'label' ] + '</option>';
    }

    html += '</select>';

    jQuery( '#filter_type_value' ).replaceWith( html );
}

function type_value_post_field_selectbox()
{
    html = '<select name="filter_type_value" id="filter_type_value">';
    html += '<option value="">Select a post field</option>';

    for( var key in wp_admin_filters.post_fields )
    {
        html += '<option value="' + key + '">' + wp_admin_filters.post_fields[ key ] + '</option>';
    }

    html += '</select>';

    jQuery( '#filter_type_value' ).replaceWith( html );
}

/*------------------------------------------------------------------------------------------------
    COMPARISON FUNCTIONS
-------------------------------------------------------------------------------------------------*/

function filter_comparison_custom_fields_selectbox()
{
    html = '<select name="filter_comparison" id="filter_comparison">';
    html += '<option value="">Select a comparison</option>';

    for( var key in wp_admin_filters.comparisons.custom_field )
    {
        html += '<option value="' + wp_admin_filters.comparisons.custom_field[ key ] + '">' + wp_admin_filters.comparisons.custom_field[ key ] + '</option>';
    }

    html += '</select>';

    jQuery( '#filter_comparison' ).replaceWith( html );
}

function filter_comparison_taxonomy_selectbox()
{
    html = '<select name="filter_comparison" id="filter_comparison">';
    html += '<option value="">Select a comparison</option>';

    for( var key in wp_admin_filters.comparisons.taxonomy )
    {
        html += '<option value="' + wp_admin_filters.comparisons.taxonomy[ key ] + '">' + wp_admin_filters.comparisons.taxonomy[ key ] + '</option>';
    }

    html += '</select>';

    jQuery( '#filter_comparison' ).replaceWith( html );
}

function filter_comparison_textbox()
{
    html = '<input type="text" name="filter_comparison" id="filter_comparison" />';
    jQuery( '#filter_comparison' ).replaceWith( html );
}

/*------------------------------------------------------------------------------------------------
    COMPARISON TYPES FUNCTIONS
-------------------------------------------------------------------------------------------------*/

function filter_comparison_type_custom_fields_selectbox()
{
    html = '<select name="filter_comparison_type" id="filter_comparison_type">';
    html += '<option value="">Select a comparison type</option>';

    for( var key in wp_admin_filters.comparisons_types.custom_field )
    {
        html += '<option value="' + wp_admin_filters.comparisons_types.custom_field[ key ] + '">' + wp_admin_filters.comparisons_types.custom_field[ key ] + '</option>';
    }

    html += '</select>';

    jQuery( '#filter_comparison_type' ).replaceWith( html );
}

function filter_comparison_type_textbox()
{
    html = '<input type="text" placeholder="Ignored for this type" name="filter_comparison_type" id="filter_comparison_type" />';

    jQuery( '#filter_comparison_type' ).replaceWith( html );
}


jQuery( document ).ready( function(){

    // When the filter type changes, change the value to text or select boxes
    jQuery( 'body' ).on( 'change', '#filter_type, #display_on', function() {

        val = jQuery( '#filter_type' ).val();
        
        switch( val )
        {
            case "taxonomy":
                type_value_taxonomy_selectbox();
                filter_comparison_taxonomy_selectbox();
                filter_comparison_type_textbox();
            break;
            case "custom_field":
                type_value_textbox();
                filter_comparison_custom_fields_selectbox();
                filter_comparison_type_custom_fields_selectbox();
            break
            case "post_field":
                type_value_post_field_selectbox();
                filter_comparison_textbox();
                filter_comparison_type_textbox();
            break;
            default:
                type_value_textbox();
                filter_comparison_textbox();
                filter_comparison_type_textbox();
            break;
        }

        jQuery( '.dependent' ).hide();
        jQuery( '.' + val ).show();

    } );

    // Basic Form validation
    jQuery( 'form.wp_admin_filter' ).validate({
        rules: {
            filter_label: 'required',
            display_on: 'required',
            filter_type: 'required',
            filter_type_value: 'required'
        }
    });

    // Trigger change
    jQuery( '#filter_type' ).trigger( 'change' );

    // When editing, set the correct values in selectboxes
    jQuery( '#filter_type_value' ).val( jQuery( '#filter_type_value_edit' ). val() );
    jQuery( '#filter_comparison' ).val( jQuery( '#filter_comparison_edit' ). val() );
    jQuery( '#filter_comparison_type' ).val( jQuery( '#filter_comparison_type_edit' ). val() );

    // When the admin is deleting a filter, confirm first!
    jQuery( '.delete_filter' ).on( 'click', function( event ) {
        // event.preventDefault();
        confirm_result = confirm( 'Are you sure you want to delete this filter?' );
        return confirm_result;
    } );

} );