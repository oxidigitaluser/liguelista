jQuery( document ).ready( function() {

    // Turn selectboxes to select2
    jQuery( '.wp_admin_filters_select' ).each( function( index ) {
        
        // Will be used to initialize the select2
        init_args = {
            width: '150px'
        };

        // Allow Clear option
        init_args.allowClear = true;

        // Minimum input, also for ajax not to start auto
        init_args.minimumInputLength = 2;

        // Multiple?
        init_args.multiple = ( typeof jQuery( this ).attr( 'multiple' ) != 'undefined' ) && jQuery( this ).attr( 'multiple' ) == 'multiple';

        init_args.initSelection = function( element, callback ) {

            // Original Value(s)
            values = jQuery( element ).val();

            if( typeof values != 'undefined' && values.length > 0 )
            {
                values_arr = values.split( ',' );

                var data = [];
                
                for( index in values_arr )
                {
                    // multiple
                    multiple = jQuery( element ).attr( 'multiple' );

                    data.push( { 'id': values_arr[ index ], 'text' : values_arr[ index ] } );

                    if( typeof multiple == 'undefined' || multiple != 'multiple' )
                    {
                        callback( data[ 0 ] );
                        return;
                    }
                }
            }

            callback( data );
        };

        // Ajax?
        ajax_url = jQuery( this ).data( 'url' );

        // If it's ajax? Select2! http://select2.github.io/select2/
        if( !( typeof ajax_url == 'undefined' ) && ajax_url.length > 0 )
        {
            init_args.ajax = {
                url: ajax_url,
                dataType: 'json',
                quietMillis: 200,
                cache: true,
                data: function( term, page )
                {
                    return { q: term }
                },
                results: function( data, page )
                {
                    return { results: data }
                }
            };
        }

        // Initialize the select2 instance for this obj
        jQuery( this ).select2( init_args );

    } );

    // Autocomplete fields should be autocompleted
    jQuery( '.wp_admin_filters_autocomplete' ).each( function( index ) {

        if( typeof jQuery( this ).data( 'url' ) == 'undefined' || jQuery( this ).data( 'url' ).length <= 0 )
            return;

        init_args = {
            source: jQuery( this ).data( 'url' ),
            minLength: 2,
            select: function(event, ui) {
                var url = ui.item.id;
                if(url != '') {
                    location.href = url;
                }
            }
        };

        jQuery( this ).autocomplete( init_args );

    } );

    // Date or datetime picker?
    jQuery( '.wp_admin_filters_autocomplete' ).each( function( index ) {

        if( typeof jQuery( this ).data( 'picker' ) == 'undefined' || jQuery( this ).data( 'picker' ) == 'none' )
            return;

        format_suffix = ( jQuery( this ).data( 'picker' ) != 'date' ) ? ' H:i:s' : '';

        init_args = {
            format: 'Y-m-d' + format_suffix,
            timepicker: ( jQuery( this ).data( 'picker' ) != 'date' )
        };

        jQuery( this ).datetimepicker( init_args );

    } );

} );