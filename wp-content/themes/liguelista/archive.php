<?php
/**
 * The template for displaying Archive pages
 *
 * @package WordPress
 * @subpackage Javo_Directory
 * @since Javo Themes 1.0
 */
global
$javo_tso_map
, $javo_tso_archive
, $javo_tso
, $wp_query
, $javo_this_terms_object;
$javo_this_terms_object = $wp_query->queried_object;
$javo_this_term = $javo_this_terms_object->term_id;
$javo_get_sub_terms_args = Array(
    'hide_empty' => 0
    , 'parent' => $javo_this_term
);

$javo_get_sub_terms = get_terms($javo_this_terms_object->taxonomy, $javo_get_sub_terms_args);

query_posts(array(
    'item_category' => $wp_query->query_vars['item_category']
    , 'posts_per_page' => $wp_query->query_vars['posts_per_page']
    , 'paged' => $wp_query->query_vars['paged']
    , 'order' => 'ASC'
    , 'orderby' => 'meta_value title'
    , 'meta_key' => 'plano_comercial'));

get_header();
?>

<!-- Map Area -->
<div class="javo-archive-header-container">
    <div class="javo-archive-header-map"></div>
    <div class="javo-archive-header-search-bar">
        <?php echo do_shortcode('[javo_search_form]'); ?>
    </div>
</div>

<!-- Main Container -->
<div class="container">
    <div class="col-md-9 main-content-wrap">
        <script type="text/template" id="javo-archive-ajax-result-header">
            <h2 class="page-header margin-top-12"><?php _e('Search Result', 'javo_fr'); ?></h2>
        </script>
        <div class="javo-output padding-top-10 javo-archive-list-wrap">
            <h1 class="page-header margin-top-12">
                <?php printf('%s <small>%s</small>', strtoupper($javo_this_terms_object->name), __('Archive', 'javo_fr')); ?>
                <!--i>
                        <a href="<?php echo home_url(); ?>"><?php _e('HOME', 'javo_fr'); ?></a>
                <?php
                $javo_archive_current = javo_get_archive_current_position($javo_this_term, $javo_this_terms_object->taxonomy);
                foreach ($javo_archive_current as $term_id) {
                    $term = get_term($term_id, $javo_this_terms_object->taxonomy);
                    printf('&gt; <a href="%s">%s</a> '
                            , get_term_link($term)
                            , strtoupper($term->name)
                    );
                };
                ?>
                </i-->
            </h1><!-- /.page-header -->

            <?php if ($javo_tso_archive->get('sub_cat_enable', 'on') == 'on'): ?>
                <!-- Archive Sub Categories -->
                <div class="javo-archive-sub-category">
                    <div class="row">
                        <?php
                        $javo_integer = 0;

                        if (!empty($javo_get_sub_terms) && $javo_tso_archive->get('sub_cat_enable', 'on') == 'on') {
                            foreach ($javo_get_sub_terms as $term) {
                                if (
                                        (int) $javo_tso_archive->get('sub_cat_count', 0) > 0 &&
                                        (int) $javo_tso_archive->get('sub_cat_count', 0) <= $javo_integer
                                ) {
                                    continue;
                                };
                                $javo_integer++;
                                ?>
                                <div class="col-md-4">

                                    <li class="lista-subcategorias">
                                        <a href="<?php echo get_term_link($term); ?>">
                                            <h2 class="titulo-lista-subcategorias"><?php echo $term->name; ?></h2>

                                        </a>
                                    </li> <!-- sub-cat-wraps -->

                                </div>
                                <?php
                            };
                        } else {
                            printf('<div class="col-md-12 no-found-sub-categories"><h4>%s</h4></div>', __('Not Found Sub Categories', 'javo_fr'));
                        }; // End if
                        ?>
                    </div><!-- /.row -->
                </div><!-- /.javo-archive-sub-category -->
                <hr>
            <?php endif; ?>
            <!-- Items List -->
            <div class="javo-archive-items-content row">
                <?php
                $javo_this_posts_return = Array();
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        $javo_this_latlng = @unserialize(get_post_meta(get_the_ID(), 'latlng', true));
                        $plano_comercial = get_post_meta(get_the_ID(), "plano_comercial", true);
                        $javo_this_posts_return[get_the_ID()]['latlng'] = $javo_this_latlng;
                        $javo_meta_query = new javo_get_meta(get_the_ID());
                        $javo_set_icon = '';
                        $javo_marker_term_id = wp_get_post_terms(get_the_ID(), 'item_category');
                        $javo_item_location = wp_get_post_terms(get_the_ID(), 'item_location', array('order' => 'DESC', 'orderby' => 'parent'));
                        $logotipo = (Array) @unserialize(get_post_meta(get_the_ID(), "logotipo_images", true));
                        $url = wp_get_attachment_image_src($logotipo[0], Array(130, 130));

                        if (!empty($javo_marker_term_id)) {
                            $javo_set_icon = get_option('javo_item_category_' . $javo_marker_term_id[0]->term_id . '_marker', '');
                            if ($javo_set_icon == '') {
                                $javo_set_icon = $javo_tso->get('map_marker', '');
                            };
                            $javo_this_posts_return[get_the_ID()]['icon'] = $javo_set_icon;
                        };

                        /* PLANO MINISITE */
                        if ($plano_comercial == '0') { //MINISITE
                            get_template_part('content', 'archive');
                        } else if ($plano_comercial == '1') { //MDVIT
                            get_template_part('content', 'mdvit');
                        } else if ($plano_comercial == '2') { //MDL
                            get_template_part('content', 'mdl');
                        } else if ($plano_comercial == '3') { //MDN
                            get_template_part('content', 'mdn');
                        } else {
                            get_template_part('content', 'free');
                        }
                        ?>
                        <script type="text/template" id="javo_map_tmp_<?php the_ID(); ?>">
                            <div class="javo_somw_info panel">
                            <div class="des">
                            <h5><?php echo javo_str_cut(get_the_title(), 30); ?></h5>
                            <ul class="list-unstyled">
                            <li>
                            <?php
                            echo $javo_meta_query->get('(');
                            echo $javo_meta_query->get('ddd1');
                            echo $javo_meta_query->get(')');
                            echo $javo_meta_query->get(' ');
                            echo $javo_meta_query->get('phone1');
                            ?>
                            </li>

                            <li><?php echo $javo_meta_query->get('website'); ?></li>
                            <li><?php echo $javo_meta_query->get('email'); ?></li>
                            </ul>
                            <!--a class="btn btn-dark javo-this-go-more" href="<?php the_permalink(); ?>"><?php _e('Ver Mais', 'javo_fr'); ?></a-->
                            </div>

                            <div class="pics">
                            <div class="thumb">
                            <a href="<?php the_permalink(); ?>">
                            <?php
                            if ($logotipo[0]) {
                                printf('<img src="%s" class="wp-post-image" style="width:130px; height:130px;">', $url[0]);
                            } else {
                                printf('<img src="%s" style="width:130px; height:130px;">', $javo_tso->get('no_image', JAVO_IMG_DIR . '/no-image.png'));
                            };
                            ?>
                            </a>
                            </div> <!-- thumb -->
                            <div class="img-in-text"><?php echo $javo_meta_query->cat('item_category', __('Sem Categoria', 'javo_fr')); ?></div>
                            <div class="javo-left-overlay">
                            <div class="javo-txt-meta-area"><?php echo $javo_meta_query->cat('item_location', __('Sem Localização', 'javo_fr')); ?></div>
                            <div class="corner-wrap">
                            <div class="corner"></div>
                            <div class="corner-background"></div>
                            </div> <!-- corner-wrap -->
                            </div> <!-- javo-left-overlay -->
                            </div> <!-- pic -->
                            </div> <!-- javo_somw_info -->
                        </script>
                        <?php
                    } // End While
                } else {
                    get_template_part('content', 'none');
                }; // End If
                printf("<input type='hidden' name='javo-this-term-posts-latlng' value='%s'>", json_encode($javo_this_posts_return));
                ?>
            </div><!-- /.javo-archive-items-content -->
            <div class="javo_pagination">
                <?php
                global $wp_query;

                $big = 999999999; // need an unlikely integer
                echo paginate_links(array(
                    'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big)))
                    , 'format' => '?paged=%#%'
                    , 'current' => max(1, get_query_var('paged'))
                    , 'total' => $wp_query->max_num_pages
                ));
                ?>
            </div><!-- javo_pagination -->
        </div><!-- /.javo-output -->
    </div><!-- /.col-md-9 -->

    <?php get_sidebar(); ?>
</div><!-- /.container -->
<fieldset>
    <input type='hidden' class='javo_map_visible' value='.javo-archive-header-map'>
    <input type="hidden" javo-map-distance-unit value="<?php echo $javo_tso_map->get('distance_unit', __('km', 'javo_fr')); ?>">
    <input type="hidden" javo-map-distance-max value="<?php echo (float) $javo_tso_map->get('distance_max', '500'); ?>">
</fieldset>

<script type="text/javascript">
    jQuery(function ($) {

        var javo_archive_list = {
            /*****************************************
             **
             ** Variables
             **
             *****************************************/
            el: $('.javo-archive-header-map'),
            distance_unit: $('[javo-map-distance-unit]').val(),
            distance: $('[javo-map-distance-unit]').val() == 'mile' ? 1609.344 : 1000,
            distance_max: $('[javo-map-distance-max]').val(),
            ob_ib: null,
            markers: null,
            bound: new google.maps.LatLngBounds(),
            sanitize_marker: JSON.parse($('input[name="javo-this-term-posts-latlng"]').val()),
            options: {
                /* InfoBubble Option */
                info_bubble: {
                    minWidth: 362,
                    minHeight: 180,
                    overflow: true,
                    shadowStyle: 1,
                    padding: 5,
                    borderRadius: 10,
                    arrowSize: 20,
                    borderWidth: 1,
                    disableAutoPan: false,
                    hideCloseButton: false,
                    arrowPosition: 50,
                    arrowStyle: 0
                }
                /* Display Ratings */
                ,
                raty: {
                    starOff: '<?php echo JAVO_IMG_DIR ?>/star-off-s.png',
                    starOn: '<?php echo JAVO_IMG_DIR ?>/star-on-s.png',
                    starHalf: '<?php echo JAVO_IMG_DIR ?>/star-half-s.png',
                    half: true,
                    readOnly: true
                }
                /* Map */
                ,
                map_init: {
                    map: {
                        options: {
                            center: new google.maps.LatLng(0, 0)
                        }
                    },
                    marker: {
                        events: {}/*,
                        cluster: {
                            radius: 100,
                            0: {
                                content: '<div class="javo-map-cluster admin-color-setting">CLUSTER_COUNT</div>',
                                width: 52,
                                height: 52
                            },
                            events: {
                                click: function (c, e, d) {
                                    c.main.map.setZoom(c.main.map.getZoom() + 1);
                                    c.main.map.panTo(d.data.latLng);

                                }
                            }
                        }*/
                    }
                }
                /* Map Style & P.O.I InfoBox Delete */
                ,
                map_style: [{
                        featureType: "poi",
                        elementType: "labels",
                        stylers: [{
                                visibility: "off"
                            }]

                    }]

                        /* Javo Search Plugin Base Option */
                ,
                search_config: {
                    post_type: 'item',
                    type: 2,
                    page: 1,
                    ppp: 9
                },
                search: {
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    loading: "<?php echo JAVO_IMG_DIR; ?>/loading_1.gif",
                    map: $(".javo_map_visible")
                }
            }
            /*****************************************
             **
             ** Main Funciton
             **
             *****************************************/
            ,
            init: function () {

                /* Get Self Oboject */
                var $object = this;

                /* Define InfoBubble Plug-in */
                this.ob_ib = new InfoBubble(this.options.info_bubble);

                /* Set Marker Variable */
                this.markers = new Array();



                /* Get Marker Informations */
                $.each(this.sanitize_marker, function (i, k) {
                    if (k.latlng.lat == "" || k.latlng.lng == "") {
                        return;
                    }
                    ;
                    $object.markers.push({
                        id: '#javo_map_tmp_' + i,
                        latLng: [k.latlng.lat, k.latlng.lng],
                        options: {
                            icon: k.icon
                        }
                    });
                    $object.bound.extend(new google.maps.LatLng(k.latlng.lat, k.latlng.lng));
                });

                /* Set bind Markers */
                this.options.map_init.marker.values = this.markers;
                this.options.map_init.marker.events.click = this.marker_click;

                /* Define Google Map for Div Element */
                this.el.height(500).gmap3(this.options.map_init);

                //Get Google Map
                this.map = this.el.gmap3('get');

                // Map Style
                this.map_style = new google.maps.StyledMapType(this.options.map_style, {
                    name: 'Javo Single Item Map'
                });
                this.map.mapTypes.set('map_style', this.map_style);
                this.map.setMapTypeId('map_style');

                if (this.markers.length > 0) {
                    this.map.fitBounds(this.bound);
                }

                /* Set Ratings */
                $('.javo_archive_list_rating').each(function () {
                    $object.options.raty.score = $(this).data('score');
                    $(this).raty($object.options.raty).width('');
                });
                this.events();
                var javo_archove_position_slide_option = {
                    start: [300],
                    step: 1,
                    range: {
                        min: [1],
                        max: [parseInt($object.distance_max)]
                    },
                    serialization: {
                        lower: [
                            $.Link({
                                target: $('[javo-wide-map-round]'),
                                format: {
                                    decimals: 0
                                }
                            }), $.Link({
                                target: '-tooltip-<div class="javo-slider-tooltip"></div>',
                                method: function (v) {
                                    $(this).html('<span>' + v + '&nbsp;' + $object.distance_unit + '</span>');
                                },
                                format: {
                                    decimals: 0,
                                    thousand: ','
                                }
                            })
                        ]
                    }
                };
                /*
                 Geo Location Slider Block
                 $('[data-javo-search-form]')
                 .find(".javo-position-slider")
                 .noUiSlider(javo_archove_position_slide_option)
                 .on('set', $object.geolocation);
                 */
            },
            geolocation: function () {
                var $this = $(this);
                var $object = javo_archive_list;
                var $radius = $('[javo-wide-map-round]').val();

                $object.el.gmap3({
                    getgeoloc: {
                        callback: function (latlng) {
                            if (!latlng) {
                                $.javo_msg({
                                    content: 'Your position access failed.'
                                });
                                return false;
                            }
                            ;
                            $(this).gmap3({
                                clear: 'circle'
                            });
                            $(this).gmap3({
                                map: {
                                    options: {
                                        center: latlng,
                                        zoom: 12
                                    }
                                },
                                circle: {
                                    options: {
                                        center: latlng,
                                        radius: $radius * parseFloat($object.distance),
                                        fillColor: '#464646',
                                        strockColor: '#000000'
                                    }
                                }
                            });
                            $(this).gmap3({
                                get: {
                                    name: 'circle',
                                    callback: function (c) {

                                        $(this).gmap3('get').fitBounds(c.getBounds());
                                    }
                                }
                            });
                        }
                    }
                });
            }
            /*****************************************
             **
             ** Event Handlers Funciton
             **
             *****************************************/
            ,
            events: function () {
                var $object = this;
                $('.sel-box').each(function () {
                    var str = $(this).find('li[data-value="' + $(this).find('[type="hidden"]').val() + '"]').text();
                    if (str != "") {
                        $(this).find('[type="text"]').val(str);
                    }
                    ;
                });

                $('[data-javo-archive-order-direction]').find('.btn').on('click', function () {
                    $(this).parent().next('input').val($(this).data('value'));
                    $(this).closest('form').submit();
                });
                $(document)
                        .on('click', '.sel-content > ul li', function () {
                            if (!$(this).closest('.sel-content').hasClass('archive-filter')) {
                                return;
                            }
                            ;
                            $(this).closest('ul').next('input[type="hidden"]').val($(this).data('value')).closest('form').submit();
                        });

                $('[data-javo-search-form]').find('[name^="filter"]').on('change', function () {

                    $object.run();
                });
            },
            marker_click: function (m, e, c) {
                var $object = javo_archive_list;

                var $this_map = $(this).gmap3('get');
                $object.ob_ib.setContent($(c.id).html());
                $object.ob_ib.open($this_map, m);
                $this_map.setCenter(m.getPosition());
            }
            /*****************************************
             **
             ** Ajax Filter Run
             **
             *****************************************/
            ,
            run: function () {
                var $object = this;
                var content_el = $('.javo-output');
                this.content_el = content_el;
                this.options.search.start = true;
                this.options.search.selFilter = $('[data-javo-search-form] [name^="filter"]');
                this.options.search.param = this.options.search_config;
                this.options.search.success_callback = function () {
                    content_el.prepend($('#javo-archive-ajax-result-header').html());
                    $('.javo_detail_slide').each(function () {
                        $(this).flexslider({
                            animation: "slide",
                            controlNav: false,
                            slideshow: true,
                        }).find('ul').magnificPopup({
                            gallery: {
                                enabled: true
                            },
                            delegate: 'u',
                            type: 'image'
                        });
                    });
                    $('.javo-tooltip').each(function (i, e) {
                        var options = {};
                        if (typeof ($(this).data('direction')) != 'undefined') {
                            options.placement = $(this).data('direction');
                        }
                        ;
                        $(this).tooltip(options);
                    });
                };
                this.content_el.javo_search(this.options.search);
            }
        };
        javo_archive_list.init();
    });
</script>

<?php
get_footer();
