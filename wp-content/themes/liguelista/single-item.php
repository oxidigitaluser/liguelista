<?php
global $javo_this_single_page_type;
$javo_this_single_page_type = get_post_meta(get_the_ID(), 'single_post_type', true);
$plano_comercial            = get_post_meta(get_the_ID(), "plano_comercial", true);

/* Single Item Type Fixed */
$javo_this_single_page_type = 'item-tab';

add_filter('body_class', 'javo_this_single_type_callback');
function javo_this_single_type_callback($classes)
{
    global $post, $javo_this_single_page_type;
    
    $classes[] = 'javo-' . $javo_this_single_page_type;
    return $classes;
}

if ($plano_comercial == '0') { //MINISITE
    get_template_part('templates/parts/single', $javo_this_single_page_type);
} else if ($plano_comercial == '1') { //MDVIT
    get_template_part('templates/parts/single', 'item-tab-mdvit');
} else if ($plano_comercial == '2') { //MDL
    get_template_part('templates/parts/single', 'item-tab-mdl');
} else if ($plano_comercial == '3') { //MDN
    get_template_part('templates/parts/single', 'item-tab-mdn');
} else if ($plano_comercial == '4'){
    get_template_part('templates/parts/single', 'item-tab-free');
} else {
    if (!empty($javo_this_single_page_type)) {
        get_template_part('templates/parts/single', $javo_this_single_page_type);
    } else {
        get_template_part('templates/parts/single', 'item-one-page');
    }
}
;