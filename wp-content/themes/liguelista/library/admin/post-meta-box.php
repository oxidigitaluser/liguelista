<?php
setlocale(LC_ALL, "pt_BR");

class javo_post_meta_box {

    public function __construct() {
        add_action('admin_enqueue_scripts', Array($this, 'javo_admin_post_meta_enqueue'));
        add_action('add_meta_boxes', Array($this, 'javo_post_meta_box_init'));
        add_action('save_post', Array($this, 'javo_post_meta_box_save'));
        add_action('admin_footer', Array($this, 'sub_main'));
    }

    public function sub_main() {
        ?>
        <script type='text/javascript'>
            (function ($) {
                "use strict";
                $("body").on("click", ".javo_pmb_option", function () {
                    if ($(this).hasClass("sidebar"))
                        $(".javo_pmb_option.sidebar").removeClass("active");
                    if ($(this).hasClass("header"))
                        $(".javo_pmb_option.header").removeClass("active");
                    if ($(this).hasClass("fancy"))
                        $(".javo_pmb_option.fancy").removeClass("active");
                    if ($(this).hasClass("slider"))
                        $(".javo_pmb_option.slider").removeClass("active");
                    $(this).addClass("active");
                }).on("change", "input[name='javo_opt_header']", function () {
                    $("#javo_post_header_fancy, #javo_post_header_slide").hide();
                    switch ($(this).val()) {
                        case "fancy":
                            $("#javo_post_header_fancy").show();
                            break;
                        case "slider":
                            $("#javo_post_header_slide").show();
                            break;
                    }
                    ;
                });
            })(jQuery);
        </script>
        <?php
    }

    public function javo_admin_post_meta_enqueue() {
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_script('wp-color-picker');
        wp_enqueue_script('my-script-handle', JAVO_THEME_DIR . '/assets/js/admin-color-picker.js', array('wp-color-picker'), false, true);
        wp_enqueue_script('thickbox');
        wp_enqueue_script('google_map_API', 'http://maps.google.com/maps/api/js?sensor=false&amp;language=en', null, '0.0.1', false);
        javo_get_script('gmap3.js', 'jQuery-gmap3', '5.1.1', false);
    }

    public function javo_post_meta_box_init() {
        $screen = Array('post', 'page');
        javo_get_asset_style('javo_admin_post_meta.css', 'javo-admin-post-meta-css');
        foreach ($screen as $s => $v) {
            add_meta_box('javo_post_sidebar_options', 'Sidebar Options', Array($this, 'javo_post_sidebar_option_box'), $v, 'side', 'high');
            add_meta_box('javo_post_header_options', 'Header Options', Array($this, 'javo_post_header_option_box'), $v, 'normal', 'core');
            add_meta_box('javo_post_header_fancy', 'Fancy header options', Array($this, 'javo_post_header_fancy_option'), $v);
            add_meta_box('javo_post_header_slide', 'Slide header options', Array($this, 'javo_post_header_slide_option'), $v);
        };
        add_meta_box('javo_page_options', __('Item Listing Filter (only for item listing pages)', 'javo_fr'), Array($this, 'javo_page_option_box'), 'page', 'side');
        add_meta_box('javo_page_item_listing', __('Item Listing Page Setup', 'javo_fr'), Array($this, 'javo_page_item_listing_callback'), 'page', 'side');
        add_meta_box('javo_blog_options', __('Blogs Listing Filter (only for post listing pages)', 'javo_fr'), Array($this, 'javo_blog_option_box'), 'page', 'side');
        add_meta_box('javo_item_options', __('Informações do Anunciante', 'javo_fr'), Array($this, 'javo_item_option_box'), 'item');
        add_meta_box('javo_payment_info', __('Payment Infomation', 'javo_fr'), Array($this, 'javo_pay_ment_info_box'), 'payment');
        add_meta_box('javo_special_item', __('Item Setting', 'javo_fr'), Array($this, 'javo_special_item_callback'), 'item', 'side');
        add_meta_box('javo_event_options', __('Event Setting', 'javo_fr'), Array($this, 'javo_event_metabox_callback'), 'jv_events', 'normal', 'core');
        add_meta_box('javo_review_otions', __('Review Setting', 'javo_fr'), Array($this, 'javo_review_metabox_callback'), 'review', 'normal', 'core');
        add_meta_box("javo_item_author", __('Property Author', 'javo_fr'), Array($this, "javo_item_author_callback"), 'item', 'side');
    }

    public function javo_item_author_callback($post) {
        $javo_get_all_user = get_users();
        ?>
        <label>
            <input type="radio" name="item_author" value="" checked>
            <?php _e('My Profile', 'javo_fr'); ?>
        </label>
        <br>
        <label>
            <input type="radio" name="item_author" value="other">
            <?php _e('Other User', 'javo_fr'); ?>
            <select name="item_author_id">
                <?php
                if (!empty($javo_get_all_user)) {
                    foreach ($javo_get_all_user as $user) {
                        printf('<option value="%s">%s %s (%s)</option>'
                                , $user->ID
                                , $user->first_name
                                , $user->last_name
                                , $user->user_login
                        );
                    }
                }
                ?>
            </select>
        </label>
        <?php
    }

    public function javo_event_metabox_callback($post) {
        $javo_meta_query = new javo_GET_META($post);
        $javo_get_items_args = Array(
            'post_type' => 'item'
            , 'post_status' => 'publish'
            , 'showposts' => -1
            , 'author' => $post->post_author
        );
        $javo_get_items = get_posts($javo_get_items_args);
        ob_start();
        ?>
        <table class="form-table">
            <tr>
                <th><?php _e('Target Item', 'javo_fr'); ?></th>
                <td>
                    <select name="javo_event[parent_post_id]">
                        <option value=""><?php _e('Not Set'); ?></option>
                        <?php
                        if (!empty($javo_get_items)) {
                            foreach ($javo_get_items as $item) {
                                setup_postdata($item);
                                printf('<option value="%s"%s>%s</option>'
                                        , $item->ID
                                        , ( $javo_meta_query->_get('parent_post_id', 0) == $item->ID ? ' selected' : '')
                                        , $item->post_title
                                );
                            }; // End Foreach
                        };  // End If
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th><?php _e('Brand Tag', 'javo_fr'); ?></th>
                <td><input type="text" name="javo_event[brand]" class="large-text" value="<?php echo $javo_meta_query->_get('brand'); ?>" placeholder="<?php _e('e.g) 40~50%    OFF', 'javo_fr'); ?>"></td>

            </tr>
            <tr></tr></table>
        <?php
        ob_end_flush();
    }

    public function javo_review_metabox_callback($post) {
        $javo_meta_query = new javo_GET_META($post);
        $javo_get_items_args = Array(
            'post_type' => 'item'
            , 'post_status' => 'publish'
            , 'showposts' => -1
        );
        $javo_get_items = get_posts($javo_get_items_args);
        ob_start();
        ?>
        <table class="form-table">
            <tr>
                <th><?php _e('Target Item', 'javo_fr'); ?></th>
                <td>
                    <select name="javo_event[parent_post_id]">
                        <option value=""><?php _e('Not Set'); ?></option>
                        <?php
                        if (!empty($javo_get_items)) {
                            foreach ($javo_get_items as $item) {
                                setup_postdata($item);
                                printf('<option value="%s"%s>%s</option>'
                                        , $item->ID
                                        , ( $javo_meta_query->_get('parent_post_id', 0) == $item->ID ? ' selected' : '')
                                        , $item->post_title
                                );
                            }; // End Foreach
                        };  // End If
                        ?>
                    </select>
                </td>
            </tr>
            <tr></tr></table>
        <?php
        ob_end_flush();
    }

    public function javo_page_item_listing_callback($post) {
        $javo_this_types = Array(
            __('Grid Style', 'javo_fr') => 2
            , __('Line List Style', 'javo_fr') => 4
        );
        $javo_pre_values = get_post_meta($post->ID, 'javo_item_listing_type', true);
        ?>
        <h4><?php _e('Default Listing Type', 'javo_fr'); ?></h4>
        <select name="javo_il[type]">
            <?php
            foreach ($javo_this_types as $label => $type) {
                printf('<option value="%s"%s>%s</option>', $type, ($javo_pre_values == $type ? ' selected' : ''), $label);
            };
            ?>
        </select>
        <script type="text/javascript">
            jQuery(function ($) {
                $(document).on('change', 'select[name="page_template"]', function () {
                    $('#javo_page_item_listing').addClass('hidden');
                    if ($(this).val() == 'templates/tp-item-list.php') {
                        $('#javo_page_item_listing').removeClass('hidden');
                    }
                });
                $('select[name="page_template"]').trigger('change');
            });
        </script>
        <?php
    }

    public function javo_special_item_callback($post) {
        $javo_get_this_item_featured = get_post_meta($post->ID, 'javo_this_featured_item', true);
        ?>
        <table class="form-table">
            <tr>
                <th><?php _e('Featured Item', 'javo_fr'); ?></th>
                <td><input name="javo_item_attribute[featured]" value="use" type="checkbox"<?php checked($javo_get_this_item_featured == 'use'); ?>></td>
            </tr>
        </table>
        <?php
    }

    public function javo_post_sidebar_option_box($post) {
        // Sidebar LEFT / CENTER / RIGHTER Setting
        $get_javo_opt_sidebar = get_post_meta($post->ID, 'javo_sidebar_type', true);
        ?>
        <h5 class="javo_pmb_title"><?php _e("Sidebar position", "javo_fr"); ?></h5>
        <label class="javo_pmb_option sidebar op_s_left<?php echo $get_javo_opt_sidebar == 'left' ? ' active' : ''; ?>">
            <span class="ico_img"></span>
            <p><input name="javo_opt_sidebar" value="left" type="radio" <?php checked($get_javo_opt_sidebar == 'left'); ?>> <?php _e("Left", "javo_fr"); ?></p>
        </label>
        <label class="javo_pmb_option sidebar op_s_right<?php echo $get_javo_opt_sidebar == 'right' ? ' active' : ''; ?>">
            <span class="ico_img"></span>
            <p><input name="javo_opt_sidebar" value="right" type="radio" <?php checked($get_javo_opt_sidebar == 'right'); ?>> <?php _e("Right", "javo_fr"); ?></p>
        </label>
        <label class="javo_pmb_option sidebar op_s_full <?php echo $get_javo_opt_sidebar == 'full' || $get_javo_opt_sidebar == '' ? ' active' : ''; ?>">
            <span class="ico_img"></span>
            <p><input name="javo_opt_sidebar" value="full" type="radio" <?php checked($get_javo_opt_sidebar == 'full' || $get_javo_opt_sidebar == ''); ?>> <?php _e("Fullwidth", "javo_fr"); ?></p>
        </label>
        <?php
    }

    public function javo_post_header_option_box($post) {
        // Header  Fancy / Slider settings
        $get_javo_opt_header = get_post_meta($post->ID, "javo_header_type", true);
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                "use strict";
                var t = "<?php echo $get_javo_opt_header; ?>";
                if (t != "")
                    $("input[name='javo_opt_header'][value='" + t + "']").trigger("click");
            });
        </script>
        <label class="javo_pmb_option header op_h_title_show active">
            <span class="ico_img"></span>
            <p><input name="javo_opt_header" type="radio" value="default"  checked="checked"> <?php _e("Show page title", "javo_fr"); ?></p>
        </label>
        <label class="javo_pmb_option header op_h_title_hide">
            <span class="ico_img"></span>
            <p><input name="javo_opt_header" type="radio" value="notitle"> <?php _e("Hide page title", "javo_fr"); ?></p>
        </label>
        <label class="javo_pmb_option header op_h_title_fancy">
            <span class="ico_img"></span>
            <p><input name="javo_opt_header" type="radio" value="fancy"> <?php _e("Fancy Header", "javo_fr"); ?></p>
        </label>
        <label class="javo_pmb_option header op_h_title_slide">
            <span class="ico_img"></span>
            <p><input name="javo_opt_header" type="radio" value="slider"> <?php _e("Slide Show", "javo_fr"); ?></p>
        </label>
        <label class="javo_pmb_option header op_h_title_map">
            <span class="ico_img"></span>
            <p><input name="javo_opt_header" type="radio" value="map"> <?php _e("Map", "javo_fr"); ?></p>
        </label>
        <?php
    }

    public function javo_post_header_fancy_option($post) {
        // Fancy Option
        $get_javo_opt_fancy = get_post_meta($post->ID, "javo_header_fancy_type", true);
        $javo_fancy = @unserialize(get_post_meta($post->ID, "javo_fancy_options", true));
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                "use strict";
                var t = "<?php echo $get_javo_opt_fancy; ?>";
                if (t != "")
                    $("input[name='javo_opt_fancy'][value='" + t + "']").trigger("click");

                $("body").on("click", ".fileupload", function (e) {
                    var attachment;
                    var t = $(this).attr("tar");
                    e.preventDefault();
                    var file_frame;
                    if (file_frame) {
                        file_frame.open();
                        return;
                    }
                    file_frame = wp.media.frames.file_frame = wp.media({
                        title: jQuery(this).data('uploader_title'),
                        button: {
                            text: jQuery(this).data('uploader_button_text'),
                        },
                        multiple: false
                    });
                    file_frame.on('select', function () {
                        attachment = file_frame.state().get('selection').first().toJSON();
                        $(t).val(attachment.url);
                        $(".javo_bg_img_preview").prop("src", attachment.url);
                    });
                    file_frame.open();
                }).on("click", ".fileuploadcancel", function () {
                    var t = $(this).attr("tar");
                    $(t).val("");
                    $(".javo_bg_img_preview").prop("src", "");
                });
            });
        </script>

        <div class="">
            <label class="javo_pmb_option fancy op_f_left active">
                <span class="ico_img"></span>
                <p><input name="javo_opt_fancy" type="radio" value="left" checked="checked"> <?php _e("Title left", "javo_fr"); ?></p>
            </label>
            <label class="javo_pmb_option fancy op_f_center">
                <span class="ico_img"></span>
                <p><input name="javo_opt_fancy" type="radio" value="center"> <?php _e("Title center", "javo_fr"); ?></p>
            </label>
            <label class="javo_pmb_option fancy op_f_right">
                <span class="ico_img"></span>
                <p><input name="javo_opt_fancy" type="radio" value="right"> <?php _e("Title right", "javo_fr"); ?></p>
            </label>
        </div>
        <hr>
        <div class="javo_pmb_field">
            <dl>
                <dt><label for="javo_fancy_field_title"><?php _e("Title", "javo_fr"); ?></label></dt>
                <dd><input name="javo_fancy[title]" id="javo_fancy_field_title" type="text" value="<?php echo $javo_fancy['title']; ?>"></dd>
            </dl>
            <dl>
                <dt><label for="javo_fancy_field_title_color"><?php _e("Title Color", "javo_fr"); ?></label></dt>
                <dd>
                    <input name="javo_fancy[title_color]" type="text" value="<?php echo ($javo_fancy['title_color'] != "") ? $javo_fancy['title_color'] : "#000000"; ?>" id="javo_fancy_field_title_color" class="wp_color_picker" data-default-color="<?php echo ($javo_fancy['title_color'] != "") ? $javo_fancy['title_color'] : "#000000"; ?>">
                </dd>
            </dl>
            <dl>
                <dt><label for="javo_fancy_field_subtitle"><?php _e("Subtitle", "javo_fr"); ?></label></dt>
                <dd><input name="javo_fancy[subtitle]" id="javo_fancy_field_subtitle" type="text" value="<?php echo $javo_fancy['subtitle']; ?>"></dd>
            </dl>
            <dl>
                <dt><label for="javo_fancy_field_subtitle_color"><?php _e("Subtitle color", "javo_fr"); ?></label></dt>
                <dd><input name="javo_fancy[subtitle_color]" value="<?php echo!empty($javo_fancy['subtitle_color']) ? $javo_fancy['subtitle_color'] : "#000000"; ?>" id="javo_fancy_field_subtitle_color" type="text" class="wp_color_picker" data-default-color="<?php echo ($javo_fancy['subtitle_color'] != "") ? $javo_fancy['subtitle_color'] : "#000000"; ?>"></dd>
            </dl>
            <hr>
            <dl>
                <dt><label for="javo_fancy_field_bg_color"><?php _e("Background color", "javo_fr"); ?></label></dt>
                <dd><input name="javo_fancy[bg_color]" value="<?php echo ($javo_fancy['title_color'] != "") ? $javo_fancy['bg_color'] : "#FFFFFF"; ?>" id="javo_fancy_field_bg_color" type="text" class="wp_color_picker" data-default-color="<?php echo ($javo_fancy['title_color'] != "") ? $javo_fancy['bg_color'] : "#FFFFFF"; ?>"></dd>
            </dl>
            <dl>
                <dt><label for="javo_fancy_field_bg_image"><?php _e("Background Image", "javo_fr"); ?></label></dt>
                <dd><input name="javo_fancy[bg_image]" id="javo_fancy_field_bg_image" type="text" value="<?php echo $javo_fancy['bg_image']; ?>"><button class="fileupload button button-primary" tar="#javo_fancy_field_bg_image"><?php _e('Upload', 'javo_fr'); ?></button><input class="fileuploadcancel button" tar="#javo_fancy_field_bg_image" value="Delete" type="button"></dd>
            </dl>
            <dl>
                <dt><?php _e("Background image preview", "javo_fr"); ?></dt>
                <dd><img src="<?php echo $javo_fancy['bg_image']; ?>" width="200" height="150" border="1" class="javo_bg_img_preview"></dd>
            </dl>
            <script type="text/javascript">
                jQuery(document).ready(function ($) {
                    "use strict";
                    var t = new Array($("select[name='javo_fancy[bg_repeat]']"), $("select[name='javo_fancy[bg_position_x]']"), $("select[name='javo_fancy[bg_position_y]']"));
                    var r = new Array("<?php echo $javo_fancy['bg_repeat']; ?>", "<?php echo $javo_fancy['bg_position_x']; ?>", "<?php echo $javo_fancy['bg_position_y']; ?>");
                    $.each(r, function (i, v) {
                        if (v != "")
                            t[i].val(v);
                    });
                });


            </script>
            <dl>
                <dt><label for="javo_fancy_field_bg_image"><?php _e("Repeat Option", "javo_fr"); ?></label></dt>
                <dd>
                    <select name="javo_fancy[bg_repeat]" id="javo_fancy_field_bg_image">
                        <option value="no-repeat"><?php _e("no-repeat", "javo_fr"); ?></option>
                        <option value="repeat-x"><?php _e("repeat-x", "javo_fr"); ?></option>
                        <option value="repeat-y"><?php _e("repeat-y", "javo_fr"); ?></option>
                    </select>
                </dd>
            </dl>
            <dl>
                <dt><label for="javo_fancy_field_position_x"><?php _e("Position X", "javo_fr"); ?></label></dt>
                <dd>
                    <select name="javo_fancy[bg_position_x]" id="javo_fancy_field_position_x">
                        <option value="left"><?php _e("Left", "javo_fr"); ?></option>
                        <option value="center"><?php _e("Center", "javo_fr"); ?></option>
                        <option value="right"><?php _e("Right", "javo_fr"); ?></option>
                    </select>
                </dd>
            </dl>
            <dl>
                <dt><label for="javo_fancy_field_position_y"><?php _e("Position Y", "javo_fr"); ?></label></dt>
                <dd>
                    <select name="javo_fancy[bg_position_y]" id="javo_fancy_field_position_y">
                        <option value="top"><?php _e("Top", "javo_fr"); ?></option>
                        <option value="center"><?php _e("Center", "javo_fr"); ?></option>
                        <option value="bottom"><?php _e("Bottom", "javo_fr"); ?></option>
                    </select>
                </dd>
            </dl>
            <hr>
            <dl>
                <dt><label for="javo_fancy_field_fullscreen"><?php _e("Height(pixel)", "javo_fr"); ?> </label></dt>
                <dd><input name="javo_fancy[height]" id="javo_fancy_field_fullscreen" value="<?php echo $javo_fancy['height']; ?>" type="text"></dd>
            </dl>

        </div>

        <?php
    }

    public function javo_post_header_slide_option($post) {
        // Slide Option
        $javo_slider = @unserialize(get_post_meta($post->ID, "javo_slider_options", true));
        $get_javo_opt_slider = get_post_meta($post->ID, "javo_slider_type", true);
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                "use strict";
                $("body").on("change", "input[name='javo_opt_slider']", function () {
                    $(".javo_pmb_tabs.slider")
                            .children("div")
                            .removeClass("active");
                    $("div[tab='" + $(this).val() + "']").addClass("active");
                });
                var t = "<?php echo $get_javo_opt_slider; ?>";
                if (t != "")
                    $("input[name='javo_opt_slider'][value='" + t + "']").trigger("click");

            });
        </script>
        <div class="">
            <label class="javo_pmb_option slider op_d_rev active">
                <span class="ico_img"></span>
                <p><input name="javo_opt_slider" type="radio" value="rev" checked="checked"> <?php _e("Revolution", "javo_fr"); ?></p>
            </label>
        </div>

        <!-- section  -->
        <div class="javo_pmb_tabs slider javo_pmb_field">
            <div class="javo_pmb_tab active" tab="rev">
                <dl>
                    <dt><label><?php _e("Choose slider", "javo_fr"); ?></label></dt>
                    <dd>
                        <?php
                        $javo_slider = @unserialize(get_post_meta($post->ID, "javo_slider_options", true));
                        if (class_exists('RevSlider')) {
                            $rev = new RevSlider();
                            $arrSliders = $rev->getArrSliders();
                            echo '<select name="javo_slide[rev_slider]">';
                            foreach ((array) $arrSliders as $revSlider) {
                                $act = ($javo_slider['rev_slider'] == $revSlider->getAlias()) ? " selected='selected'" : "";
                                printf("<option value='%s'%s>%s</option>", $revSlider->getAlias(), $act, $revSlider->getTitle());
                            }
                            echo '</select>';
                        } else {
                            printf('<label>%s</label>', __('Please install revolition slider plugin or create slide item.', 'javo_fr'));
                        };
                        ?>
                    </dd>
                </dl>
            </div>
        </div>

        <?php
    }

    public function javo_page_option_box($post) {
        $taxs = get_taxonomies(Array("object_type" => Array("item")));
        $tax_array = @unserialize(get_post_meta($post->ID, 'javo_item_tax', true));
        $term_array = (array) @unserialize(get_post_meta($post->ID, "javo_item_terms", true));
        ?>

        <h3><?php _e('Selecione a Categoria.', 'javo_fr'); ?></h3>
        <div class="javo_item_listing_filter">
            <select name="javo_item_tax[tax1]">
                <option value=""><?php _e('None', 'javo_fr'); ?></option>
                <?php
                foreach ($taxs as $tax) {
                    printf('<option value="%s" %s>%s</option>'
                            , $tax, ((!empty($tax_array['tax1']) && $tax == $tax_array['tax1']) ? ' selected' : '')
                            , get_taxonomy($tax)->label);
                };
                ?>
            </select>
            <?php foreach ($taxs as $tax) { ?>
                <div class="hidden" data-javo-il="<?php echo $tax; ?>">
                    <?php
                    $terms = get_terms($tax, Array("hide_empty" => 0));
                    foreacH ($terms as $term)
                        printf("<label><input name='javo_item_terms[tax1][" . $term->term_id . "]' value='%s' type='checkbox' %s>&nbsp;%s</label><br>"
                                , $term->term_id
                                , checked(!empty($term_array['tax1'][$term->term_id]) && $term_array['tax1'][$term->term_id] == $term->term_id, true, false)
                                , $term->name
                        );
                    ?>
                </div>
            <?php } ?>
        </div>
        <hr>
        <h3><?php _e('Selecione a Cidade.', 'javo_fr'); ?></h3>
        <div class="javo_item_listing_filter">
            <select name="javo_item_tax[tax2]">
                <option value=""><?php _e('None', 'javo_fr'); ?></option>
                <?php
                foreach ($taxs as $tax) {
                    printf('<option value="%s" %s>%s</option>'
                            , $tax, ((!empty($tax_array['tax2']) && $tax == $tax_array['tax2']) ? ' selected' : '')
                            , get_taxonomy($tax)->label);
                };
                ?>
            </select>
            <?php foreach ($taxs as $tax) { ?>
                <div class="hidden" data-javo-il="<?php echo $tax; ?>">
                    <?php
                    $terms = get_terms($tax, Array("hide_empty" => 0));
                    foreach ($terms as $term)
                        printf("<label><input name='javo_item_terms[tax2][" . $term->term_id . "]' value='%s' type='checkbox' %s>&nbsp;%s</label><br>"
                                , $term->term_id
                                , checked(!empty($term_array['tax2'][$term->term_id]) && $term_array['tax2'][$term->term_id] == $term->term_id, true, false)
                                , $term->name
                        );
                    ?>
                </div>
            <?php } ?>
        </div>
        <script type="text/javascript">
            (function ($) {
                "use strict";
                var _cur_tmp = "templates/tp-item-list.php";

                $('.javo_item_listing_filter').each(function () {
                    var $this = $(this);
                    $this.find('select').on('change', function () {
                        $this.find('[data-javo-il]').addClass('hidden');
                        $this.find('[data-javo-il="' + $(this).val() + '"]').removeClass('hidden').show();
                    }).trigger('change');
                });
                $(document).on("change", "select[name='page_template']", function () {
                    $("#javo_page_options").addClass('hidden').hide();
                    if ($(this).val() == _cur_tmp)
                        $("#javo_page_options").remove('hidden').show();
                }).trigger('change');
            })(jQuery);
        </script>

        <?php
    }

    public function javo_blog_option_box($post) {
        $javo_blog_tax = get_taxonomies(Array("object_type" => Array("post")));
        $javo_blog_term_array = @unserialize(get_post_meta($post->ID, "javo_blog_terms", true));

        printf('<select name="javo_blog_tax"><option value="">%s</option>', __('None', 'javo_fr'));
        foreach ($javo_blog_tax as $tax)
            printf("<option value='%s'>%s</option>"
                    , $tax
                    , get_taxonomy($tax)->label
            );
        echo "</select>";

        foreach ($javo_blog_tax as $tax) {
            ?>
            <div class="javo_blog_tax_<?php echo get_taxonomy($tax)->label; ?>">
                <?php
                $javo_blog_terms = get_terms($tax, Array("hide_empty" => 0));
                foreacH ($javo_blog_terms as $term)
                    printf("<label><input name='javo_blog_terms[%s]' value='use' type='checkbox'>&nbsp;%s</label><br>"
                            , $term->term_id
                            , $term->name
                    );
                ?>
            </div>
        <?php } ?>
        <hr>
        <script type="text/javascript">
            var _cur_blog_tmp = "templates/tp-blogs.php";
            (function ($) {
                "use strict";
                if ($("select[name='page_template']").val() == _cur_blog_tmp)
                    $("#javo_blog_options").show();

                $("select[name='page_template']").on("change", function () {
                    $("#javo_blog_options").hide()
                    if ($(this).val() == _cur_blog_tmp)
                        $("#javo_blog_options").show()
                });
                var current_blog_term = new Array();
                var current_blog_tax = "<?php echo get_post_meta($post->ID, 'javo_blog_tax', true); ?>";
        <?php
        if (!empty($javo_blog_term_array)) {
            foreach ($javo_blog_term_array as $term => $value) {
                printf("current_blog_term.push(%s);\n", $term);
            };
        };
        ?>
                for (i in current_blog_term) {
                    $("input[name='javo_blog_terms[" + current_blog_term[i] + "]']").trigger("click");
                }
                $("div[class^='javo_blog_tax_']").hide();
                $("select[name='javo_blog_tax']").on("change", function () {
                    $("div[class^='javo_blog_tax_']").hide();
                    $(".javo_blog_tax_" + $(this).children("option:selected").text()).show();
                });
                if (current_blog_tax != "") {
                    $("select[name='javo_blog_tax']").val(current_blog_tax).trigger("change");
                }
            })(jQuery);
        </script>

        <?php
    }

    public function javo_pt_add($meta_key, $post_id = NULL) {
        if ($post_id == NULL)
            return;
        return sprintf("<input name='javo_pt[%s]' value='%s'>"
                , $meta_key
                , get_post_meta($post_id, $meta_key, true));
    }

    public function javo_item_option_box($post) {
        global $javo_tso;
        $javo_ts_map = new javo_ARRAY((Array) $javo_tso->get('map', Array()));
        $alerts = Array(
            "address_search_fail" => __('Sorry, find address failed', 'javo_fr')
        );
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                "use strict";
                var _cur;
                _cur = $("input[name='javo_pt[lat]']").val() != "" && $("input[name='javo_pt[lng]']").val() != "" ?
                        new google.maps.LatLng($("input[name='javo_pt[lat]']").val(), $("input[name='javo_pt[lng]']").val()) :
                        new google.maps.LatLng(<?php echo $javo_ts_map->get('default_lat', 40.7143528); ?>, <?php echo $javo_ts_map->get('default_lng', -74.0059731); ?>);
                var option = {
                    map: {options: {zoom: 10}},
                    marker: {
                        options: {draggable: true},
                        events: {
                            dragend: function (m) {
                                var mark = m.getPosition();
                                $("input[name='javo_pt[lat]']").val(mark.lat());
                                $("input[name='javo_pt[lng]']").val(mark.lng());
                            }
                        }
                    }
                };
                option.map.options.center = _cur;
                option.marker.values = [{latLng: _cur}];

                $(".map_area").css("height", 300).gmap3(option);
                $("body").on("keyup", ".javo_txt_find_address", function (e) {
                    e.preventDefault();
                    if (e.keyCode == 13)
                        $(".javo_btn_find_address").trigger("click");
                    return false;
                }).parent().find(".javo_btn_find_address").on("click", function () {
                    var _addr = $(".javo_txt_find_address").val();
                    $(".map_area").gmap3({
                        getlatlng: {
                            address: _addr,
                            callback: function (r) {
                                if (!r) {
                                    alert('<?php echo $alerts["address_search_fail"]; ?>');
                                    return false;
                                }
                                var _find = r[0].geometry.location;
                                $("input[name='javo_pt[lat]']").val(_find.lat());
                                $("input[name='javo_pt[lng]']").val(_find.lng());
                                $(".map_area").gmap3({
                                    get: {
                                        name: "marker",
                                        callback: function (m) {
                                            m.setPosition(_find);
                                            $(".map_area").gmap3({map: {options: {center: _find}}});
                                        }
                                    }
                                });
                            }
                        }
                    });
                });
                $("body").on("click", ".javo_pt_detail_del", function () {
                    var t = $(this);
                    t.parents(".javo_pt_field").remove();

                });
                $("body").on("click", ".javo_pt_detail_add", function (e) {
                    e.preventDefault();
                    var attachment;
                    var file_frame, t = $(this);
                    if (file_frame) {
                        file_frame.open();
                        return;
                    }
                    file_frame = wp.media.frames.file_frame = wp.media({
                        title: jQuery(this).data('uploader_title'),
                        button: {
                            text: jQuery(this).data('uploader_button_text'),
                        },
                        multiple: false
                    });
                    file_frame.on('select', function () {
                        var str = "";
                        attachment = file_frame.state().get('selection').first().toJSON();

                        str += "<div class='javo_pt_field' style='float:left;'>";
                        str += "<img src='" + attachment.url + "' width='150'> <div align='center'>";
                        str += "<input name='javo_pt_detail[]' value='" + attachment.id + "' type='hidden'>";
                        str += "<input class='javo_pt_detail_del button' type='button' value='Delete'>";
                        str += "</div></div>";
                        t.parents("td").find(".javo_pt_images").append(str);
                    });
                    file_frame.open();
                });

                $("body").on("click", ".javo_pt_related_del", function () {
                    var t = $(this);
                    t.parents(".javo_pt_field").remove();

                });
                $("body").on("click", ".javo_pt_related_add", function (e) {
                    e.preventDefault();
                    var attachment;
                    var file_frame, t = $(this);
                    if (file_frame) {
                        file_frame.open();
                        return;
                    }
                    file_frame = wp.media.frames.file_frame = wp.media({
                        title: jQuery(this).data('uploader_title'),
                        button: {
                            text: jQuery(this).data('uploader_button_text'),
                        },
                        multiple: false
                    });
                    file_frame.on('select', function () {
                        var str = "";
                        attachment = file_frame.state().get('selection').first().toJSON();

                        str += "<div class='javo_pt_field_rel' style='float:left;'>";
                        str += "<img src='" + attachment.url + "' width='150'> <div align='center'>";
                        str += "<input name='javo_pt_related[]' value='" + attachment.id + "' type='hidden'>";
                        str += "<input class='javo_pt_related_del button' type='button' value='Delete'>";
                        str += "</div></div>";
                        t.parents("td").find(".javo_pt_related").append(str);
                    });
                    file_frame.open();
                });

                $("body").on("click", ".javo_pt_logotipo_del", function () {
                    var t = $(this);
                    t.parents(".javo_pt_field").remove();

                });
                $("body").on("click", ".javo_pt_logotipo_add", function (e) {
                    e.preventDefault();
                    var attachment;
                    var file_frame, t = $(this);
                    if (file_frame) {
                        file_frame.open();
                        return;
                    }
                    file_frame = wp.media.frames.file_frame = wp.media({
                        title: jQuery(this).data('uploader_title'),
                        button: {
                            text: jQuery(this).data('uploader_button_text'),
                        },
                        multiple: false
                    });
                    file_frame.on('select', function () {
                        var str = "";
                        attachment = file_frame.state().get('selection').first().toJSON();

                        str += "<div class='javo_pt_field_rel' style='float:left;'>";
                        str += "<img src='" + attachment.url + "' width='150'> <div align='center'>";
                        str += "<input name='javo_pt_logotipo[]' value='" + attachment.id + "' type='hidden'>";
                        str += "<input class='javo_pt_logotipo_del button' type='button' value='Delete'>";
                        str += "</div></div>";
                        t.parents("td").find(".javo_pt_logotipo").append(str);
                    });
                    file_frame.open();
                });

                $("body").on("click", ".javo_pt_banner_del", function () {
                    var t = $(this);
                    t.parents(".javo_pt_field").remove();

                });
                $("body").on("click", ".javo_pt_banner_add", function (e) {
                    e.preventDefault();
                    var attachment;
                    var file_frame, t = $(this);
                    if (file_frame) {
                        file_frame.open();
                        return;
                    }
                    file_frame = wp.media.frames.file_frame = wp.media({
                        title: jQuery(this).data('uploader_title'),
                        button: {
                            text: jQuery(this).data('uploader_button_text'),
                        },
                        multiple: false
                    });
                    file_frame.on('select', function () {
                        var str = "";
                        attachment = file_frame.state().get('selection').first().toJSON();

                        str += "<div class='javo_pt_field_rel' style='float:left;'>";
                        str += "<img src='" + attachment.url + "' width='150'> <div align='center'>";
                        str += "<input name='javo_pt_banner[]' value='" + attachment.id + "' type='hidden'>";
                        str += "<input class='javo_pt_banner_del button' type='button' value='Delete'>";
                        str += "</div></div>";
                        t.parents("td").find(".javo_pt_banner").append(str);
                    });
                    file_frame.open();
                });

            });
        </script>
        <table class="form-table">
            <!--tr>
                <th><?php _e('Additional Item Information', 'javo_fr'); ?></th>
                <td>
            <?php $javo_meta = new javo_get_meta($post->ID); ?>
                </td>
            </tr-->
            <tr>
                <th><?php _e('Código', 'javo_fr'); ?></th>
                <td><label name="codigo"><?php echo $post->ID; ?></label>
            </tr>
            <tr>
                <th><?php _e('Exibir nos cadernos', 'javo_fr'); ?></th>
                <td>
                    <?php $exibir_cadernos = get_post_meta($post->ID, 'exibir_cadernos', true); ?>
                    <input name="javo_pt[meta][exibir_cadernos]" id="exibir_cadernos" value="y" type="checkbox"<?php checked($exibir_cadernos == '1'); ?>>
                </td>
            </tr>
            <tr>
                <th><?php _e('Plano Comercial', 'javo_fr'); ?></th>
                <td>
                    <?php $plano_comercial = get_post_meta($post->ID, 'plano_comercial', true); ?>
                    <select name="javo_pt[meta][plano_comercial]" class="form-control">
                        <option value=""><?php _e("Selecione", "javo_fr"); ?></option>
                        <option value="MINISITE" <?php echo selected($plano_comercial == '0'); ?>><?php _e("MINI SITE", "javo_fr"); ?></option>
                        <option value="MDVIT" <?php echo selected($plano_comercial == '1'); ?>><?php _e("MDVIT - Módulo Vitrine", "javo_fr"); ?></option>
                        <option value="MDL" <?php echo selected($plano_comercial == '2'); ?>><?php _e("MDL - Módulo com Logomarca", "javo_fr"); ?></option>
                        <option value="MDN" <?php echo selected($plano_comercial == '3'); ?>><?php _e("MDN - Módulo Negrito", "javo_fr"); ?></option>
                        <option value="GRATUITO" <?php echo selected($plano_comercial == '4'); ?>><?php _e("GRATUITO", "javo_fr"); ?></option>
                    </select>
                </td>
            </tr>
            <tr>
                <th><?php _e('Contrato', 'javo_fr'); ?></th>
                <td><input name="javo_pt[meta][contract]" value="<?php echo $javo_meta->get('contract'); ?>"></td>
            </tr>
            <tr>
                <th><?php _e('PF / PJ', 'javo_fr'); ?></th>
                <td class="btn-group" data-toggle="buttons">
                    <label>
                        <?php _e('PF', 'javo_fr'); ?>
                        <input type="radio" name="javo_pt[meta][pfpj]" value="pf" <?php checked($javo_meta->get('pfpj') == 'pf'); ?>>
                    </label>
                    <label>
                        <?php _e('PJ', 'javo_fr'); ?>
                        <input type="radio" name="javo_pt[meta][pfpj]" value="pj" <?php checked($javo_meta->get('pfpj') == 'pj'); ?>>
                    </label>
                </td>
            </tr>
            <tr>
                <th><?php _e('CPF / CNPJ', 'javo_fr'); ?></th>
                <td><input name="javo_pt[meta][cpfcnpj]" value="<?php echo $javo_meta->get('cpfcnpj'); ?>"></td>
            </tr>
            <hr>
            <tr>
                <th><?php _e('Endereço', 'javo_fr'); ?></th>
                <td><input name="javo_pt[meta][address]" value="<?php echo $javo_meta->get('address'); ?>"></td>
            </tr>
            <tr>
                <th><?php _e('Cidade/Estado', 'javo_fr'); ?></th>
                <td><input name="javo_pt[meta][city]" value="<?php echo $javo_meta->get('city'); ?>"></td>
            </tr>
            <tr>
                <th><?php _e('Complemento', 'javo_fr'); ?></th>
                <td><input name="javo_pt[meta][complement]" value="<?php echo $javo_meta->get('complement'); ?>"></td>
            </tr>
            <tr>
                <th><?php _e('Bairro', 'javo_fr'); ?></th>
                <td><input name="javo_pt[meta][district]" value="<?php echo $javo_meta->get('district'); ?>"></td>
            </tr>
            <tr>
                <th><?php _e('CEP', 'javo_fr'); ?></th>
                <td><input name="javo_pt[meta][cep]" value="<?php echo $javo_meta->get('cep'); ?>"></td>
            </tr>
            <tr>
                <th><?php _e('Localização', 'javo_fr'); ?></th>
                <td>
                    <input class="javo_txt_find_address" type="text"><a class="button javo_btn_find_address"><?php _e('Pesquisar', 'javo_fr'); ?></a>
                    <div class="map_area"></div>
                    <?php
                    $latlng = @unserialize(get_post_meta($post->ID, "latlng", true));
                    printf("
                                Latitude : <input name='javo_pt[lat]' value='%s' type='text'>
                                Longitude : <input name='javo_pt[lng]' value='%s' type='text'>
                            ", $latlng['lat'], $latlng['lng']);
                    ?>
                </td>

            </tr>

            <tr>
                <th><?php _e('Telefone 1', 'javo_fr'); ?></th>
                <td><input name="javo_pt[meta][ddd1]" value="<?php echo $javo_meta->get('ddd1'); ?>" style="width: 50px;">
                    <input name="javo_pt[meta][phone1]" value="<?php echo $javo_meta->get('phone1'); ?>"></td>
            </tr>
            <tr>
                <th><?php _e('Telefone 2', 'javo_fr'); ?></th>
                <td><input name="javo_pt[meta][ddd2]" value="<?php echo $javo_meta->get('ddd2'); ?>" style="width: 50px;">
                    <input name="javo_pt[meta][phone2]" value="<?php echo $javo_meta->get('phone2'); ?>"></td>
            </tr>
            <tr>
                <th><?php _e('Telefone 3', 'javo_fr'); ?></th>
                <td><input name="javo_pt[meta][ddd3]" value="<?php echo $javo_meta->get('ddd3'); ?>" style="width: 50px;">
                    <input name="javo_pt[meta][phone3]" value="<?php echo $javo_meta->get('phone3'); ?>"></td>
            </tr>
            <tr>
                <th><?php _e('Telefone 0800', 'javo_fr'); ?></th>
                <td><input name="javo_pt[meta][phone0800]" value="<?php echo $javo_meta->get('phone0800'); ?>"></td>
            </tr>
            <tr>
                <th><?php _e('Email', 'javo_fr'); ?></th>
                <td><input name="javo_pt[meta][email]" value="<?php echo $javo_meta->get('email'); ?>"></td>
            </tr>
            <tr>
                <th><?php _e('Website', 'javo_fr'); ?>
            <p style="font-weight: 100;">Retirar o http://</p>
        </th>
        <td><input name="javo_pt[meta][website]" value="<?php echo $javo_meta->get('website'); ?>"></td>
        </tr>

        <tr>
            <th><?php _e('Formas de Pagamento', 'javo_fr'); ?></th>
            <td>
                <div>
                    <label><?php _e('Cartões de Crédito', 'javo_fr'); ?></label>
                    <ul>
                        <li>
                            <input name="javo_pt[meta][visa]" id="visa" value="y" type="checkbox"<?php checked($javo_meta->get('visa') == 'y'); ?>>
                            <label for="visa"> <?php _e('Visa', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][master]" id="master" value="y" type="checkbox"<?php checked($javo_meta->get('master') == 'y'); ?>>
                            <label for="master"><?php _e('MarterCard', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][amex]" id="amex" value="y" type="checkbox"<?php checked($javo_meta->get('amex') == 'y'); ?>>
                            <label for="amex"><?php _e('American Express', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][dinclub]" id="dinclub" value="y" type="checkbox"<?php checked($javo_meta->get('dinclub') == 'y'); ?>>
                            <label for="dinclub"><?php _e('Diners Club', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][hipercard]" id="hipercard" value="y" type="checkbox"<?php checked($javo_meta->get('hipercard') == 'y'); ?>>
                            <label for="hipercard"><?php _e('Hipercard', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][elo]" id="elo" value="y" type="checkbox"<?php checked($javo_meta->get('elo') == 'y'); ?>>
                            <label for="elo"><?php _e('Elo', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][aura]" id="aura" value="y" type="checkbox"<?php checked($javo_meta->get('aura') == 'y'); ?>>
                            <label for="aura"><?php _e('Aura', 'javo_fr'); ?></label>
                        </li>
                    </ul>
                </div>
                <div>
                    <label><?php _e('Cartões de Débito', 'javo_fr'); ?></label>
                    <ul>
                        <li>
                            <input name="javo_pt[meta][visael]" id="visael" value="y" type="checkbox"<?php checked($javo_meta->get('visael') == 'y'); ?>>
                            <label for="visael"> <?php _e('Visa Electron', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][maestro]" id="maestro" value="y" type="checkbox"<?php checked($javo_meta->get('maestro') == 'y'); ?>>
                            <label for="maestro"><?php _e('Maestro', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][itau]" id="itau" value="y" type="checkbox"<?php checked($javo_meta->get('itau') == 'y'); ?>>
                            <label for="itau"><?php _e('Itaú', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][bradesco]" id="bradesco" value="y" type="checkbox"<?php checked($javo_meta->get('bradesco') == 'y'); ?>>
                            <label for="bradesco"><?php _e('Bradesco', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][bb]" id="bb" value="y" type="checkbox"<?php checked($javo_meta->get('bb') == 'y'); ?>>
                            <label for="bb"><?php _e('Banco do Brasil', 'javo_fr'); ?></label>
                        </li>
                    </ul>
                </div>
                <div>
                    <label><?php _e('Vale Refeição', 'javo_fr'); ?></label>
                    <ul>
                        <li>
                            <input name="javo_pt[meta][ticket]" id="ticket" value="y" type="checkbox"<?php checked($javo_meta->get('ticket') == 'y'); ?>>
                            <label for="ticket"><?php _e('Ticket Restaurante', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][vale]" id="vale" value="y" type="checkbox"<?php checked($javo_meta->get('vale') == 'y'); ?>>
                            <label for="vale"><?php _e('Vale Refeição', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][sodexo]" id="sodexo" value="y" type="checkbox"<?php checked($javo_meta->get('sodexo') == 'y'); ?>>
                            <label for="sodexo"><?php _e('Sodexo', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][vvalelo]" id="vvalelo" value="y" type="checkbox"<?php checked($javo_meta->get('vvalelo') == 'y'); ?>>
                            <label for="vvalelo"><?php _e('Visa Vale - Alelo', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][vcard]" id="vcard" value="y" type="checkbox"<?php checked($javo_meta->get('vcard') == 'y'); ?>>
                            <label for="vcard"><?php _e('Vale Card', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][nutricash]" id="nutricash" value="y" type="checkbox"<?php checked($javo_meta->get('nutricash') == 'y'); ?>>
                            <label for="nutricash"><?php _e('Nutricash', 'javo_fr'); ?></label>
                        </li>
                    </ul>
                </div>
                <div>
                    <label><?php _e('Outros', 'javo_fr'); ?></label>
                    <ul>
                        <li>
                            <input name="javo_pt[meta][din]" id="din" value="y" type="checkbox"<?php checked($javo_meta->get('din') == 'y'); ?>>
                            <label for="din"><?php _e('Dinheiro', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][bol]" id="bol" value="y" type="checkbox"<?php checked($javo_meta->get('bol') == 'y'); ?>>
                            <label for="bol"><?php _e('Boleto Bancário', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][dep]" id="dep" value="y" type="checkbox"<?php checked($javo_meta->get('dep') == 'y'); ?>>
                            <label for="dep"><?php _e('Depósito / Transferência', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][pagseg]" id="pagseg" value="y" type="checkbox"<?php checked($javo_meta->get('pagseg') == 'y'); ?>>
                            <label for="pagseg"><?php _e('PagSeguro', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][pp]" id="pp" value="y" type="checkbox"<?php checked($javo_meta->get('pp') == 'y'); ?>>
                            <label for="pp"><?php _e('PayPal', 'javo_fr'); ?></label>
                        </li>
                        <li>
                            <input name="javo_pt[meta][moip]" id="moip" value="y" type="checkbox"<?php checked($javo_meta->get('moip') == 'y'); ?>>
                            <label for="moip"><?php _e('MoIP', 'javo_fr'); ?></label>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php _e('Redes Sociais', 'javo_fr'); ?>
        <p style="font-weight: 100;">Usar o endereço completo.<br><br>
            Exemplo:<br>https://www.facebook.com/OxiDigital</p>
        </th>
        <td>
            <table>
                <tr>
                    <td style="padding: 0px 10px">
                        <label for="facebook"><?php _e('Facebook', 'javo_fr'); ?></label>
                    </td>
                    <td style="padding: 0px 10px">
                        <input name="javo_pt[meta][face]" id="facebook" value="<?php echo $javo_meta->get('face'); ?>">
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px">
                        <label for="twitter"><?php _e('Twitter', 'javo_fr'); ?></label>
                    </td>
                    <td style="padding: 0px 10px">
                        <input name="javo_pt[meta][twitter]" id="twitter" value="<?php echo $javo_meta->get('twitter'); ?>">
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px">
                        <label for="insta"><?php _e('Instagram', 'javo_fr'); ?></label>
                    </td>
                    <td style="padding: 0px 10px">
                        <input name="javo_pt[meta][insta]" id="insta" value="<?php echo $javo_meta->get('insta'); ?>">
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px">
                        <label for="plus"><?php _e('Google Plus', 'javo_fr'); ?></label>
                    </td>
                    <td style="padding: 0px 10px">
                        <input name="javo_pt[meta][plus]" id="plus" value="<?php echo $javo_meta->get('plus'); ?>">
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px">
                        <label for="youtube"><?php _e('Youtube', 'javo_fr'); ?></label>
                    </td>
                    <td style="padding: 0px 10px">
                        <input name="javo_pt[meta][youtube]" id="youtube" value="<?php echo $javo_meta->get('youtube'); ?>">
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px">
                        <label for="vimeo"><?php _e('Vimeo', 'javo_fr'); ?></label>
                    </td>
                    <td style="padding: 0px 10px">
                        <input name="javo_pt[meta][vimeo]" id="vimeo" value="<?php echo $javo_meta->get('vimeo'); ?>">
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px">
                        <label for="pinterest"><?php _e('Pinterest', 'javo_fr'); ?></label>
                    </td>
                    <td style="padding: 0px 10px">
                        <input name="javo_pt[meta][pint]" id="pinterest" value="<?php echo $javo_meta->get('pint'); ?>">
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px">
                        <label for="linkedin"><?php _e('LinkedIn', 'javo_fr'); ?></label>
                    </td>
                    <td style="padding: 0px 10px">
                        <input name="javo_pt[meta][lnkdin]" id="linkedin" value="<?php echo $javo_meta->get('lnkdin'); ?>">
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px">
                        <label for="4sqr"><?php _e('Foursquare', 'javo_fr'); ?></label>
                    </td>
                    <td style="padding: 0px 10px">
                        <input name="javo_pt[meta][4sqr]" id="4sqr" value="<?php echo $javo_meta->get('4sqr'); ?>">
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px">
                        <label for="tumblr"><?php _e('Tumblr', 'javo_fr'); ?></label>
                    </td>
                    <td style="padding: 0px 10px">
                        <input name="javo_pt[meta][tumblr]" id="tumblr" value="<?php echo $javo_meta->get('tumblr'); ?>">
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px">
                        <label for="fckr"><?php _e('Flickr', 'javo_fr'); ?></label>
                    </td>
                    <td style="padding: 0px 10px">
                        <input name="javo_pt[meta][fckr]" id="fckr" value="<?php echo $javo_meta->get('fckr'); ?>">
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px">
                        <label for="behance"><?php _e('Behance', 'javo_fr'); ?></label>
                    </td>
                    <td style="padding: 0px 10px">
                        <input name="javo_pt[meta][behance]" id="behance" value="<?php echo $javo_meta->get('behance'); ?>">
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px">
                        <label for="github"><?php _e('Github', 'javo_fr'); ?></label>
                    </td>
                    <td style="padding: 0px 10px">
                        <input name="javo_pt[meta][github]" id="github" value="<?php echo $javo_meta->get('github'); ?>">
                    </td>
                </tr>
            </table>
        </td>
        </tr>
        <th><?php _e('Video', 'javo_fr'); ?></th>
        <td>
            <?php
            $javo_video_portals = Array('youtube', 'vimeo', 'dailymotion', 'yahoo', 'bliptv', 'veoh', 'viddler');
            $javo_get_video_meta = (Array) get_post_meta($post->ID, 'video', true);
            $javo_video_meta = new javo_ARRAY($javo_get_video_meta);
            $javo_get_video = Array(
                "portal" => $javo_video_meta->get('portal', NULL)
                , "video_id" => $javo_video_meta->get('video_id', NULL)
            );
            $javo_this_selbox_options = Array(
                __('With slider (first)', 'javo_fr') => 'slide'
                , __('Header ( only "Youtube", "vimeo")', 'javo_fr') => 'header'
                , __('After Description Section', 'javo_fr') => 'descript'
                , __('After Contact Section', 'javo_fr') => 'contact'
                , __('Hide / Disable', 'javo_fr') => 'disabled'
            );
            ?>

            <select name="javo_video[portal]">
                <option value=""><?php _e('None', 'javo_fr'); ?></option>
                <?php
                foreach ($javo_video_portals as $portal) {
                    printf('<option value="%s"%s>%s</option>'
                            , $portal
                            , ($portal == $javo_get_video['portal'] ? ' selected' : '')
                            , $portal
                    );
                };
                ?>
            </select>
            <input name="javo_video[video_id]" value="<?php echo $javo_get_video['video_id']; ?>">

            <div class="javo_iclbox_wrap">
                <br>
                <h4><?php _e('Video Location', 'javo_fr'); ?></h4>
                <div>
                    <select name="javo_video[single_position]">
                        <?php
                        foreach ($javo_this_selbox_options as $text => $value) {
                            printf('<option value="%s"%s>%s</option>'
                                    , $value
                                    , ( $javo_video_meta->get('single_position', '') == $value ? ' selected' : '')
                                    , $text
                            );
                        };
                        ?>
                    </select>
                </div>
            </div>
        </td>
        </tr>

        <tr>
            <th><?php _e('Logotipo', 'javo_fr'); ?>
        <p style="font-weight: 100;">Tamanho padrão 130px por 130px.<br>
            Planos: MINISITE, MDVIT e MDL.</p>
        </th>

        <td>
            <div class="">
                <a href="javascript:" class="button button-primary javo_pt_logotipo_add"><?php _e('Adicionar Logotipo', 'javo_fr'); ?></a>
            </div>
            <div class="javo_pt_logotipo">
                <?php
                $logotipo = @unserialize(get_post_meta($post->ID, "logotipo_images", true));
                if (is_Array($logotipo)) {
                    foreach ($logotipo as $iamge => $src) {
                        $url = wp_get_attachment_image_src($src, 'thumbnail');
                        printf("
                                <div class='javo_pt_field' style='float:left;'>
                                    <img src='%s'><input name='javo_pt_logotipo[]' value='%s' type='hidden'>
                                    <div class='' align='center'>
                                        <input class='javo_pt_logotipo_del button' type='button' value='Deletar Logotipo'>
                                    </div>
                                </div>
                                ", $url[0], $src);
                    };
                };
                ?>
            </div>
        </td>
        </tr>
        <tr>
            <th><?php _e('Banner', 'javo_fr'); ?>
        <p style="font-weight: 100;">Tamanho padrão 770px por 400px.<br>
            Planos: MDVIT.</p>
        </th>
        <td>
            <div class="">
                <a href="javascript:" class="button button-primary javo_pt_banner_add"><?php _e('Adicionar Imagem', 'javo_fr'); ?></a>
            </div>
            <div class="javo_pt_banner">
                <?php
                $banner = @unserialize(get_post_meta($post->ID, "banner_images", true));
                if (is_Array($banner)) {
                    foreach ($banner as $iamge => $src) {
                        $url = wp_get_attachment_image_src($src, 'thumbnail');
                        printf("
                                <div class='javo_pt_field' style='float:left;'>
                                    <img src='%s'><input name='javo_pt_banner[]' value='%s' type='hidden'>
                                    <div class='' align='center'>
                                        <input class='javo_pt_banner_del button' type='button' value='Deletar'>
                                    </div>
                                </div>
                                ", $url[0], $src);
                    };
                };
                ?>
            </div>
        </td>
        </tr>
        <tr>
            <th><?php _e('Slider MINISITE', 'javo_fr'); ?>
        <p style="font-weight: 100;">Tamanho padrão 800px por 420px.<br>
            Planos: MINISITE.</p>
        </th>
        <td>
            <div class="">
                <a href="javascript:" class="button button-primary javo_pt_detail_add"><?php _e('Adicionar Imagem', 'javo_fr'); ?></a>
            </div>
            <div class="javo_pt_images">
                <?php
                $images = @unserialize(get_post_meta($post->ID, "detail_images", true));
                if (is_Array($images)) {
                    foreach ($images as $iamge => $src) {
                        $url = wp_get_attachment_image_src($src, 'thumbnail');
                        printf("
                                <div class='javo_pt_field' style='float:left;'>
                                    <img src='%s'><input name='javo_pt_detail[]' value='%s' type='hidden'>
                                    <div class='' align='center'>
                                        <input class='javo_pt_detail_del button' type='button' value='Deletar'>
                                    </div>
                                </div>
                                ", $url[0], $src);
                    };
                };
                ?>
            </div>
        </td>
        </tr>
        <tr>
            <th><?php _e('Galeria de Imagens', 'javo_fr'); ?>
        <p style="font-weight: 100;">Preferencia para imagens com pelo menos 1200px por 800px.<br>
            Planos: MINISITE.</p></th>
        <td>
            <div class="">
                <a href="javascript:" class="button button-primary javo_pt_related_add"><?php _e('Adicionar Imagem', 'javo_fr'); ?></a>
            </div>
            <div class="javo_pt_related">
                <?php
                $imagesRelated = @unserialize(get_post_meta($post->ID, "related_images", true));
                if (is_Array($imagesRelated)) {
                    foreach ($imagesRelated as $iamge => $src) {
                        $url = wp_get_attachment_image_src($src, 'thumbnail');
                        printf("
                                <div class='javo_pt_field' style='float:left;'>
                                    <img src='%s'><input name='javo_pt_related[]' value='%s' type='hidden'>
                                    <div class='' align='center'>
                                        <input class='javo_pt_related_del button' type='button' value='Deletar'>
                                    </div>
                                </div>
                                ", $url[0], $src);
                    };
                };
                ?>
            </div>
        </td>
        </tr>
        <!--tr>
            <th><?php _e('Custom Field Information', 'javo_fr'); ?></th>
            <td>
        <?php
        global $javo_custom_field, $edit;
        $edit = $post;
        echo $javo_custom_field->form();
        ?>
            </td>
        </tr-->
        </table>
        <?php
    }

    public function javo_pay_ment_info_box($post) {
        $javo_pay_post = new javo_get_meta($post);
        ?>
        <table class="form-table">
            <tr>
                <th><?php _e('Item name', 'javo_fr'); ?></th>
                <td><?php echo $javo_pay_post->_get('pay_item_id'); ?></td>
            </tr>
            <tr>
                <th><?php _e('Item("Post")', 'javo_fr'); ?></th>
                <td><?php printf('%s %s', $javo_pay_post->_get('pay_cnt_post'), __('posts', 'javo_fr')); ?></td>
            </tr>
            <tr>
                <th><?php _e('Item("Post Expired")', 'javo_fr'); ?></th>
                <td><?php printf('%s %s', $javo_pay_post->_get('pay_expire_day'), __('days', 'javo_fr')); ?></td>
            </tr>
            <tr>
                <th><?php _e('Pay Type', 'javo_fr'); ?></th>
                <td><?php echo $javo_pay_post->_get('pay_type'); ?></td>
            </tr>
            <tr>
                <th><?php _e('Pay Status', 'javo_fr'); ?></th>
                <td><?php echo get_post_status($post->ID) == "publish" ? "Active" : "Pending"; ?></td>
            </tr>
            <tr>
                <th><?php _e('Pay Price', 'javo_fr'); ?></th>
                <td><?php echo $javo_pay_post->_get('pay_price'); ?></td>
            </tr>
            <tr>
                <th><?php _e('Currency', 'javo_fr'); ?></th>
                <td><?php echo $javo_pay_post->_get('pay_currency'); ?></td>
            </tr>
            <tr>
                <th><?php _e('Pay Process Day', 'javo_fr'); ?></th>
                <td><?php echo $javo_pay_post->_get('pay_day'); ?></td>
            </tr>
            <tr></tr>
        </table>
        <?php
    }

    public function javo_post_meta_box_save($post_id) {

        if (defined("DOING_AUTOSAVE") && DOING_AUTOSAVE) {
            return $post_id;
        };

        /*
         *         Variables Initialize
         *
         * ======================================================================================*
         */
        $javo_query = new javo_ARRAY($_POST);
        $javo_itemlist_query = new javo_ARRAY($javo_query->get('javo_il', Array()));


        /*
         *         Page Template Layout Setup
         *
         * ======================================================================================*
         */
        // Result Save
        if ($javo_query->get('javo_opt_header') != null) {
            update_post_meta($post_id, "javo_header_type", $javo_query->get('javo_opt_header'));
        };
        if ($javo_query->get('javo_opt_fancy') != null) {
            update_post_meta($post_id, "javo_header_fancy_type", $javo_query->get('javo_opt_fancy'));
        };
        if ($javo_query->get('javo_opt_sidebar') != null) {
            update_post_meta($post_id, "javo_sidebar_type", $javo_query->get('javo_opt_sidebar'));
        };
        if ($javo_itemlist_query->get('type', null) != null) {
            update_post_meta($post_id, "javo_item_listing_type", $javo_itemlist_query->get('type'));
        };

        // Fancy options
        if ($javo_query->get('javo_fancy', null) != null) {
            update_post_meta($post_id, "javo_fancy_options", @serialize($javo_query->get('javo_fancy', null)));
        };
        if ($javo_query->get('javo_slide', null) != null) {
            update_post_meta($post_id, "javo_slider_options", @serialize($javo_query->get('javo_slide', null)));
        };
        $javo_controller_setup = !empty($_POST['javo_post_control']) ? @serialize($_POST['javo_post_control']) : "";
        update_post_meta($post_id, "javo_control_options", $javo_controller_setup);

        /*
         *         Set Page Template Default Values
         *
         * ======================================================================================*
         */
        update_post_meta($post_id, "javo_slider_type", $javo_query->get('javo_opt_slider'));
        update_post_meta($post_id, "javo_posts_per_page", $javo_query->get('javo_posts_per_page'));
        update_post_meta($post_id, "javo_item_tax", @serialize((array) $javo_query->get('javo_item_tax')));
        update_post_meta($post_id, "javo_blog_tax", $javo_query->get('javo_blog_tax'));
        update_post_meta($post_id, "javo_item_terms", @serialize((array) $javo_query->get('javo_item_terms')));
        update_post_meta($post_id, "javo_blog_terms", @serialize($javo_query->get('javo_blog_terms', null)));

        /*
         *         Custom Post Types Meta Save
         *
         * ======================================================================================*
         */
        switch (get_post_type($post_id)) {
            case "item":
                $javo_item_query = new javo_ARRAY($javo_query->get('javo_item_attribute', Array()));
                update_post_meta($post_id, "javo_this_featured_item", $javo_item_query->get('featured', ''));


                // item meta
                if (isset($_POST['javo_pt'])) {
                    $ppt_meta = $_POST['javo_pt'];
                    $javo_pt_query = new javo_array($ppt_meta);
                    $latlng = Array("lat" => $ppt_meta['lat'], "lng" => $ppt_meta['lng']);
                    $ppt_images = !empty($_POST['javo_pt_detail']) ? $_POST['javo_pt_detail'] : null;
                    $rel_images = !empty($_POST['javo_pt_related']) ? $_POST['javo_pt_related'] : null;
                    $logo_images = !empty($_POST['javo_pt_logotipo']) ? $_POST['javo_pt_logotipo'] : null;
                    $banner_images = !empty($_POST['javo_pt_banner']) ? $_POST['javo_pt_banner'] : null;

                    $map_area_settings = !empty($ppt_meta['item_map_positon']) ? $ppt_meta['item_map_positon'] : Array();
                    $map_area_settings = @serialize($map_area_settings);
                    $map_type_settings = !empty($ppt_meta['item_map_type']) ? $ppt_meta['item_map_type'] : Array();
                    $map_type_settings = @serialize($map_type_settings);

                    // is Assign ?
                    if ($javo_query->get('item_author') == 'other') {
                        remove_action('save_post', Array($this, 'javo_post_meta_box_save'));
                        $post_id = wp_update_post(Array(
                            'ID' => $post_id
                            , 'post_author' => $javo_query->get('item_author_id')
                        ));
                        add_action('save_post', Array($this, 'javo_post_meta_box_save'));
                    };

                    // Upload Video
                    $javo_video_query = new javo_ARRAY($javo_query->get('javo_video', Array()));
                    $javo_video = null;
                    if ($javo_video_query->get('portal', NULL) != NULL) {
                        switch ($javo_video_query->get('portal')) {
                            case 'youtube': $javo_attachment_video = 'http://www.youtube-nocookie.com/embed/' . $javo_video_query->get('video_id', 0);
                                break;
                            case 'vimeo': $javo_attachment_video = 'http://player.vimeo.com/video/' . $javo_video_query->get('video_id', 0);
                                break;
                            case 'dailymotion': $javo_attachment_video = 'http://www.dailymotion.com/embed/video/' . $javo_video_query->get('video_id', 0);
                                break;
                            case 'yahoo': $javo_attachment_video = 'http://d.yimg.com/nl/vyc/site/player.html#vid=' . $javo_video_query->get('video_id', 0);
                                break;
                            case 'bliptv': $javo_attachment_video = 'http://a.blip.tv/scripts/shoggplayer.html#file=http://blip.tv/rss/flash/' . $javo_video_query->get('video_id', 0);
                                break;
                            case 'veoh': $javo_attachment_video = 'http://www.veoh.com/static/swf/veoh/SPL.swf?videoAutoPlay=0&permalinkId=' . $javo_video_query->get('video_id', 0);
                                break;
                            case 'viddler': $javo_attachment_video = 'http://www.viddler.com/simple/' . $javo_video_query->get('video_id', 0);
                                break;
                        };
                        $javo_video = Array(
                            'portal' => $javo_video_query->get('portal', null)
                            , 'video_id' => $javo_video_query->get('video_id', null)
                            , 'url' => $javo_attachment_video
                            , 'html' => (!empty($javo_attachment_video) ? sprintf('<iframe width="100%%" height="370" src="%s"></iframe>', $javo_attachment_video) : null)
                            , 'single_position' => $javo_video_query->get('single_position', null)
                        );
                    };
                    update_post_meta($post_id, "video", $javo_video);
                    update_post_meta($post_id, "latlng", @serialize($latlng));
                    update_post_meta($post_id, "directory_meta", @serialize($ppt_meta['meta']));
                    update_post_meta($post_id, "detail_images", @serialize($ppt_images));
                    update_post_meta($post_id, "related_images", @serialize($rel_images));
                    update_post_meta($post_id, "logotipo_images", @serialize($logo_images));
                    update_post_meta($post_id, "banner_images", @serialize($banner_images));
                    update_post_meta($post_id, "item_map_positon", $map_area_settings);
                    update_post_meta($post_id, "item_map_type", $map_type_settings);

                    if ($ppt_meta['meta']['exibir_cadernos'] == "0")                    
                        update_post_meta($post_id, "exibir_cadernos", 0);
                    else
                        update_post_meta($post_id, "exibir_cadernos", 1);

                    if ($ppt_meta['meta']['plano_comercial'] == "MINISITE")
                        update_post_meta($post_id, "plano_comercial", 0);
                    else if ($ppt_meta['meta']['plano_comercial'] == "MDVIT")
                        update_post_meta($post_id, "plano_comercial", 1);
                    else if ($ppt_meta['meta']['plano_comercial'] == "MDL")
                        update_post_meta($post_id, "plano_comercial", 2);
                    else if ($ppt_meta['meta']['plano_comercial'] == "MDN")
                        update_post_meta($post_id, "plano_comercial", 3);
                    else
                        update_post_meta($post_id, "plano_comercial", 4);
                };
                break;
            case 'jv_events':
                $javo_event_query = new javo_ARRAY($javo_query->get('javo_event', Array()));
                update_post_meta($post_id, "parent_post_id", $javo_event_query->get('parent_post_id', null));
                update_post_meta($post_id, "brand", $javo_event_query->get('brand', null));
                break;
            case 'review':
                $javo_event_query = new javo_ARRAY($javo_query->get('javo_event', Array()));
                update_post_meta($post_id, "parent_post_id", $javo_event_query->get('parent_post_id', null));
                update_post_meta($post_id, "brand", $javo_event_query->get('brand', null));
                break;
        }; // End Switch
    }
}

new javo_post_meta_box();
