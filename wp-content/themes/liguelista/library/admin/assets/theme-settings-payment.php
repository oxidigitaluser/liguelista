<div class="javo_ts_tab javo-opts-group-tab hidden" tar="payment">
	<h2> <?php _e("Payment Settings", "javo_fr"); ?> </h2>
	<table class="form-table">
	<tr><th>
		<?php _e('Item Payment Setting', 'javo_fr');?>
		<span class="description">
			<?php _e('Choose your item payment setting.', 'javo_fr');?>
		</span>
	</th><td>
		<h4><?php _e('Payment type', 'javo_fr');?></h4>
		<fieldset>
			<label><input type="radio" name="javo_ts[item_publish]" value="" <?php checked($javo_tso->get('item_publish') == "");?>><?php _e('Free', 'javo_fr');?></label>
			<label><input type="radio" name="javo_ts[item_publish]" value="paid" <?php checked($javo_tso->get('item_publish') == "paid");?>><?php _e('Paid', 'javo_fr');?></label>
		</fieldset>

	</td></tr><tr><th>
		<?php _e('Payment System', 'javo_fr');?>
		<span class="description">
			<?php _e('This information is for Paypal payment or direct bank transfer.', 'javo_fr');?>
		</span>
	</th><td>
		<h4><?php _e('Paypal', 'javo_fr');?></h4>
		<fieldset>
			<label>
				<input type="checkbox" name="javo_ts[paypal_enable]" value="use" <?php checked("use" == $javo_tso->get('paypal_enable'));?>">
				<?php _e('Use', 'javo_fr');?>
			</label>
		</fieldset>

		<h4><?php _e('Paypal Mode', 'javo_fr');?></h4>
		<fieldset>
			<label><input type="radio" name="javo_ts[paypal_mode]" value="" <?php checked($javo_tso->get('paypal_mode') == "");?>><?php _e('SendBox (Test)', 'javo_fr');?></label>
			<label><input type="radio" name="javo_ts[paypal_mode]" value="normal" <?php checked($javo_tso->get('paypal_mode') == "normal");?>><?php _e('Real Transaction', 'javo_fr');?></label>
		</fieldset>

		<h4><?php _e('Paypal Required Information', 'javo_fr');?></h4>
		<fieldset style="margin-left:30px;">
			<p>
				<?php _e('Company (work place) Name', 'javo_fr');?>
				<input type="text" name="javo_ts[paypal_company]" value="<?php echo $javo_tso->get('paypal_company');?>" class="large-text">
			</p>
			<p>
				<?php _e('Company (work place) Email', 'javo_fr');?><span class='required'><?php _e('required', 'javo_fr');?></span>
				<input type="text" name="javo_ts[paypal_email]" value="<?php echo $javo_tso->get('paypal_email');?>" class="large-text">
			</p>
			<p>
				<?php _e('Company (work place) Phone', 'javo_fr');?>
				<input type="text" name="javo_ts[paypal_phone]" value="<?php echo $javo_tso->get('paypal_phone');?>" class="large-text">
			</p>
		</fieldset>

		<hr>

		<h4><?php _e('Direct Bank Transfer', 'javo_fr');?></h4>
		<fieldset>
			<label>
				<input type="checkbox" name="javo_ts[bank_enable]" value="use" <?php checked("use" == $javo_tso->get('bank_enable'));?>">
				<?php _e('Use', 'javo_fr');?>
			</label>
		</fieldset>
		<fieldset style="margin-left:30px;">
			<p>
				<?php _e('Account Name', 'javo_fr');?>
				<input type="text" name="javo_ts[account_name]" value="<?php echo $javo_tso->get('account_name');?>" class="large-text">
			</p>
			<p>
				<?php _e('Account Number', 'javo_fr');?>
				<input type="text" name="javo_ts[account_number]" value="<?php echo $javo_tso->get('account_number');?>" class="large-text">
			</p>
			<p>
				<?php _e('Bank Name', 'javo_fr');?>
				<input type="text" name="javo_ts[bank_name]" value="<?php echo $javo_tso->get('bank_name');?>" class="large-text">
			</p>
		</fieldset>
	</td></tr>
	</table>
</div>