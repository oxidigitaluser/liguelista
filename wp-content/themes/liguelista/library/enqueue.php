<?php
add_action('wp_enqueue_scripts', 'javo_load_scripts');

function javo_load_scripts() {
    global $javo_theme_option, $javo_tso;

    $javo_general_styles = Array();
    $javo_assets_styles = Array(
        'home-map-layout.css' => 'javo-home-map-template-layout'
        , '../bootstrap/bootstrap-select.css' => 'bootstrap-select'
        , 'chosen.min.css' => 'jQuery-chosen-autocomplete-style'
        , 'footer-top.css' => 'Footer-top'
        , 'register-component.css' => 'register-component'
        , 'register-base.css' => 'register-base'
        , 'magnific-popup.css' => 'magnific-popup'
        , 'pace-theme-big-counter.css' => 'pace-theme-big-counter'
        , 'bootstrap-markdown.min.css' => 'bootstrap-markdown-style'
        , 'spectrum.css' => 'jquery-spectrum-style'
        , 'jquery.nouislider.min.css' => 'jQuery-nouiSlider-style'
    );
    $javo_single_assets_styles = Array(
        'wide-gallery-component.css' => 'wide-gallery-component'
        , 'wide-gallery-base.css' => 'wide-gallery-base'
        , 'wide-gallery-normalize.css' => 'wide-gallery-normalize'
        , 'single-reviews-style.css' => 'single-reviews-style'
    );
    $javo_assets_header_scripts = Array(
        'gmap3.js' => 'gmap3'
        , 'oms.min.js' => 'oms-same-position-script'
        , 'common.js' => 'Javo-common-script'
        , 'chosen.jquery.min.js' => 'jQuery-chosen-autocomplete'
        , 'jquery-ui.js' => 'jquery-ui'
        , 'jquery.autocomplete.js' => 'jquery.autocomplete'
        , 'jquery.javo.msg.js' => 'javoThemes-Message-Plugin'
        , 'jquery.parallax.min.js' => 'jQuery-Parallax'
        , 'jquery.favorite.js' => 'jQuery-javo-Favorites'
        , 'jquery_javo_search.js' => 'jQuery-javo-search'
        , 'jquery.flexslider-min.js' => 'jQuery-flex-Slider'
        , 'google.map.infobubble.js' => 'Google-Map-Info-Bubble'
        , 'pace.min.js' => 'Pace-Script'
        , 'single-reviews-modernizr.custom.79639.js' => 'single-reviews-modernizr.custom'
        , 'jquery.magnific-popup.js' => 'jquery.magnific-popup'
        , 'jquery.balloon.min.js' => 'jquery.balloon'
    );
    $javo_assets_scripts = Array(
        'bootstrap.min.js' => 'bootstrap'
        , 'jquery.easing.min.js' => 'jQuery-Easing'
        , 'jquery.form.js' => 'jQuery-Ajax-form'
        , 'sns-link.js' => 'sns-link'
        , 'jquery.raty.min.js' => 'jQuery-Rating'
        , 'jquery.spectrum.js' => 'jQuery-Spectrum'
        , 'jquery.parallax.min.js' => 'jQuery-parallax'
        , 'jquery.javo.mail.js' => 'jQuery-javo-Emailer'
        , 'common.js' => 'javo-assets-common-script'
        , 'bootstrap.hover.dropmenu.min.js' => 'bootstrap-hover-dropdown'
        , '../bootstrap/bootstrap-select.js' => 'bootstrap-select-script'
        , 'javo-footer.js' => 'javo-Footer-script'
        , 'bootstrap-markdown.js' => 'bootstrap-markdown'
        , 'bootstrap-markdown.fr.js' => 'bootstrap-markdown-fr'
        , 'jquery.quicksand.js' => 'jQuery-QuickSnad'
        , 'jquery.nouislider.min.js' => 'jQuery-nouiSlider'
        , 'okvideo.min.js' => 'okVideo-Plugin'
        , 'jquery.slight-submenu.min.js' => 'slight-submenu.min-Plugin'
    );

    $javo_single_assets_scripts = Array(
        'single-reviews-slider.js' => 'single-reviews-slider'
        , 'common-single-item.js' => 'common-single-item'
    );

    // Theme Setting > General
    if ($javo_tso->get('smoothscroll', '') == '') {
        $javo_assets_scripts['smoothscroll.js'] = 'smoothscroll';
    };

    wp_enqueue_script('jquery');
    wp_enqueue_script("google_map_API", "http://maps.googleapis.com/maps/api/js?key=AIzaSyBGYqF8DSyUVBdXexuvhk6QQ6gw-EU_WjU&sensor=false", null, "0.0.1", false);

    /** Load Styles * */
    foreach ($javo_general_styles as $src => $id) {
        javo_get_style($src, $id);
    };
    foreach ($javo_assets_styles as $src => $id) {
        javo_get_asset_style($src, $id);
    };
    if (is_single()) {
        foreach ($javo_single_assets_styles as $src => $id) {
            javo_get_asset_style($src, $id);
        };
    }

    /** Load Scripts * */
    foreach ($javo_assets_header_scripts as $src => $id) {
        javo_get_asset_script($src, $id, null, false);
    }
    foreach ($javo_assets_scripts as $src => $id) {
        javo_get_asset_script($src, $id);
    }
    if (is_single()) {
        foreach ($javo_single_assets_scripts as $src => $id) {
            javo_get_asset_script($src, $id, null, false);
        }
    }
    $theme_data = wp_get_theme();
    wp_enqueue_style('javoThemes-directory', get_stylesheet_uri(), array(), $theme_data['Version']);

    // Custom css - Javo themes option
    $javo_upload_path = wp_upload_dir();
    if (
            get_option("javo_themes_settings_css") != "" &&
            file_exists($javo_upload_path['url'] . "/" . basename(get_option("javo_themes_settings_css")))
    ) {
        wp_enqueue_style("javo_drt_custom_style", $javo_upload_path['url'] . "/" . basename(get_option("javo_themes_settings_css")));
    };
}

;

// Google Fonts
add_action('wp_head', 'javo_google_font_apply_callback');

function javo_google_font_apply_callback() {
    global $javo_tso;

    $protocol = is_ssl() ? 'https' : 'http';
    $javo_load_fonts = Array("basic_font", "h1_font", "h2_font", "h3_font", "h4_font", "h5_font", "h6_font");
    foreach ($javo_load_fonts as $index => $font) {
        if ($javo_tso->get($font) == 'Nanum Gothic') {
            wp_enqueue_style("javo-$font-fonts", 'http://fonts.googleapis.com/earlyaccess/nanumgothic.css');
        } elseif ($javo_tso->get($font) != "") {
            wp_enqueue_style("javo-$font-fonts", "$protocol://fonts.googleapis.com/css?family=" . $javo_tso->get($font));
        };
    };
    ?>
    <style type="text/css">
    <?php
    printf("*{ font-family:'%s', sans-seif; }", $javo_tso->get('basic_font', null));
    printf("h1{ font-family:'%s', sans-seif !important; }", $javo_tso->get('h1_font', null));
    printf("h2{ font-family:'%s', sans-seif !important; }", $javo_tso->get('h2_font', null));
    printf("h3{ font-family:'%s', sans-seif !important; }", $javo_tso->get('h3_font', null));
    printf("h4{ font-family:'%s', sans-seif !important; }", $javo_tso->get('h4_font', null));
    printf("h5{ font-family:'%s', sans-seif !important; }", $javo_tso->get('h5_font', null));
    printf("h5{ font-family:'%s', sans-seif !important; }", $javo_tso->get('h6_font', null));
    ?>
    </style>
    <?php
}

;
