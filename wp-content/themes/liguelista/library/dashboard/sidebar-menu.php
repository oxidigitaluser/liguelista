<?php
/**
*** Sidebar Menu
***/
global
	$javo_custom_item_label
	, $javo_custom_item_tab;
?>
	<div class="col-xs-6 col-sm-2 sidebar-offcanvas main-content-left my-page-nav" id="sidebar" role="navigation">
		<p class="visible-xs">
			<button type="button" class="btn btn-primary btn-xs" data-toggle="mypage-offcanvas">
				<i class="glyphicon glyphicon-chevron-left"><?php _e('Close', 'javo_fr');?></i>
			</button>
		</p>
		<div class="well sidebar-nav mypage-left-menu">
			<ul class="nav nav-sidebar">
				<li class="titles"><?php _e('PROFILE', 'javo_fr');?></li>
				<li><a href="<?php echo home_url('member/'.wp_get_current_user()->user_login);?>"><i class="glyphicon glyphicon-cog"></i>&nbsp;<?php _e('Home', 'javo_fr');?></a></li>
				<li><a href="<?php echo home_url('member/'.wp_get_current_user()->user_login.'/'.JAVO_PROFILE_SLUG);?>"><i class="glyphicon glyphicon-cog"></i>&nbsp;<?php _e('Edit My Profile', 'javo_fr');?></a></li>
				<li><a href="<?php echo home_url('member/'.wp_get_current_user()->user_login.'/'.JAVO_LOSTPW_SLUG);?>"><i class="glyphicon glyphicon-cog"></i>&nbsp;<?php _e('Change Password', 'javo_fr');?></a></li>
			</ul>
			<ul class="nav nav-sidebar">
				<li class="titles"><?php _e('My Shops', 'javo_fr');?></li>
				<li><a href="<?php echo home_url('member/'.wp_get_current_user()->user_login.'/'.JAVO_FAVORITIES_SLUG);?>"><i class="glyphicon glyphicon-cog"></i>
				<?php _e('Saved Shops', 'javo_fr');?></a></li>
				<?php if( $javo_custom_item_tab->get('reviews', '') == '' ):?>
					<li><a href="<?php echo home_url('member/'.wp_get_current_user()->user_login.'/'.JAVO_REVIEWS_SLUG);?>"><i class="glyphicon glyphicon-cog"></i>
					<?php printf( __('My %s', 'javo_fr'), $javo_custom_item_label->get('reviews', __('Reviews', 'javo_fr')));?></a></li>
				<?php endif; ?>
				<?php if( $javo_custom_item_tab->get('ratings', '') == '' ):?>
					<li><a href="<?php echo home_url('member/'.wp_get_current_user()->user_login.'/'.JAVO_RATINGS_SLUG);?>"><i class="glyphicon glyphicon-cog"></i>
					<?php printf( __('My %s', 'javo_fr'), $javo_custom_item_label->get('ratings', __('Ratings', 'javo_fr')));?></a></li>
				<?php endif; ?>
				<?php if( $javo_custom_item_tab->get('reviews', '') == '' ):?>
				<li><a href="<?php echo home_url('member/'.wp_get_current_user()->user_login.'/'.JAVO_ADDREVIEW_SLUG);?>"><i class="glyphicon glyphicon-pencil"></i>
					<?php printf( __('Add %s', 'javo_fr'), $javo_custom_item_label->get('reviews', __('Reviews', 'javo_fr')));?></a></li>
				<?php endif; ?>
			</ul>
			<ul class="nav nav-sidebar">
				<li class="titles"><?php _e('Listing Menu', 'javo_fr');?></li>
				<li><a href="<?php echo home_url('member/'.wp_get_current_user()->user_login.'/'.JAVO_ADDITEM_SLUG);?>"><i class="glyphicon glyphicon-pencil"></i>
					<?php _e('Post an Item', 'javo_fr');?>
				</a></li>
				<?php if( $javo_custom_item_tab->get('events', '') == '' ):?>
					<li><a href="<?php echo home_url('member/'.wp_get_current_user()->user_login.'/'.JAVO_ADDEVENT_SLUG);?>"><i class="glyphicon glyphicon-pencil"></i>
					<?php printf( __('Post an %s', 'javo_fr'), $javo_custom_item_label->get('event', __('Event', 'javo_fr')));?></a></li>
				<?php endif; ?>
				<li><a href="<?php echo home_url('member/'.wp_get_current_user()->user_login.'/'.JAVO_ITEMS_SLUG);?>"><i class="glyphicon glyphicon-cog"></i>&nbsp;<?php _e('My Posted Items', 'javo_fr');?></a></li>
				<?php if( $javo_custom_item_tab->get('events', '') == '' ):?>
					<li><a href="<?php echo home_url('member/'.wp_get_current_user()->user_login.'/'.JAVO_EVENTS_SLUG);?>"><i class="glyphicon glyphicon-cog"></i>
					<?php printf( __('My %s', 'javo_fr'), $javo_custom_item_label->get('events', __('Events', 'javo_fr'))); printf(' %s', __('List', 'javo_fr'));?></a></li>
				<?php endif; ?>
				<li class="javo-my-menu-payment"><a href="<?php echo home_url('member/'.wp_get_current_user()->user_login.'/'.JAVO_PAYMENT_SLUG);?>"><i class="glyphicon glyphicon-cog"></i>&nbsp;<?php _e('Payment History', 'javo_fr');?></a></li>
			</ul>
		</div><!--/.well -->
	</div><!--/col-xs-->