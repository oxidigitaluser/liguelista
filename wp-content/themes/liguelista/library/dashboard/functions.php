<?php
class javo_dashboard{
	static $pages;
	public function __construct(){
		add_filter('template_include', Array(__class__, 'javo_dashboard_template'), 99);
		add_action('admin_init', Array(__class__, 'javo_rewrite_callback'));
		add_filter( 'query_vars',  Array(__class__, 'javo_dashboard_page_query_add_callback' ));
		self::$pages = Array(
			'JAVO_MEMBER_SLUG'			=>'member'
			, 'JAVO_PROFILE_SLUG'		=>'edit-my-profile'
			, 'JAVO_FAVORITIES_SLUG'	=>'favorites'
			, 'JAVO_LOSTPW_SLUG'		=>'lost-password'
			, 'JAVO_ITEMS_SLUG'			=>'my-item-list'
			, 'JAVO_EVENTS_SLUG'		=>'events'
			, 'JAVO_RATINGS_SLUG'		=>'rating'
			, 'JAVO_REVIEWS_SLUG'		=>'reviews'
			, 'JAVO_ADDITEM_SLUG'		=>'add-item'
			, 'JAVO_ADDEVENT_SLUG'		=>'add-event'
			, 'JAVO_ADDREVIEW_SLUG'		=>'add-review'
			, 'JAVO_PAYMENT_SLUG'		=>'my-payments'
			, 'JAVO_MANAGE_SLUG'		=>'manage'
		);
		foreach( self::$pages as $key => $value ){
			define( $key, $value);		
		}
	}
	static function javo_dashboard_template($template){
		global $wp_query;

		if( get_query_var('pagename') == 'member' )
		{
			/*$javo_this_get_user = get_user_by('login', get_query_var('user'));

			if( !empty( $javo_this_get_user ) )
			{
				if( in_Array( get_query_var('sub_page'), self::$pages ) )
				{
					add_action( 'wp_enqueue_scripts', Array(__class__, 'wp_media_enqueue_callback'));					
					add_filter( 'wp_title', Array(__class__, 'javo_dashbarod_set_title_callback'));
					return JAVO_DSB_DIR.'/mypage-'.get_query_var('sub_page').'.php';
				}
				else
				{
					add_filter( 'wp_title', Array(__class__, 'javo_dashbarod_set_title_callback'));
					return JAVO_DSB_DIR.'/mypage-member.php';
				}
				add_filter( 'body_class', Array(__class__, 'javo_dashboard_bodyclass_callback'));
			}
			else
			{
				return JAVO_DSB_DIR.'/mypage-no-user.php';
			}
			*/
		}
		return $template;
	}
	static function javo_dashbarod_set_title_callback(){
		$javo_this_return = '';
		switch( get_query_var('sub_page') ){
			case 'edit-my-profile':	$javo_this_return = __('Edit My Profile', 'javo_fr'); break;
			case 'favorites':		$javo_this_return = __('Saved Shop', 'javo_fr'); break;
			case 'lost-password':	$javo_this_return = __('Change Password', 'javo_fr'); break;
			case 'my-item-list':	$javo_this_return = __('My Posted Items', 'javo_fr'); break;
			case 'events':			$javo_this_return = __('My Event List', 'javo_fr'); break;
			case 'rating':			$javo_this_return = __('My Ratings', 'javo_fr'); break;
			case 'review':			$javo_this_return = __('My Reviews', 'javo_fr'); break;
			case 'add-item':		$javo_this_return = __('Post an Item', 'javo_fr'); break;
			case 'add-event':		$javo_this_return = __('Post an Event', 'javo_fr'); break;
			case 'add-review':		$javo_this_return = __('Add Reviews', 'javo_fr'); break;
			case 'my-payments':		$javo_this_return = __('Payment History', 'javo_fr'); break;
			//case 'manage':		$javo_this_return = __('Edit My Page', 'javo_fr'); break;
			case 'member': default:	$javo_this_return = __('Home', 'javo_fr');
		}
		$javo_this_return = $javo_this_return ." | ";
		return $javo_this_return . __('My Dashboard', 'javo_fr').' | '.get_bloginfo('name') ;
	}
	static function javo_dashboard_bodyclass_callback($classes){
		if( !is_admin_bar_showing() ){ return $classes; };
		$classes[] = 'admin-bar';
		return $classes;

	}
	static function wp_media_enqueue_callback(){
		wp_enqueue_media();
	}
	static function javo_dashboard_page_query_add_callback($q){
		$q[] = 'user';
		$q[] = 'sub_page';
		return $q;
	}
	/**

	Action : admin_init
	javo_rewrite_callback
	**/
	static function javo_rewrite_callback()
	{

		/*
		
		add_rewrite_rule( '^member/([^/]*)/?$'					, 'index.php?page_id=3410&user=$matches[1]', 'top');
		add_rewrite_rule( '^member/([^/]*)/([^/]*)/?$'			, 'index.php?page_id=3410&user=$matches[1]&sub_page=$matches[2]', 'top');
		add_rewrite_rule( '^member/([^/]*)/([^/]*)/page/([^/]*)/?$'	, 'index.php?page_id=3410&user=$matches[1]&sub_page=$matches[2]&paged=$matches[3]', 'top');

		*/

		//add_rewrite_rule( '^member/([^/]*)/?$'						, 'index.php?pagename=member&user=$matches[1]', 'top');
		//add_rewrite_rule( '^member/([^/]*)/([^/]*)/?$'				, 'index.php?pagename=member&user=$matches[1]&sub_page=$matches[2]', 'top');
		//add_rewrite_rule( '^member/([^/]*)/([^/]*)/page/([^/]*)/?$'	, 'index.php?pagename=member&user=$matches[1]&sub_page=$matches[2]&paged=$matches[3]', 'top');

		/*
		foreach( self::$pages as $page){
			add_rewrite_rule( '^'.$page.'/([^/]*)/?$', 'index.php?pagename='.$page.'&user=$matches[1]', 'top');
			add_rewrite_rule( '^'.$page.'/([^/]*)/page/([^/]*)/?', 'index.php?pagename='.$page.'&user=$matches[1]&paged=$matches[2]', 'top');
			/*
			add_rewrite_rule( '([^/]*)/^'.$page.'/([^/]*)/?$', 'index.php?pagename='.$page.'&lang=$matches[1]&user=$matches[2]', 'top');
			add_rewrite_rule( '([^/]+)/([^/]+)/?$', 'index.php?pagename='.$page.'&ss=$matches[1]&cc=$matches[2]&lang=$matches[1]', 'top');
			* /
		};*/
	}

}
new javo_dashboard();