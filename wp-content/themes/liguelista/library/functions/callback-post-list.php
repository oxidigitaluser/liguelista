<?php
// Post, item get lists.
add_action('wp_ajax_nopriv_post_list', 'javo_post_list_callback');
add_action('wp_ajax_post_list', 'javo_post_list_callback');

function javo_post_list_callback() {

    // Wordpress Queries
    global
    $javo_tso
    , $javo_favorite;

    $javo_query = new javo_ARRAY($_POST);

    if ($javo_query->get('lang', null) != null) {
        global $sitepress;
        if (!empty($sitepress)) {
            $sitepress->switch_lang($javo_query->get('lang'), true);
        };
    };

    // List view tyope
    $mode = $javo_query->get('type', null);
    $post_type = $javo_query->get('post_type', 'item');
    $p = $javo_query->get('post_id', null);
    $ppp = $javo_query->get('ppp', 10);
    $page = $javo_query->get('page', 1);
    $tax = $javo_query->get('tax', 10);
    $term_meta = $javo_query->get('term_meta', 10);

    $meta = Array();

    // $args = Array(
    //     'post_type' => $post_type
    //     , 'post_status' => 'publish'
    //     , 'posts_per_page' => $ppp
    //     , 'paged' => $page
    //     , 'order' => 'ASC'
    //     , 'orderby' => 'meta_value title'
    //     , 'meta_key' => 'plano_comercial'
    // );

    $args = Array(
        'post_type' => $post_type
        , 'post_status' => 'publish'
        , 'posts_per_page' => $ppp
        , 'paged' => $page
        , 'order' => 'ASC'
        , 'orderby' => 'meta_value title'
        , 'meta_query' => array(
            array('key' => 'plano_comercial'),
            array('key' => 'exibir_cadernos', 'compare' => '=', 'value' => '1'))
        );


/*
    if ((int) $p > 0)
        $args['p'] = $p;
*/
        if ($tax != NULL && is_Array($tax)) {
            foreach ($tax as $key => $value) {
                if ($value != "") {
                    $args['tax_query']['relation'] = "AND";
                    $args['tax_query'][] = Array(
                        "taxonomy" => $key,
                        "field" => "term_id",
                        "terms" => $value
                        );
                };
            };
        };

        if ($term_meta != NULL && is_Array($term_meta)) {
            foreach ($term_meta as $key => $value) {
                if ($value != "") {
                    $args['meta_query']['relation'] = "AND";
                    $args['meta_query'][] = Array(
                        "key" => $key,
                        "value" => (int) $value,
                        "compare" => ">="
                        );
                };
            };
        };
        $args["s"] = $javo_query->get('keyword', null);

    // Not found image featured url
        $noimage = JAVO_IMG_DIR . "/no-image.png";
        ob_start();
        ?>
        <script type="text/javascript">
        if (typeof (window.jQuery) != "undefined")
        {
            jQuery(document).ready(function ($)
            {
                "use strict";
                $(".javo_hover_body").css({
                    "position": "absolute",
                    "top": "0",
                    "left": "0",
                    "z-index": "2",
                    "width": "100%",
                    "height": "100%",
                    "padding": "10px",
                    "margin": "0px",
                    "backgroundColor": "rgba(0, 0, 0, 0.4)",
                    "display": "none"
                });
                $(".javo_img_hover")
                .css({
                    "position": "relative", "overflow": "auto", "display": "inline-block"
                }).hover(function () {
                    $(this).find(".javo_hover_body").fadeIn("fast");
                }, function () {
                    $(this).find(".javo_hover_body").clearQueue().fadeOut("slow");
                });
            });
        }
        ;
        </script>
        <?php
        switch ($mode) {
            case 2: $posts = new WP_Query($args); ?>
            <div class="row javo-item-grid-listing">
                <?php
                $i = 0;
                ## Thumbnail Type ###################
                if ($posts->have_posts()) {
                    while ($posts->have_posts()) {
                        $posts->the_post();
                        $javo_meta_query = new javo_GET_META(get_the_ID());
                        $meta = Array(
                            'strong' => $javo_meta_query->cat('item_category', __('No Category', 'javo_fr'))
                            , "featured" => $javo_meta_query->cat('item_location', __('No Location', 'javo_fr'))
                            );
                            ?>
                            <div class="col-md-4 col-sm-6 col-xs-6 pull-left">
                                <div class="panel panel-default panel-relative">
                                    <?php
                                    $detail_images = (Array) @unserialize(get_post_meta(get_the_ID(), "detail_images", true));
                                    $detail_images[] = get_post_thumbnail_id(get_the_ID());
                                    if (!empty($detail_images)):
                                        echo '<div class="javo_detail_slide">';
                                    $javo_this_image_meta = wp_get_attachment_image_src($detail_images, 'large');
                                    $javo_this_image = $javo_this_image_meta[0];
                                    ?>

                                    <?php
                                    echo '<ul class="slides list list-unstyled">';
                                    foreach ($detail_images as $index => $image) {
                                        $javo_this_image_meta = wp_get_attachment_image_src($image, 'full');
                                        $javo_this_image = $javo_this_image_meta[0];
                                        ?>
                                        <li>
                                            <u href="<?php echo $javo_this_image; ?>" style="cursor:pointer;">
                                                <?php
                                                $javo_this = wp_get_attachment_image($image, "javo-box-v", false);
                                                if ($javo_this) {
                                                    echo $javo_this;
                                                } else {
                                                    printf('<img src="%s" class="img-responsive wp-post-image" style="width:100%%; height:219px;">', $javo_tso->get('no_image', JAVO_IMG_DIR . '/no-image.png'));
                                                };
                                                ?>
                                            </u>
                                        </li>
                                        <?php
                                    };
                                    echo '</ul>';
                                    echo '</div>';
                                    endif;
                                    ?>
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <a href="<?php the_permalink(); ?>" class="javo-tooltip javo-igl-title"  title="<?php the_title(); ?>">
                                                <div>
                                                    <h2 class="panel-title text-center">
                                                        <strong> <?php echo javo_str_cut(get_the_title(), 120); ?> </strong>
                                                    </h2>
                                                    <div class="text-center"><?php echo $meta['strong']; ?></div>
                                                </div>
                                            </a>
                                            <ul class="list list-unstyled">
                                                <li class="text-center text-excerpt javo-grid-listing-excerpt"><?php echo javo_str_cut(get_the_excerpt(), 150); ?></li>
                                                <li class="text-center">
                                                    <h2 class="javo-sns-wrap social-wrap">
                                                        <i class="sns-facebook" data-title="<?php the_title(); ?>" data-url="<?php the_permalink(); ?>">
                                                            <a class="facebook javo-tooltip" title="<?php _e('Share Facebook', 'javo_fr'); ?>"></a>
                                                        </i>
                                                        <i class="sns-twitter" data-title="<?php the_title(); ?>" data-url="<?php the_permalink(); ?>">
                                                            <a class="twitter javo-tooltip" title="<?php _e('Share Twitter', 'javo_fr'); ?>"></a>
                                                        </i>
                                                        <i class="sns-heart">
                                                            <a class="javo-tooltip favorite javo_favorite<?php echo $javo_favorite->on(get_the_ID(), ' saved'); ?>"  data-post-id="<?php the_ID(); ?>" title="<?php _e('Add My Favorite', 'javo_fr'); ?>"></a>
                                                        </i>
                                                    </h2>
                                                </li>
                                            </ul><!-- List group -->
                                        </div>
                                    </div>
                                    <div class="javo-left-overlay">
                                        <div class="javo-txt-meta-area admin-color-setting">
                                            <?php echo $meta['featured']; ?>
                                        </div> <!-- javo-txt-meta-area -->
                                        <div class="corner-wrap">
                                            <div class="corner admin-color-setting"></div>
                                            <div class="corner-background admin-color-setting"></div>
                                        </div> <!-- corner-wrap -->
                                    </div><!-- javo-left-overlay -->
                                </div> <!-- /.panel -->
                            </div><!-- /.col-sm-4 -->
                            <?php
                            $i++;
                            echo $i % 3 == 0 ? '<p style="clear:both;"></p>' : '';
                    }; // End While
                }; // End If
                break;
                case 4:
                $posts = new WP_Query($args);
                $added = array();

                //$locationPage = $javo_query->get("tax", null)["item_location"];
                //$categoryPage = $javo_query->get("tax", null)["item_category"];
                $javo_item_filter_terms = @unserialize(get_post_meta($p, "javo_item_terms", true));

                if (!empty($javo_item_filter_terms) && !empty($categoryPage)) {

                    if (empty($locationPage)) {
                        $locationPage = '';
                    } else {
                        $locationPage = $locationPage[0];
                    }
                    echo '<div name="cut-category-page">';

                    foreach ($javo_item_filter_terms['tax1'] as $index) {
                        $cat = get_term($index, 'item_category');
                        $url = "http://" . $_SERVER['HTTP_HOST'] . "/?post_type=item&category=" . $cat->term_id . "&location=" . $locationPage . "&s=";
                        ?>
                        <div class="col-subcategorias">
                            <li class="lista-subcategorias">
                                <a href="<?php echo $url; ?>">
                                    <h2 class="titulo-lista-subcategorias">
                                        <?php echo $cat->name; ?>
                                    </h2>
                                </a>
                            </li>
                        </div>
                        <?php
                    }

                    echo "</div>";
                };

                if ($posts->have_posts()) {
                    while ($posts->have_posts()) {
                        $posts->the_post();
                        $javo_this_latlng = @unserialize(get_post_meta(get_the_ID(), 'latlng', true));
                        $plano_comercial = get_post_meta(get_the_ID(), "plano_comercial", true);
                        $javo_this_posts_return[get_the_ID()]['latlng'] = $javo_this_latlng;
                        $javo_meta_query = new javo_get_meta(get_the_ID());
                        $javo_set_icon = '';
                        $javo_marker_term_id = wp_get_post_terms(get_the_ID(), 'item_category');
                        $javo_item_location = wp_get_post_terms(get_the_ID(), 'item_location', array('order' => 'DESC', 'orderby' => 'parent'));
                        $logotipo = (Array) @unserialize(get_post_meta(get_the_ID(), "logotipo_images", true));
                        $url = wp_get_attachment_image_src($logotipo[0], Array(130, 130));

                        foreach ($javo_marker_term_id as $cate) {
                            if (search_array($cate->name, $added) || $cate->term_id == 1015)
                                continue;

                            array_push($added, array("nome" => $cate->name, "codigo" => $cate->term_id));
                        }

                        if (!empty($javo_marker_term_id)) {
                            $javo_set_icon = get_option('javo_item_category_' . $javo_marker_term_id[0]->term_id . '_marker', '');
                            if ($javo_set_icon == '') {
                                $javo_set_icon = $javo_tso->get('map_marker', '');
                            };
                            $javo_this_posts_return[get_the_ID()]['icon'] = $javo_set_icon;
                        };

                        /* PLANO MINISITE */
                        if ($plano_comercial == '0') { //MINISITE
                            get_template_part('content', 'archive');
                        } else if ($plano_comercial == '1') { //MDVIT
                            get_template_part('content', 'mdvit');
                        } else if ($plano_comercial == '2') { //MDL
                            get_template_part('content', 'mdl');
                        } else if ($plano_comercial == '3') { //MDN
                            get_template_part('content', 'mdn');
                        } else {
                            get_template_part('content', 'free');
                        }
                        ?>
                        <script type="text/template" id="javo_map_tmp_<?php the_ID(); ?>">
                        <div class="javo_somw_info panel">
                        <div class="des">
                        <h5><?php echo javo_str_cut(get_the_title(), 30); ?></h5>
                        <ul class="list-unstyled">

                        <?php if ($javo_meta_query->get('phone1') != '') { ?>
                            <li><?php
                            echo ('(');
                                echo $javo_meta_query->get('ddd1');
                                echo (')');
                                echo (' ');
                                echo $javo_meta_query->get('phone1');
                                ?></li>
                                <?php } if ($javo_meta_query->get('address') != '') { ?>
                                    <li><?php echo $javo_meta_query->get('address'); ?></li>
                                    <?php } if ($javo_meta_query->cat('item_location') != '') { ?>
                                        <li><?php echo $javo_item_location[0]->name . " - " . get_sigla_estado($javo_item_location[1]->name); ?></li>
                                        <?php } ?>
                                        </ul>
                                        </div>

                                        <div class="pics">
                                        <div class="thumb">

                                        <?php
                                        if ($logotipo[0]) {
                                            printf('<img src="%s" class="wp-post-image" style="width:130px; height:130px;">', $url[0]);
                                        } else {
                                            printf('<img src="%s" style="width:130px; height:130px;">', $javo_tso->get('no_image', JAVO_IMG_DIR . '/no-image.png'));
                                        };
                                        ?>

                                        </div> <!-- thumb -->
                                        <div class="img-in-text"><?php echo $javo_meta_query->cat('item_category', __('Sem Categoria', 'javo_fr')); ?></div>
                                        <div class="javo-left-overlay">
                                        <div class="javo-txt-meta-area"><?php echo $javo_meta_query->cat('item_location', __('Sem Localização', 'javo_fr')); ?></div>
                                        <div class="corner-wrap">
                                        <div class="corner"></div>
                                        <div class="corner-background"></div>
                                        </div> <!-- corner-wrap -->
                                        </div> <!-- javo-left-overlay -->
                                        </div> <!-- pic -->
                                        </div> <!-- javo_somw_info -->
                                        </script>
                                        <?php
                    }; // End While
                    
                    echo '<div name="cut-category"><ul id="triple" style="padding: 0px;">';

                    asort($added);

                    foreach ($added as $category2) {
                        $urlNova = "http://" . $_SERVER['HTTP_HOST'] . "/?post_type=item&category=" . $category2["codigo"] . "&location=" . $locationPage[0] . "&s=";
                        
                        echo "<li><a href='" . $urlNova . "'>" . $category2["nome"] . "</a></li>";
                    }
                    echo '</ul></div>';
                }; // End If
                break;
                case 11:
                $posts = new WP_Query($args);
                $javo_variable_integer = 0;
                ?>
                <div class="row" id="box-listing">
                    <?php
                    if ($posts->have_posts()) {
                        while ($posts->have_posts()) {
                            $posts->the_post();
                            $javo_meta_query = new javo_GET_META(get_the_ID());
                            $javo_meta_string = Array(
                                'strong' => get_comments_number(get_the_ID())
                                , 'featured' => $javo_meta_query->cat('category', __('No Category', 'javo_fr'))
                                , 'type' => sprintf('<span class="glyphicon glyphicon-user"></span> %s ( %s )'
                                    , get_the_author_meta('display_name')
                                    , esc_attr(get_the_date('M d, Y'))
                                    )
                                );
                                ?>
                                <div class="col-md-6 col-sm-6 col-xs-6 box-wraps">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="main-box item-wrap1">
                                                <div class="row blog-wrap-inner">
                                                    <div class="col-md-5 col-sm-5 col-xs-5 img-wrap">
                                                        <div class="javo_img_hover">
                                                            <?php
                                                            if (has_post_thumbnail()) {
                                                                the_post_thumbnail('javo-box', Array('class' => 'img-responsive', 'style' => 'width:100%;'));
                                                            } else {
                                                                printf('<img src="%s" class="img-responsive wp-post-image">', $javo_tso->get('no_image', JAVO_IMG_DIR . '/no-image.png'));
                                                            };
                                                            ?>
                                                        </div> <!-- javo_img_hover -->
                                                    </div> <!-- col-md-5 -->
                                                    <div class="col-md-7 col-sm-7 col-xs-7">
                                                        <div class="detail">
                                                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                            <p class="expert"><?php echo javo_str_cut(get_the_excerpt(), 120); ?></p>
                                                        </div>
                                                    </div> <!-- col-md-7 -->
                                                </div> <!-- row -->
                                            </div> <!-- main-box -->
                                        </div><!-- Panel Body -->
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <div class="row">
                                                    <div class="col-md-9 col-sm-9 col-xs-8 javo-box-listing-meta">
                                                        <?php echo $javo_meta_string['type']; ?>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3 col-xs-4 text-right">
                                                        <span class="javo-sns-wrap social-wrap">
                                                            <i class="sns-facebook" data-title="<?php the_title(); ?>" data-url="<?php the_permalink(); ?>">
                                                                <a class="facebook javo-tooltip" title="<?php _e('Share Facebook', 'javo_fr'); ?>"></a>
                                                            </i>
                                                            <i class="sns-twitter" data-title="<?php the_title(); ?>" data-url="<?php the_permalink(); ?>">
                                                                <a class="twitter javo-tooltip" title="<?php _e('Share Twitter', 'javo_fr'); ?>"></a>
                                                            </i>
                                                        </span>
                                                    </div> <!-- col-6-md -->
                                                </div><!-- row -->
                                            </li>
                                        </ul>
                                        <div class="javo-left-overlay">
                                            <div class="javo-txt-meta-area admin-color-setting">
                                                <?php echo $javo_meta_string['featured']; ?>
                                            </div> <!-- javo-txt-meta-area -->
                                            <div class="corner-wrap">
                                                <div class="corner"></div>
                                                <div class="corner-background"></div>
                                            </div> <!-- corner-wrap -->
                                        </div><!-- javo-left-overlay -->
                                    </div><!-- Panel Wrap -->
                                </div> <!-- col-lg-6 -->
                                <?php
                                $javo_variable_integer++;
                                echo $javo_variable_integer % 2 == 0 ? '<p class="clearfix"></p>' : '';
                        }; // End While
                    }; // End IF
                    ?>
                </div>
                <?php
                break;
                case "listers": $posts = query_posts($args);
                ## Blog Calender Type #################
                foreach ($posts as $post): setup_postdata($post);
                $javo_list_str = new get_char($post);
                $author = get_userdata($post->post_author);
                if (has_post_thumbnail($post->ID)) {
                    $thumbnail = get_the_post_thumbnail($post->ID, "javo-avatar");
                } else {
                    $thumbnail = $noimage;
                };
                ?>
                <!--<div class="row">-->
                <div class="col-md-3" id="listers-box-list">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="lister-pic">
                                <a href="<?php echo get_permalink($post->ID); ?>">
                                    <?php
                                    $img_src = wp_get_attachment_image_src(get_user_meta($author->ID, "avatar", true), "javo-avatar");
                                    if ($img_src != "")
                                        printf("<img src='%s'>", $img_src[0]);
                                    ?>
                                </a>
                                <div class="text-rb-meta">
                                    <?php
                                    printf("%s %s"
                                        , number_format($javo_list_str->get_author_item_count())
                                        , __("items", "javo_fr")
                                        );
                                        ?>
                                    </div>
                                </div>
                            </div> <!-- panel-heading -->
                            <ul class="list-group">
                                <li class="list-group-item"><h3 class="panel-title"><span class="glyphicon glyphicon-user"></span>&nbsp;
                                    <?php echo $author->first_name; ?>&nbsp;<?php echo $author->last_name; ?></h3></li>
                                    <li class="list-group-item"><?php echo javo_str_cut($javo_list_str->a_meta('description'), 130); ?></li>
                                    <li class="list-group-item">
                                        <a href="http://facebook.com/<?php echo $javo_list_str->a_meta('facebook'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="http://twitter.com/<?php echo $javo_list_str->a_meta('twitter'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                                    </li>
                                </ul><!-- List group -->
                            </div><!-- panel -->
                        </div> <!-- col-md-3 -->
                        <?php
                        endforeach;
                        break;
                        case "new_list": $posts = new WP_Query($args);
                        ?>
                        <div class="body-content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- grid/list -->
                                        <div id="products" class="row list-group">
                                            <?php
                                            if ($posts->have_posts()) {
                                                while ($posts->have_posts()) {
                                                    $posts->the_post();
                                                    $javo_this_author_avatar_id = get_the_author_meta('avatar');
                                                    $javo_rating = new javo_rating(get_the_ID());
                                                    ?>
                                                    <div class="item col-md-4 javo-animation x1 javo-left-to-right-100">

                                                        <div class="panel panel-default  item-list-box-listing">
                                                            <div class="panel-body">
                                                                <div class="main-box blog-wrap1">
                                                                    <div class="row blog-wrap-inner">
                                                                        <div class="col-md-12img-wrap">
                                                                            <div class="javo_img_hover" style="position: relative; display: inline-block;">
                                                                                <a href="<?php the_permalink(); ?>">
                                                                                    <?php
                                                                                    if (has_post_thumbnail()) {
                                                                                        the_post_thumbnail('javo-box-v', Array('class' => 'group list-group-image item-thumbs img-responsive'));
                                                                                    };
                                                                                    ?>
                                                                                    <div class="img-on-ratings text-right">
                                                                                        <div class="javo-rating-registed-score" data-score="<?php echo $javo_rating->parent_rating_average; ?>"></div>
                                                                                    </div>

                                                                                    <div class="javo_hover_body" style="position: absolute; top: 0px; left: 0px; z-index: 2; width: 100%; height: 100%; padding: 10px; margin: 0px; display: none; opacity: 0.3151841979620183; background-color: rgba(0, 0, 0, 0.4);">
                                                                                        <div class="javo_hover_content" style="cursor: pointer; display: table; width: 100%; height: 100%;" data-href="<?php the_permalink(); ?>">
                                                                                            <div class="javo_hover_content_link" style="display: table-cell; color: rgb(239, 239, 239); vertical-align: middle; font-size: 2em; text-align: center;"><i class="glyphicon glyphicon-plus"></i></div></div> <!-- javo_hover_content -->
                                                                                        </div> <!-- javo_hover_body -->
                                                                                    </a>
                                                                                </div> <!-- javo_img_hover -->
                                                                            </div> <!-- col-md-12 -->
                                                                        </div> <!-- row -->
                                                                    </div> <!-- main-box -->
                                                                </div><!-- Panel Body -->
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">
                                                                        <div class="row">
                                                                            <div class="col-md-8">
                                                                                <?php echo javo_str_cut(get_the_title(), 20); ?>
                                                                            </div>
                                                                            <div class="col-md-4 text-right">
                                                                                <div class="social-wrap javo-type-11-share">
                                                                                    <span class="javo-sns-wrap">
                                                                                        <i class="sns-facebook" data-title="<?php the_title(); ?>" data-url="<?php the_permalink(); ?>">
                                                                                            <a class="facebook"></a>
                                                                                        </i>
                                                                                        <i class="sns-twitter" data-title="<?php the_title(); ?>" data-url="<?php the_permalink(); ?>">
                                                                                            <a class="twitter"></a>
                                                                                        </i>
                                                                                        <i class="sns-heart">
                                                                                            <a class="favorite javo_favorite<?php echo $javo_favorite->on(get_the_ID(), ' saved'); ?>"  data-post-id="<?php the_ID(); ?>"></a>
                                                                                        </i>
                                                                                    </span>
                                                                                </div> <!-- sc-social-wrap -->
                                                                            </div> <!-- col-6-md -->
                                                                        </div><!-- row -->
                                                                    </li> <!-- list-group-item -->
                                                                </ul> <!-- list-group -->

                                                                <div class="panel-footer options-wrap">
                                                                    <div class="row">
                                                                        <ul class="options">
                                                                            <li class="col-md-6 col-sm-6 col-xs-6"><i class="javo-con category"></i>&nbsp;<?php echo javo_get_cat(get_the_ID(), 'item_category', __('No Category', 'javo_fr')); ?></li>
                                                                            <li class="col-md-6 col-sm-6 col-xs-6 text-right"><i class="javo-con location"></i>&nbsp;<?php echo javo_get_cat(get_the_ID(), 'item_location', __('No Location', 'javo_fr')); ?></li>
                                                                        </ul>
                                                                    </div>
                                                                </div><!-- panel-footer -->
                                                                <div class="javo-left-overlay">
                                                                    <div class="javo-txt-meta-area">
                                                                        <?php echo javo_get_cat(get_the_ID(), 'item_category', __('No Category', 'javo_fr')); ?>
                                                                    </div> <!-- javo-txt-meta-area -->
                                                                    <div class="corner-wrap">
                                                                        <div class="corner"></div>
                                                                        <div class="corner-background"></div>
                                                                    </div> <!-- corner-wrap -->
                                                                </div><!-- javo-left-overlay -->
                                                            </div> <!-- panel -->
                                                        </div> <!-- item col-md-4 -->
                                                        <?php
                                        }; // Close while
                                    } else {
                                        
                                    }; // Emd While
                                    ?>
                                    <!-- // grid/list -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    break;
                    default:
                    _e('Error', 'javo_fr');
                };

            $big = 999999999; // need an unlikely integer
            wp_reset_query();
            echo "<div class='javo_pagination'>" . paginate_links(array(
                'base' => "%_%",
                'format' => '?paged=%#%',
                'current' => $page,
                'total' => $posts->max_num_pages
                )) . "</div>";

            $content = ob_get_clean();

            // Markers
            $javo_this_info_window_contents = new WP_Query($args);
            $markers = Array();
            if ($javo_this_info_window_contents->have_posts()) {
                while ($javo_this_info_window_contents->have_posts()) {
                    $javo_this_info_window_contents->the_post();
                    $javo_meta_query = new javo_GET_META(get_the_ID());
                    $javo_this_author_avatar_id = get_the_author_meta('avatar');
                    $javo_this_author_name = sprintf('%s %s', get_the_author_meta('first_name'), get_the_author_meta('last_name'));
                    ob_start();
                    ?>
                    <div class="javo_somw_info">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <h4><?php the_title(); ?></h4>
                                <ul class="list-unstyled">
                                    <li><div class="prp-meta"><?php echo $javo_meta_query->get('phone'); ?>
                                        <li><div class="prp-meta"><?php echo $javo_meta_query->get('mobile'); ?>
                                            <li><div class="prp-meta"><?php echo $javo_meta_query->get('website'); ?>
                                                <li><div class="prp-meta"><?php echo $javo_meta_query->get('email'); ?>
                                                </ul>
                                            </div><!-- /.col-md-8 -->
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="thumb">
                                                    <a href="<?php the_permalink(); ?>" target="_blank"><?php the_post_thumbnail('javo-map-thumbnail'); ?></a>
                                                    <div class="img-in-text"><?php echo $javo_meta_query->cat('item_category', __('No Category', 'javo_fr')); ?>
                                                    </div>
                                                    <div class="javo-left-overlay">
                                                        <div class="javo-txt-meta-area"><?php echo $javo_meta_query->cat('item_location', __('No Location', 'javo_fr')); ?>
                                                        </div>
                                                        <div class="corner-wrap">
                                                            <div class="corner"></div>
                                                            <div class="corner-background"></div>
                                                        </div> <!-- corner-wrap -->
                                                    </div> <!-- javo-left-overlay -->
                                                </div> <!-- thumb -->
                                            </div><!-- /.col-md-4 -->
                                        </div><!-- /.row -->
                                    </div> <!-- javo_somw_info -->
                                    <?php
                                    $infoWindow = ob_get_clean();
                                    $latlng = @unserialize(get_post_meta(get_the_ID(), 'latlng', true));
                                    $javo_set_icon = '';
                                    $javo_marker_term_id = wp_get_post_terms(get_the_ID(), 'item_category');
                                    if (!empty($javo_marker_term_id)) {
                                        $javo_set_icon = get_option('javo_item_category_' . $javo_marker_term_id[0]->term_id . '_marker', '');
                                        if ($javo_set_icon == '') {
                                            $javo_set_icon = $javo_tso->get('map_marker', '');
                                        };
                                    };

                                    $markers[] = Array(
                                        'lat' => $latlng['lat']
                                        , 'lng' => $latlng['lng']
                                        , 'info' => $infoWindow
                                        , 'icon' => $javo_set_icon
                                        );
                }; // End While
            }; // End If
    // Not found results
            ob_start();
            ?>
            <div class="row">
                <div class="col-md-12">
                    <h3><?php _e('No results found!', 'javo_fr'); ?></h3>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p><?php _e('Please try to following', 'javo_fr'); ?></p>
                    <ol>
                        <li><?php _e('Add new item or blog posts', 'javo_fr'); ?></li>
                        <li><?php _e('Change to search condition.', 'javo_fr'); ?></li>
                    </ol>
                </div>
            </div>

            <?php
            $blank_content = ob_get_clean();
            $result = Array(
                "result" => "success"
                , "html" => $posts->found_posts > 0 ? $content : $blank_content
                , "markers" => $markers
                );
            echo json_encode($result);
            exit(0);
        }