<?php
class javo_item_price{
	public function __construct(){
		add_shortcode("javo_item_price", Array($this, "javo_item_price_callback"));
	}
	public function javo_item_price_callback($atts, $content=""){
		global $javo_tso;
		extract(shortcode_atts(Array(
			'title'						=> ''
			, 'sub_title'				=> ''
			, 'title_text_color'		=> '#000'
			, 'sub_title_text_color'	=> '#000'
			, 'line_color'				=> '#fff'
		), $atts)
		);
		ob_start();
		echo apply_filters('javo_shortcode_title', $title, $sub_title, Array('title'=>'color:'.$title_text_color.';', 'subtitle'=>'color:'.$sub_title_text_color.';', 'line'=>'border-color:'.$line_color.';'));

		// Not have payment
		$javo_pay_parametters = Array(
			'item1'		=> sprintf('user=%s&item_id=%s&post=%s&days=%s'
					, get_current_user_id()
				, $javo_tso->get('payment_item1_name')
				, $javo_tso->get('payment_item1_posts')
				, $javo_tso->get('payment_item1_days')
			), 'item2'	=> sprintf('user=%s&item_id=%s&post=%s&days=%s'
				, get_current_user_id()
				, $javo_tso->get('payment_item2_name')
				, $javo_tso->get('payment_item2_posts')
				, $javo_tso->get('payment_item2_days')
			), 'item3'	=> sprintf('user=%s&item_id=%s&post=%s&days=%s'
				, get_current_user_id()
				, $javo_tso->get('payment_item3_name')
				, $javo_tso->get('payment_item3_posts')
				, $javo_tso->get('payment_item3_days')
			), 'item4'	=> sprintf('user=%s&item_id=%s&post=%s&days=%s'
				, get_current_user_id()
				, $javo_tso->get('payment_item4_name')
				, $javo_tso->get('payment_item4_posts')
				, $javo_tso->get('payment_item4_days')
			)
		);
			?>
			<div class="javo-item-price-table">
				<?php
					
				# ITEM 1
				if( $javo_tso->get('payment_item1_use', '') == 'active' ){
					if( (int)$javo_tso->get('payment_item1_price', 0) > 0 ){
						?>
						<div class="col-sm-3">
							<div class="panel panel-default text-center">
								<div class="panel-heading" style="color:<?php echo $javo_tso->get('payment_item1_font_color'); ?>; background:<?php echo $javo_tso->get('payment_item1_color');?>; border-color:<?php echo $javo_tso->get('payment_item1_color');?>;">
									<h3 class="panel-title"><?php echo $javo_tso->get('payment_item1_name');?></h3>
								</div>
								<div class="panel-body" style="color:<?php echo $javo_tso->get('payment_item1_font_color'); ?>; background:<?php echo $javo_tso->get('payment_item1_color');?>; opacity:0.9; filter: alpha(opacity=90);">
									<h3 class="panel-title price"><span><?php printf('%s %s', $javo_tso->get('paypal_produce_price_prefix'), number_format($javo_tso->get('payment_item1_price', 0)))?></span></h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item">
										<?php
										printf('<div><i class="glyphicon glyphicon-pencil"></i>%s %s</div><div><i class="glyphicon glyphicon-calendar"></i>%s %s</div>'
										, $javo_tso->get('payment_item1_posts', 0)
										, __('Posts', 'javo_fr')
										, $javo_tso->get('payment_item1_days', 0)
										, __('Days', 'javo_fr')
										);?>
									</li>
									<li class="list-group-item">
										<form method="post" data-javo-payment-form>
											<input type="hidden" name="param" value="<?php echo $javo_pay_parametters['item1'];?>">
											<input type="hidden" name="item_id" value="1">
											<input type="hidden" name="act3" value="true">
											<input type="submit" class="btn btn-default" value="<?php _e('Continue', 'javo_fr');?>" style="color:<?php echo $javo_tso->get('payment_item1_font_color');?>; background:<?php echo $javo_tso->get('payment_item1_color');?>; opacity:0.9; filter: alpha(opacity=90);">
										</form>
									</li>
								</ul>
							</div><!-- Panel Close -->
						</div><!-- Item End -->
						<?php
					}else{
						echo apply_filters('javo_add_item_free_table', null,1);						
					}
				}

				# ITEM 2
				if( $javo_tso->get('payment_item2_use', '') == 'active' ){
					if( (int)$javo_tso->get('payment_item2_price', 0) > 0 ){
						?>
						<div class="col-sm-3">
							<div class="panel panel-default text-center">
								<div class="panel-heading" style="color:<?php echo $javo_tso->get('payment_item2_font_color'); ?>; background:<?php echo $javo_tso->get('payment_item2_color');?>; border-color:<?php echo $javo_tso->get('payment_item2_color');?>;">
									<h3 class="panel-title"><?php echo $javo_tso->get('payment_item2_name');?></h3>
								</div>
								<div class="panel-body" style="color:<?php echo $javo_tso->get('payment_item2_font_color'); ?>; background:<?php echo $javo_tso->get('payment_item2_color');?>; opacity:0.9; filter: alpha(opacity=90);">
									<h3 class="panel-title price"><span><?php printf('%s %s', $javo_tso->get('paypal_produce_price_prefix'), number_format($javo_tso->get('payment_item2_price', 0)))?></span></h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item">
										<?php
										printf('<div><i class="glyphicon glyphicon-pencil"></i>%s %s</div>
												  <div><i class="glyphicon glyphicon-calendar"></i>%s %s</div>'
										, $javo_tso->get('payment_item2_posts', 0)
										, __('Posts', 'javo_fr')
										, $javo_tso->get('payment_item2_days', 0)
										, __('Days', 'javo_fr')
										);?>
									</li>
									<li class="list-group-item">
										<form method="post" data-javo-payment-form>
											<input type="hidden" name="param" value="<?php echo $javo_pay_parametters['item2'];?>">
											<input type="hidden" name="item_id" value="2">
											<input type="hidden" name="act3" value="true">
											<input type="submit" class="btn btn-default" value="<?php _e('Continue', 'javo_fr');?>"style="color:<?php echo $javo_tso->get('payment_item2_font_color');?>; background:<?php echo $javo_tso->get('payment_item2_color');?>; opacity:0.9;filter: alpha(opacity=90); ">
										</form>
									</li>
								</ul>
							</div><!-- Panel Close -->
						</div><!-- Item End -->
						<?php
					}else{
						echo apply_filters('javo_add_item_free_table', null,2);
					}
				}

				# ITEM 3
				if( $javo_tso->get('payment_item3_use', '') == 'active' ){
					if( (int)$javo_tso->get('payment_item3_price', 0) > 0 ){
						?>
						<div class="col-sm-3">
							<div class="panel panel-default text-center">
								<div class="panel-heading" style="color:<?php echo $javo_tso->get('payment_item3_font_color'); ?>; background:<?php echo $javo_tso->get('payment_item3_color');?>; border-color:<?php echo $javo_tso->get('payment_item3_color');?>;">
									<h3 class="panel-title"><?php echo $javo_tso->get('payment_item3_name');?></h3>
								</div>
								<div class="panel-body" style="color:<?php echo $javo_tso->get('payment_item3_font_color'); ?>; background:<?php echo $javo_tso->get('payment_item3_color');?>; opacity:0.9; filter: alpha(opacity=90);">
									<h3 class="panel-title price"><span><?php printf('%s %s', $javo_tso->get('paypal_produce_price_prefix'), number_format($javo_tso->get('payment_item3_price', 0)))?></span></h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item">
										<?php
										printf('<div><i class="glyphicon glyphicon-pencil"></i>%s %s</div><div><i class="glyphicon glyphicon-calendar"></i>%s %s</div>'
										, $javo_tso->get('payment_item3_posts', 0)
										, __('Posts', 'javo_fr')
										, $javo_tso->get('payment_item3_days', 0)
										, __('Days', 'javo_fr')
										);?>
									</li>
									<li class="list-group-item">
										<form method="post" data-javo-payment-form>
											<input type="hidden" name="param" value="<?php echo $javo_pay_parametters['item3'];?>">
											<input type="hidden" name="item_id" value="3">
											<input type="hidden" name="act3" value="true">
											<input type="submit" class="btn btn-default" value="<?php _e('Continue', 'javo_fr');?>" style="color:<?php echo $javo_tso->get('payment_item3_font_color');?>; background:<?php echo $javo_tso->get('payment_item3_color');?>; opacity:0.9; filter: alpha(opacity=90);">
										</form>
									</li>
								</ul>
							</div><!-- Panel Close -->
						</div><!-- Item End -->
						<?php
					}else{
						echo apply_filters('javo_add_item_free_table', null,3);
					}
				}

				# ITEM 4
				if( $javo_tso->get('payment_item4_use', '') == 'active' ){
					if( (int)$javo_tso->get('payment_item4_price', 0) > 0 ){
						?>
						<div class="col-sm-3">
							<div class="panel panel-default text-center">
								<div class="panel-heading" style="color:<?php echo $javo_tso->get('payment_item4_font_color'); ?>; background:<?php echo $javo_tso->get('payment_item4_color');?>; border-color:<?php echo $javo_tso->get('payment_item4_color');?>;">
									<h3 class="panel-title"><?php echo $javo_tso->get('payment_item4_name');?></h3>
								</div>
								<div class="panel-body" style="color:<?php echo $javo_tso->get('payment_item4_font_color'); ?>; background:<?php echo $javo_tso->get('payment_item4_color');?>; opacity:0.9; filter: alpha(opacity=90);">
									<h3 class="panel-title price"><span><?php printf('%s %s', $javo_tso->get('paypal_produce_price_prefix'), number_format($javo_tso->get('payment_item4_price', 0)))?></span></h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item">
										<?php
										printf('<div><i class="glyphicon glyphicon-pencil"></i>%s %s</div><div><i class="glyphicon glyphicon-calendar"></i>%s %s</div>'
										, $javo_tso->get('payment_item4_posts', 0)
										, __('Posts', 'javo_fr')
										, $javo_tso->get('payment_item4_days', 0)
										, __('Days', 'javo_fr')
										);?>
									</li>
									<li class="list-group-item">
										<form method="post" data-javo-payment-form>
											<input type="hidden" name="param" value="<?php echo $javo_pay_parametters['item4'];?>">
											<input type="hidden" name="item_id" value="4">
											<input type="hidden" name="act3" value="true">
											<input type="submit" class="btn btn-default" value="<?php _e('Continue', 'javo_fr');?>" style="color:<?php echo $javo_tso->get('payment_item4_font_color');?>; background:<?php echo $javo_tso->get('payment_item4_color');?>; opacity:0.9; filter: alpha(opacity=90);">
										</form>
									</li>
								</ul>
							</div><!-- Panel Close -->
						</div><!-- Item End -->
						<?php
					}else{
						echo apply_filters('javo_add_item_free_table', null,4);						
					}
				}?>
			</div><!-- /.javo-item-price-table -->
			<input type="hidden" name="javo-item-payment-redirect" value="<?php echo home_url('add-item/'.wp_get_current_user()->user_login);?>">
			<script type="text/javascript">
			(function($){
				"use strict";
				$('[data-javo-payment-form]').each(function(){
					$(this).on('submit', function(e){
						e.preventDefault();
						if( !$('body').hasClass('logged-in') ){
							$('#login_panel').modal();
							return false;
						};
						$(this).get(0).action = $('[name="javo-item-payment-redirect"]').val();
						$(this).get(0).submit();
					});				
				});
			})(jQuery);
			</script>
		<?php
		$content = ob_get_clean();
		return $content;
	}
}
new javo_item_price();