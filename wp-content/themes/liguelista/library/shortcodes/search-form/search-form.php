<?php

class javo_search_form {

    public function __construct() {
        add_shortcode('javo_search_form', Array($this, 'javo_search_form_callback'));

        add_action('wp_ajax_nopriv_get_categories_autocomplete', Array($this, 'get_categories_autocomplete_callback'));
        add_action('wp_ajax_get_categories_autocomplete', Array($this, 'get_categories_autocomplete_callback'));
    }

    public function get_categories_autocomplete_callback() {
        $autoComplete = array();

        $listCategories = get_terms("item_category", Array("hide_empty" => 0));

        foreach ($listCategories as $category) {
            array_push($autoComplete, array("value" => $category->name, "data" => $category->term_id));
        }

        asort($autoComplete);

        echo json_encode(Array('result' => $autoComplete));

        exit;
    }

    public function javo_search_form_callback($atts, $content = '') {

        extract(shortcode_atts(Array(
                    'action' => 'home'
                    , 'hide_field' => Array()
                        ), $atts));
        $hide_field = @explode(',', $hide_field);
        if (!empty($hide_field)) {
            foreach ($hide_field as $idx => $item) {
                $hide_field[$item] = 'hide';
                unset($hide_field[$idx]);
            }
        }
        $javo_this_onoff = new javo_ARRAY($hide_field);
        if ($action == 'home') {
            $javo_this_form_action = home_url('/');
        } else {
            $javo_this_form_action = get_permalink((int) $action);
        }
        $javo_query = new javo_ARRAY($_GET);

        ob_start();
        ?>
        <div class="container search-type-a-wrap">
            <form role="form" data-javo-search-form>
                <div class="search-type-a-inner">
                    <div class="search-box-inline<?php echo $javo_this_onoff->get('keyword', null) == 'hide' ? ' hidden' : ''; ?>">
                        <input title="Digite ao menos 3 caracteres para pesquisar!" type="text" class="search-a-items" name="s" placeholder="<?php _e('Keyword', 'javo_fr'); ?>" value="<?php echo $javo_query->get('s', null); ?>">
                    </div>

                    <div class="search-box-inline<?php echo $javo_this_onoff->get('category', null) == 'hide'? ' hidden':'';?>" style="display: none;">
                        <select name="filter[item_category]" class="form-control">
                            <option value=""><?php _e('Category', 'javo_fr');?></option>
                            <?php echo apply_filters('javo_get_selbox_child_term_lists', 'item_category', null, 'select', $javo_query->get('category', 0), 0, 0 );?>
                        </select>
                    </div>

                    <div class="search-box-inline<?php echo $javo_this_onoff->get('location', null) == 'hide' ? ' hidden' : ''; ?>">
                        <select name="filter[item_location]" class="form-control">
                            <option value=""><?php _e('Location', 'javo_fr'); ?></option>
                            <?php echo apply_filters('javo_get_selbox_child_term_lists', 'item_location', null, 'select-sigla', $javo_query->get('location', 0), 0, 0, "-"); ?>
                        </select>
                    </div><!-- /.search-box-inline -->
                    <div class="search-box-inline hidden<?php //echo $javo_this_onoff->get('dropdown', null) == 'hide' || is_archive() || is_search() ? ' hidden' : ''; ?>">
                        <select class="form-control" data-javo-search-form-action-type>
                            <option value="data-javo-patch-form-for-result"><?php _e('Search Result Page', 'javo_fr'); ?></option>
                            <option value="data-javo-patch-form-for-template"><?php _e('Search on the Map', 'javo_fr'); ?></option>
                        </select>
                    </div>
                    <div class="search-box-inline">
                        <input type="submit" class="btn btn-block btn-primary admin-color-setting" title="Pesquisar" alt="<?php _e('Search', 'javo_fr'); ?>" >
                    </div><!-- /.col-md-2 -->
                </div> <!-- search-type-a-inner -->
            </form>
            <fieldset>
                <input type="hidden" value="<?php echo (int) $action > 0 ? get_permalink($action) : null; ?>" data-javo-search-action-template-url>
            </fieldset>
            <!-- Search Result Page -->
            <form action="<?php echo home_url('/'); ?>" method="get" data-javo-patch-form-for-result>
                <input type="hidden" name="post_type" value="item">
                <input type="hidden" name="category" value="<?php echo $javo_query->get('category'); ?>" data-javo-sf-category>
                <input type="hidden" name="location" value="<?php echo $javo_query->get('location'); ?>" data-javo-sf-location>
                <input type="hidden" name="s" data-javo-sf-keyword>
            </form><!-- /data-javo-patch-form-for-result : Go to Archive Page -->
            <!-- Javo Map Template -->
            <form action="<?php echo $javo_this_form_action; ?>" method="post" data-javo-patch-form-for-template>
                <input type="hidden" name="filter[item_category]" value="<?php echo $javo_query->get('category'); ?>" data-javo-sf-category>
                <input type="hidden" name="filter[item_location]" value="<?php echo $javo_query->get('location'); ?>" data-javo-sf-location>
                <input type="hidden" name="keyword" data-javo-sf-keyword>
            </form><!-- /data-javo-patch-form-for-template : Go to Map -->
        </div> <!-- container search-type-a-wrap -->

        <script type="text/javascript">
            jQuery(function($){
                jQuery('.search-box-inline').find('input[type=submit]').click(function(){

                    var hasSearch = (jQuery('.search-a-items').val().length >= 4);
                    var hasCategory = (jQuery('[name="filter[item_category]"]').val() !== "");

                    if (!hasSearch && !hasCategory) {
                        jQuery('.search-box-inline:nth-child(2)').showBalloon();
                        jQuery('.search-box-inline:nth-child(3)').showBalloon();
                        return false;
                    }

                    if(jQuery('[name="filter[item_location]"]').val() === ""){
                        jQuery('.search-box-inline:nth-child(3)').showBalloon();
                        return false;
                    }
                    /*
                    if(jQuery('.search-a-items').val().length < 2) {
                        jQuery('.search-a-items').showBalloon();
                        return false;
                    } else if(jQuery('[name="filter[item_category]"]').val() === ""){
                        jQuery('.search-box-inline:nth-child(2)').showBalloon();
                        return false;
                    }
                    */
                });

                /*jQuery('.search-a-items').keypress(function(){
                    var regex = new RegExp("^[0-9A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ'\" ]+$");
                    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                    if (!regex.test(key) && event.keyCode != 13) {
                        event.preventDefault();
                        return false;
                    }
                });*/

                var javo_search_bar_script = {
                    el:{
                        origin: '[data-javo-search-form]'
                        , result: '[data-javo-patch-form-for-result]'
                        , template: '[data-javo-patch-form-for-template]'
                        , type: '[data-javo-search-form-action-type]'
                    }
                    , template_url: $('[data-javo-search-action-template-url]').val()
                    , msg: function(str){ $.javo_msg({content:str, delay:10000}); }
                    , init: function(){
                        jQuery('.search-a-items').balloon();
                        jQuery('.search-box-inline:nth-child(2)').attr("title", "Selecione uma categoria!");
                        jQuery('.search-box-inline:nth-child(2)').balloon();
                        jQuery('.search-box-inline:nth-child(3)').attr("title", "Selecione uma cidade ou estado!");
                        jQuery('.search-box-inline:nth-child(3)').balloon();

                        var $cat = this.setAutoComplete('[name="filter[item_category]"]');
                        var $loc = this.setAutoComplete('[name="filter[item_location]"]');

                        $('body').on('change', '[name="s"]', function () {
                            $('[name="filter[item_category]"]').val("");
                        });

                        var categories = new Array();

                        $.ajax({
                            url:'<?php echo admin_url("admin-ajax.php"); ?>'
                            , type: 'post'
                            , dataType: 'json'
                            , async: false
                            , data:{ action:'get_categories_autocomplete' }
                            , success:function(d){
                                categories = d.result;
                            }
                            , error: function(e){
                                console.log(e.responseText);
                            }
                        });

                        var categoriesArray = Object.keys(categories).map(function(k) { return categories[k] });

                        var formatRegexp = function(q) {
                            q = q.replace(/e/i,'[eéèêëEÉÈÊË]');
                            q = q.replace(/a/i,'[aàáâãäAÀÁÂÃÄÅÆ]');
                            q = q.replace(/c/i,'[cçÇC]');
                            q = q.replace(/i/i,'[íìiïîÍÌIÌÍÎÏ]');
                            q = q.replace(/o/i,'[oóòôöÒÓÔÕÖ]');
                            q = q.replace(/u/i,'[uúùüûUÜÛÙÚ]');
                            q = q.replace(/y/i,'[ýyYÿ^yÝ]');
                            q = q.replace(/n/i,'[nñNÑ]');
                            return q;
                        };

                        var _autocompleteLookup = function (suggestion, originalQuery, queryLowerCase) {
                                var pattern = '(\\b|^)('+$.Autocomplete.utils.escapeRegExChars(queryLowerCase)+')';
                                pattern = formatRegexp(pattern);
                                var matcher = new RegExp(pattern);
                                var ret = suggestion.value.toLowerCase().match(matcher);
                                return ret;
                        };

                        var _autocompleteFormatResult = function (suggestion, currentValue) {
                                var pattern = '(\\b|^)('+$.Autocomplete.utils.escapeRegExChars(currentValue)+')';
                                pattern = formatRegexp(pattern);
                                return suggestion.value.replace(new RegExp(pattern, 'gi'), '<strong>$1$2<\/strong>');
                        };

                        jQuery('[name="s"]').autocomplete({
                            lookup: categoriesArray,
                            dataType: 'json',
                            minChars: 3,
                            type: 'GET',
                            onSelect: function (suggestion) {
                                $('[name="filter[item_category]"]').val(suggestion.data);
                            },
                            onInvalidateSelection: function () {
                                $('[name="filter[item_category]"]').val("");
                            },
                            lookupFilter: _autocompleteLookup,
                            formatResult: _autocompleteFormatResult
                        });

                        $(document)
                            .on('submit', this.el.origin, this.submit)
                            .on('change', '[data-javo-search-form-action-type]', this.swapType);
                    }
                    , setAutoComplete: function(el){
                        $(this.el.origin).find(el).chosen({ search_contains: 1 });
                        return $(this.el.origin).find(el);
                    }
                    , swapType: function(){
                        var o = javo_search_bar_script;

                        if( o.template_url == ""){
                            o.msg('<?php _e("The map template (to show result page) is not setup or was deleted. Please contact to webmaster.", "javo_fr"); ?>');
                            $(this).get(0).selectedIndex = 0;
                            return false;
                        };
                    }
                    , submit: function(e){
                        e.preventDefault();
                        var o = javo_search_bar_script;
                        var r = $( o.el.origin );
                        var t = $('[' + $(o.el.type).val() + ']');
                        t.find('[data-javo-sf-category]').val(r.find('[name="filter[item_category]"]').val());
                        t.find('[data-javo-sf-location]').val(r.find('[name="filter[item_location]"]').val());

                        if (r.find('[name="filter[item_category]"]').val() == undefined || r.find('[name="filter[item_category]"]').val() == "") {
                            t.find('[data-javo-sf-keyword]').val(r.find('input[name="s"]').val());
                        }

                        t.submit();
                    }
                }
                javo_search_bar_script.init();
            });
        </script>
        <?php
        return ob_get_clean();
    }
}

new javo_search_form();