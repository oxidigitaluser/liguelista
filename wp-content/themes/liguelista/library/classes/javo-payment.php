<?php
class javo_payment{
	static $is_expired;
	function __construct()
	{
		// Define
		define('JAVO_EXPIRED_DAY'	, 'item_expired');
		define('JAVO_PAY_STATE'		, 'post_pay_status');
		define('JAVO_PAY_BALENCE'	, 'pay_cnt_post');
		define('JAVO_DATE_FORMAT'	, 'YmdHis');

		// Publish item
		add_action("wp_ajax_nopriv_publish_item", Array(__CLASS__, "javo_publish_item_callback"));
		add_action("wp_ajax_publish_item", Array(__CLASS__, "javo_publish_item_callback"));
	}

	static function get_expired( $display='origin', $format = 'Y-m-d h:i:s')
	{
		global $post;

		if( !empty( $post->ID ) ){
			$javo_exp_date = get_post_meta( $post->ID, JAVO_EXPIRED_DAY, true);
			if( get_post_meta( $post->ID, JAVO_PAY_STATE, true ) == 'paid' ){
				switch($display){
					case 'expire':
						$javo_this_return = floor( ( strtotime($javo_exp_date) - strtotime('now') ) / 86400 );
					break;
					case 'origin': default:
						$javo_this_return = date( $format, strtotime( $javo_exp_date ) );
				}
			}else{
				$javo_this_return	= __('free', 'javo_fr');
			}
		}else{
			$javo_this_return		= __('Unknown', 'javo_fr');
		}

		// Expired
		self::$is_expired = strtotime($javo_exp_date) <= strtotime('now') ? 'expired' : '';
		return $javo_this_return;
	}

	static function javo_publish_item_callback()
	{
		// Initialize Variables.
		$javo_query = new javo_ARRAY( $_POST );

		// It Has Bad Post ID
		if( (int)$javo_query->get('post_id') <= 0){
			echo json_encode(Array(
				'status'	=> 'fail'
				, 'comment'	=> __('Not found item posting', 'javo_fr')
			));
			exit;
		};

		// It Has Bad User ID
		if( (int)$javo_query->get('user_id') <= 0){
			echo json_encode(Array(
				'status'	=> 'fail'
				, 'comment'	=> __('Not found user', 'javo_fr')
			));
			exit;
		};

		// It Has Bad Payment Item ID
		if( (int)$javo_query->get('item_id') <= 0){
			echo json_encode(Array(
				'status'	=> 'fail'
				, 'comment'	=> __('Not found payment', 'javo_fr')
			));
			exit;
		};

		// Get Payment Item Informations.

		# Item ID
			$javo_pay_meta = new javo_get_meta( $javo_query->get('item_id') );
		# Post Amount
			$jav_pay_cnt_cur = (int)$javo_pay_meta->_get('pay_cnt_post');
		# Post Expired Days
			$jav_pay_day_cur = (int)$javo_pay_meta->_get('pay_expire_day');

		if($jav_pay_cnt_cur <= 0){
			echo json_encode(Array(
				'status'	=> 'fail'
				, 'comment'	=> __('Bad Payment Options.', 'javo_fr')
			));
			exit;
		};

		// Active Item By Payment Item
		$post_id = wp_update_post(Array(
			"ID"=> (int)$javo_query->get('post_id')
			, "post_status"=> "publish"
		));

		// Used Payment Item
		$jav_pay_cnt_cur--;
		update_post_meta($javo_query->get('item_id'), JAVO_PAY_BALENCE, $jav_pay_cnt_cur);

		// Update Target Item Information
		update_post_meta($post_id, JAVO_EXPIRED_DAY		, date(JAVO_DATE_FORMAT, strtotime($jav_pay_day_cur.' days')));
		update_post_meta($post_id, JAVO_PAY_STATE		, 'paid');

		// Return Strings & Variables.
		echo json_encode(Array(
			'status'		=> 'success'
			, 'expired'		=> date('Y-m-d', strtotime('2014-05-05'))
			, 'permalink'	=> get_permalink($post_id)
		));
		exit;
	}
}
new javo_payment();