<?php
/**
 * The Header template for Javo Theme
 *
 * @package WordPress
 * @subpackage Javo_Directory
 * @since Javo Themes 1.0
 */
 // Get Options
global $javo_theme_option;
global $javo_tso;
$javo_theme_option = @unserialize(get_option("javo_themes_settings"));
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php //bloginfo('name'); ?> <?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="icon" type="image/x-icon" href="<?php echo $javo_tso->get('favicon_url', '');?>" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo $javo_tso->get('favicon_url', '');?>" />


<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lte IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php
// Custom CSS AREA
if($javo_tso->get('custom_css', '') != ''){
	printf('<style type="text/css">%s</style>', stripslashes( $javo_tso->get('custom_css', '') ) );
};?>

<?php wp_head(); ?>

<link rel="stylesheet" href="/novo.css" type="text/css">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54242636-1', 'auto');
  ga('send', 'pageview');

</script>


</head>
<style type="text/css">
.admin-color-setting,
.btn.admin-color-setting,
.javo-txt-meta-area.admin-color-setting,
.javo-left-overlay.bg-black .javo-txt-meta-area.admin-color-setting,
.javo-left-overlay.bg-red .javo-txt-meta-area.admin-color-setting,
.javo-txt-meta-area.custom-bg-color-setting
{
	background-color: <?php echo $javo_tso->get('total_button_color');?>;
	<?php if( $javo_tso->get('total_button_border_use') == 'use'): ?>
	border-style:solid;
	border-width:1px;
	border-color: <?php echo $javo_tso->get('total_button_border_color');?>;
	<?php else:?>
	border:none;
	<?php endif;?>
}
.javo-left-overlay .corner-wrap .corner-background.admin-color-setting,
.javo-left-overlay .corner-wrap .corner.admin-color-setting{
	border:2px solid <?php echo $javo_tso->get('total_button_color');?>;
	border-bottom-color: transparent !important;
	border-left-color: transparent !important;
	background:none !important;
}
.admin-border-color-setting{
	border-color:<?php echo $javo_tso->get('total_button_border_color');?>;
}
.custom-bg-color-setting,
#javo-events-gall .event-tag.custom-bg-color-setting{
	background-color: <?php echo $javo_tso->get('total_button_color');?>;
}
.custom-font-color{
	color:<?php echo $javo_tso->get('total_button_color');?>;
}
.javo_pagination > .page-numbers.current{
	background-color:<?php echo $javo_tso->get('total_button_color');?>;
	color:#fff;
}
.progress .progress-bar{border:none; background-color:<?php echo $javo_tso->get('total_button_color');?>;}
<?php echo $javo_tso->get('preloader_hide') == 'use'? '.pace{ display:none !important; }' : '';?>

<?php if($javo_tso->get('single_page_menu_other_bg_color')=='use'){ ?>
.single-item #header-one-line{background:<?php echo $javo_tso->get('single_page_menu_bg_color'); ?> !important;}
<?php } ?>
.single-item #header-one-line .navbar-nav>li>a,
.single-item #header-one-line #javo-navibar .navbar-right>li>a>span, 
.single-item #header-one-line #javo-navibar .navbar-right>li>a>img{color:<?php echo $javo_tso->get('single_page_menu_text_color'); ?> !important; border-color:<?php echo $javo_tso->get('single_page_menu_text_color'); ?>;}

#javo-archive-sidebar-nav > li > a { background: <?php echo $javo_tso->get('total_button_color');?>; }
#javo-archive-sidebar-nav > li.li-with-ul > span{ color:#fff; }
#javo-archive-sidebar-nav .slight-submenu-button{ color: <?php echo $javo_tso->get('total_button_color');?>; }
.javo-archive-header-search-bar>.container{background:<?php echo $javo_tso->get('archive_searchbar_bg_color'); ?>; border-color:<?php echo $javo_tso->get('archive_searchbar_border_color'); ?>;}

</style>
<body <?php body_class();?>>
<?php if( defined('ICL_LANGUAGE_CODE') ){ ?>
	<input type="hidden" name="javo_cur_lang" value="<?php echo ICL_LANGUAGE_CODE;?>">
<?php }; ?>

<div id="page-style" class="<?php echo $javo_tso->get('layout_style_boxed') == "active"? "boxed":""; ?>">

<?php
require_once $javo_tso->get('header_file', 'library/header/head-directory.php');
if(is_singular()){
	get_template_part("library/header/post", "header");
};