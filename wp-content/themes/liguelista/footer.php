<?php
/**
 * The template for displaying the footer
 *
 * @package WordPress
 * @subpackage Javo_Directory
 * @since Javo Themes 1.0
 */
?>
<?php global $javo_theme_option, $javo_tso; ?>
	<?php if($javo_tso->get('footer-banner')) : ?>
		<div class="row footer-top-banner-row">
			<div class="container">
				<a href="<?php echo $javo_tso->get('footer-banner-link') ? 'http://'.$javo_tso->get('footer-banner-link') :'';?>" target="_brank">
					<img src="<?php echo $javo_tso->get('footer-banner'); ?>" style="width:<?php echo $javo_tso->get('footer-banner-width'); ?>px; height:<?php echo $javo_tso->get('footer-banner-height'); ?>px;">
				</a>
			</div>
		</div> <!--footer-top-banner-row -->
	<?php endif; ?>
	
	<?php if( is_active_sidebar('footer-level1-1') || is_active_sidebar('footer-level1-2')  ) : ?>
<!-- SUPPORT & NEWSLETTER SECTION ================================================ -->
<div class="row footer-top-full-wrap">
	<div class="container footer-top">
		<section>
		  <div id="support">
			<div class="row">
			  <div class="col-md-3">
				<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-level1-1')):
				endif;
				?>
			  </div>

			  <div class="col-md-8 col-md-offset-1">
				<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-level1-2')):
				endif;
				?>
			  </div>
			</div><!--end row-->
		  </div><!--end support-->
		</section>
	</div><!-- container-->
</div> <!-- footer-top-full-wrap -->
<!--END SUPPORT & NEWSLETTER SECTION-->
<?php endif; ?>
<footer class="footer-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			</div><!-- /.col-md-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</footer><!-- /.footer-wwrap" -->
<footer class="footer-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-3"><?php if( is_active_sidebar('sidebar-foot1') ) : ?><?php dynamic_sidebar("Footer Sidebar1");?><?php endif; ?></div> <!-- col-md-3 -->
			<div class="col-md-3"><?php if( is_active_sidebar('sidebar-foot2') ) : ?><?php dynamic_sidebar("Footer Sidebar2");?><?php endif; ?></div> <!-- col-md-3 -->
			<div class="col-md-3"><?php if( is_active_sidebar('sidebar-foot3') ) : ?><?php dynamic_sidebar("Footer Sidebar3");?><?php endif; ?></div> <!-- col-md-3 -->
			<div class="col-md-3"><?php if( is_active_sidebar('sidebar-foot4') ) : ?><?php dynamic_sidebar("Footer Sidebar4");?><?php endif; ?></div> <!-- col-md-3 -->
		</div> <!-- container -->
	</div> <!-- row -->
</footer>

<div class="footer-bottom">
    <div class="container">
		<p><?php echo stripslashes($javo_tso->get('copyright', null));?></p>
    </div> <!-- container -->
</div> <!-- footer-bottom -->


	


<?php
get_template_part('templates/parts/modal', 'login');		// modal login
get_template_part('templates/parts/modal', 'register');		// modal Register
get_template_part('templates/parts/modal', 'contact-us');	// modal contact us
get_template_part('templates/parts/modal', 'map-brief');	// Map Brief
get_template_part("templates/parts/modal", "emailme");		// Link address send to me
get_template_part("templates/parts/modal", "reviews");		// reviews
get_template_part("templates/parts/modal", "claim");		// claim
echo stripslashes($javo_tso->get('analytics'));
?>
<?php wp_footer(); ?>
</div>
</body>
</html>