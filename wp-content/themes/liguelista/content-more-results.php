<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$allPosts = array();

get_header();

$args = array_merge($wp_query->query_vars, array(
    'post_type' => 'item'
    , 'location' => $_GET['location']
    , 's' => $_GET['s']
    , 'order' => 'ASC'
    , 'orderby' => 'meta_value title'
    , 'meta_key' => 'plano_comercial'));

query_posts($args);

$postsSearch = $wp_query->posts;

wp_reset_query();

$queryCategory = sprintf(
     "SELECT $wpdb->postmeta.meta_value, $wpdb->posts.*
        FROM $wpdb->posts
       INNER JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id)
       INNER JOIN $wpdb->postmeta ON ($wpdb->posts.ID = $wpdb->postmeta.post_id)
       INNER JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
       INNER JOIN $wpdb->terms ON ($wpdb->terms.term_id = $wpdb->term_taxonomy.term_id)
       WHERE $wpdb->terms.name LIKE '%s'
         AND $wpdb->term_taxonomy.taxonomy = 'item_category'
         AND $wpdb->posts.post_password = ''
         AND $wpdb->posts.post_type = 'item'
         AND $wpdb->posts.post_status = 'publish'
         AND $wpdb->postmeta.meta_key = 'plano_comercial'", '%' . $_GET['s'] . '%');

if (!empty($_GET['location'])) {
    $queryCategory = $queryCategory . sprintf(" AND $wpdb->term_relationships.term_taxonomy_id IN (%s)", $_GET['location']);
}

$queryCategory = $queryCategory . " GROUP BY $wpdb->posts.ID ORDER BY $wpdb->postmeta.meta_value ASC, $wpdb->posts.post_title ASC";

$postsCategory = $wpdb->get_results($queryCategory, OBJECT);

wp_reset_query();

foreach (array_merge($postsSearch, $postsCategory) as $p) {
    if (search_array($p->ID, $allPosts))
        continue;

    array_push($allPosts, $p->ID);
}

if (count($allPosts) != 0) {
    $wp_query->query_vars = array(
        'post__in' => $allPosts
        , 'location' => $_GET['location']
        , 'post_type' => 'item'
        , 'order' => 'ASC'
        , 'orderby' => 'meta_value title'
        , 'meta_key' => 'plano_comercial'
        , 'posts_per_page' => 5
        , 'paged' => 2
    );

    query_posts($wp_query->query_vars);
    $wp_query->is_search = true;
}

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$allPosts = array();

get_header();

$args = array_merge($wp_query->query_vars, array(
    'post_type' => 'item'
    , 'location' => $_GET['location']
    , 's' => $_GET['s']
    , 'order' => 'ASC'
    , 'orderby' => 'meta_value title'
    , 'meta_key' => 'plano_comercial'));

query_posts($args);

$postsSearch = $wp_query->posts;

wp_reset_query();

$queryCategory = sprintf(
     "SELECT $wpdb->postmeta.meta_value, $wpdb->posts.*
        FROM $wpdb->posts
       INNER JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id)
       INNER JOIN $wpdb->postmeta ON ($wpdb->posts.ID = $wpdb->postmeta.post_id)
       INNER JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
       INNER JOIN $wpdb->terms ON ($wpdb->terms.term_id = $wpdb->term_taxonomy.term_id)
       WHERE $wpdb->terms.name LIKE '%s'
         AND $wpdb->term_taxonomy.taxonomy = 'item_category'
         AND $wpdb->posts.post_password = ''
         AND $wpdb->posts.post_type = 'item'
         AND $wpdb->posts.post_status = 'publish'
         AND $wpdb->postmeta.meta_key = 'plano_comercial'", '%' . $_GET['s'] . '%');

if (!empty($_GET['location'])) {
    $queryCategory = $queryCategory . sprintf(" AND $wpdb->term_relationships.term_taxonomy_id IN (%s)", $_GET['location']);
}

$queryCategory = $queryCategory . " GROUP BY $wpdb->posts.ID ORDER BY $wpdb->postmeta.meta_value ASC, $wpdb->posts.post_title ASC";

$postsCategory = $wpdb->get_results($queryCategory, OBJECT);

wp_reset_query();

foreach (array_merge($postsSearch, $postsCategory) as $p) {
    if (search_array($p->ID, $allPosts))
        continue;

    array_push($allPosts, $p->ID);
}

if (count($allPosts) != 0) {
    $wp_query->query_vars = array(
        'post__in' => $allPosts
        , 'location' => $_GET['location']
        , 'post_type' => 'item'
        , 'order' => 'ASC'
        , 'orderby' => 'meta_value title'
        , 'meta_key' => 'plano_comercial'
        , 'posts_per_page' => 5
        , 'paged' => 2
    );

    query_posts($wp_query->query_vars);
    $wp_query->is_search = true;
}

while (have_posts()) {
                                        the_post();
                                        $javo_this_latlng = @unserialize(get_post_meta(get_the_ID(), 'latlng', true));
                                        $plano_comercial = get_post_meta(get_the_ID(), "plano_comercial", true);
                                        $javo_this_posts_return[get_the_ID()]['latlng'] = $javo_this_latlng;
                                        $javo_meta_query = new javo_get_meta(get_the_ID());
                                        $javo_set_icon = '';
                                        $javo_marker_term_id = wp_get_post_terms(get_the_ID(), 'item_category');
                                        $javo_item_location = wp_get_post_terms(get_the_ID(), 'item_location', array('order' => 'DESC', 'orderby' => 'parent'));
                                        $logotipo = (Array) @unserialize(get_post_meta(get_the_ID(), "logotipo_images", true));
                                        $url = wp_get_attachment_image_src($logotipo[0], Array(130, 130));

                                        if (!empty($javo_marker_term_id)) {
                                            $javo_set_icon = get_option('javo_item_category_' . $javo_marker_term_id[0]->term_id . '_marker', '');
                                            if ($javo_set_icon == '') {
                                                $javo_set_icon = $javo_tso->get('map_marker', '');
                                            };
                                            $javo_this_posts_return[get_the_ID()]['icon'] = $javo_set_icon;
                                        };

                                        /* PLANO MINISITE */
                                        if ($plano_comercial == '0') { //MINISITE
                                            get_template_part('content', 'archive');
                                            ?>
                                            <script type="text/template" id="javo_map_tmp_<?php the_ID(); ?>">
                                                <div class="javo_somw_info panel">
                                                <div class="des">
                                                <h5><?php echo javo_str_cut(get_the_title(), 30); ?></h5>
                                                <ul class="list-unstyled">
                                                <li>
                                                <?php
                                                echo $javo_meta_query->get('(');
                                                echo $javo_meta_query->get('ddd1');
                                                echo $javo_meta_query->get(')');
                                                echo $javo_meta_query->get(' ');
                                                echo $javo_meta_query->get('phone1');
                                                ?>
                                                </li>

                                                <li><?php echo $javo_meta_query->get('website'); ?></li>
                                                <li><?php echo $javo_meta_query->get('email'); ?></li>
                                                </ul>
                                                <!--a class="btn btn-dark javo-this-go-more" href="<?php the_permalink(); ?>"><?php _e('Ver Mais', 'javo_fr'); ?></a-->
                                                </div>

                                                <div class="pics">
                                                <div class="thumb">
                                                <a href="<?php the_permalink(); ?>">
                                                <?php
                                                if ($logotipo[0]) {
                                                    printf('<img src="%s" class="wp-post-image" style="width:130px; height:130px;">', $url[0]);
                                                } else {
                                                    printf('<img src="%s" style="width:130px; height:130px;">', $javo_tso->get('no_image', JAVO_IMG_DIR . '/no-image.png'));
                                                };
                                                ?>
                                                </a>
                                                </div> <!-- thumb -->
                                                <div class="img-in-text"><?php echo $javo_meta_query->cat('item_category', __('Sem Categoria', 'javo_fr')); ?></div>
                                                <div class="javo-left-overlay">
                                                <div class="javo-txt-meta-area"><?php echo $javo_meta_query->cat('item_location', __('Sem Localização', 'javo_fr')); ?></div>
                                                <div class="corner-wrap">
                                                <div class="corner"></div>
                                                <div class="corner-background"></div>
                                                </div> <!-- corner-wrap -->
                                                </div> <!-- javo-left-overlay -->
                                                </div> <!-- pic -->
                                                </div> <!-- javo_somw_info -->
                                            </script>
                                            <?php
                                        } else if ($plano_comercial == '1') { //MDVIT
                                            get_template_part('content', 'mdvit');
                                            ?>
                                            <script type="text/template" id="javo_map_tmp_<?php the_ID(); ?>">
                                                <div class="javo_somw_info panel">
                                                <div class="des">
                                                <h5><?php echo javo_str_cut(get_the_title(), 30); ?></h5>
                                                <ul class="list-unstyled">
                                                <li>
                                                <?php
                                                echo $javo_meta_query->get('(');
                                                echo $javo_meta_query->get('ddd1');
                                                echo $javo_meta_query->get(')');
                                                echo $javo_meta_query->get(' ');
                                                echo $javo_meta_query->get('phone1');
                                                ?>
                                                </li>

                                                <li><?php echo $javo_meta_query->get('website'); ?></li>
                                                <li><?php echo $javo_meta_query->get('email'); ?></li>
                                                </ul>
                                                <!--a class="btn btn-dark javo-this-go-more" href="<?php the_permalink(); ?>"><?php _e('Ver Mais', 'javo_fr'); ?></a-->
                                                </div> <!-- des -->

                                                <div class="pics">
                                                <div class="thumb">
                                                <a href="<?php the_permalink(); ?>">
                                                <?php
                                                if ($logotipo[0]) {
                                                    printf('<img src="%s" class="wp-post-image" style="width:130px; height:130px;">', $url[0]);
                                                } else {
                                                    printf('<img src="%s" style="width:130px; height:130px;">', $javo_tso->get('no_image', JAVO_IMG_DIR . '/no-image.png'));
                                                };
                                                ?>
                                                </a>
                                                </div> <!-- thumb -->
                                                <div class="img-in-text"><?php echo $javo_meta_query->cat('item_category', __('Sem Categoria', 'javo_fr')); ?></div>
                                                <div class="javo-left-overlay">
                                                <div class="javo-txt-meta-area"><?php echo $javo_meta_query->cat('item_location', __('Sem Localização', 'javo_fr')); ?></div>
                                                <div class="corner-wrap">
                                                <div class="corner"></div>
                                                <div class="corner-background"></div>
                                                </div> <!-- corner-wrap -->
                                                </div> <!-- javo-left-overlay -->
                                                </div> <!-- pic -->
                                                </div> <!-- javo_somw_info -->
                                            </script>
                                            <?php
                                        } else if ($plano_comercial == '2') { //MDL
                                            get_template_part('content', 'mdl');
                                            ?>
                                            <script type="text/template" id="javo_map_tmp_<?php the_ID(); ?>">
                                                <div class="javo_somw_info panel">
                                                <div class="des">
                                                <h5><?php echo javo_str_cut(get_the_title(), 30); ?></h5>
                                                <ul class="list-unstyled">
                                                <li>
                                                <?php
                                                echo $javo_meta_query->get('(');
                                                echo $javo_meta_query->get('ddd1');
                                                echo $javo_meta_query->get(')');
                                                echo $javo_meta_query->get(' ');
                                                echo $javo_meta_query->get('phone1');
                                                ?>
                                                </li>

                                                <li><?php echo $javo_meta_query->get('website'); ?></li>
                                                <li><?php echo $javo_meta_query->get('email'); ?></li>
                                                </ul>
                                                <!-- class="btn btn-dark javo-this-go-more" href="<?php the_permalink(); ?>"><?php _e('Ver Mais', 'javo_fr'); ?></a-->
                                                </div> <!-- des -->

                                                <div class="pics">
                                                <div class="thumb">
                                                <a href="<?php the_permalink(); ?>">
                                                <?php
                                                if ($logotipo[0]) {
                                                    printf('<img src="%s" class="wp-post-image" style="width:130px; height:130px;">', $url[0]);
                                                } else {
                                                    printf('<img src="%s" style="width:130px; height:130px;">', $javo_tso->get('no_image', JAVO_IMG_DIR . '/no-image.png'));
                                                };
                                                ?>
                                                </a>
                                                </div> <!-- thumb -->
                                                <div class="img-in-text"><?php echo $javo_meta_query->cat('item_category', __('Sem Categoria', 'javo_fr')); ?></div>
                                                <div class="javo-left-overlay">
                                                <div class="javo-txt-meta-area"><?php echo $javo_meta_query->cat('item_location', __('Sem Localização', 'javo_fr')); ?></div>
                                                <div class="corner-wrap">
                                                <div class="corner"></div>
                                                <div class="corner-background"></div>
                                                </div> <!-- corner-wrap -->
                                                </div> <!-- javo-left-overlay -->
                                                </div> <!-- pic -->
                                                </div> <!-- javo_somw_info -->
                                            </script>
                                            <?php
                                        } else if ($plano_comercial == '3') { //MDN
                                            get_template_part('content', 'mdn');
                                            ?>
                                            <script type="text/template" id="javo_map_tmp_<?php the_ID(); ?>">
                                                <div class="javo_somw_info panel">
                                                <div class="des">
                                                <h5><?php echo javo_str_cut(get_the_title(), 30); ?></h5>
                                                <ul class="list-unstyled">
                                                <li>
                                                <?php
                                                echo $javo_meta_query->get('(');
                                                echo $javo_meta_query->get('ddd1');
                                                echo $javo_meta_query->get(')');
                                                echo $javo_meta_query->get(' ');
                                                echo $javo_meta_query->get('phone1');
                                                ?>
                                                </li>

                                                <li><?php echo $javo_meta_query->get('website'); ?></li>
                                                <li><?php echo $javo_meta_query->get('email'); ?></li>
                                                </ul>
                                                <!--a class="btn btn-dark javo-this-go-more" href="<?php the_permalink(); ?>"><?php _e('Ver Mais', 'javo_fr'); ?></a-->
                                                </div> <!-- des -->

                                                <div class="pics">
                                                <div class="thumb">
                                                <a href="<?php the_permalink(); ?>">
                                                <?php
                                                if ($logotipo[0]) {
                                                    printf('<img src="%s" class="wp-post-image" style="width:130px; height:130px;">', $url[0]);
                                                } else {
                                                    printf('<img src="%s" style="width:130px; height:130px;">', $javo_tso->get('no_image', JAVO_IMG_DIR . '/no-image.png'));
                                                };
                                                ?>
                                                </a>
                                                </div> <!-- thumb -->                                                                <div class="img-in-text"><?php echo $javo_meta_query->cat('item_category', __('Sem Categoria', 'javo_fr')); ?></div>
                                                <div class="javo-left-overlay">
                                                <div class="javo-txt-meta-area"><?php echo $javo_meta_query->cat('item_location', __('Sem Localização', 'javo_fr')); ?></div>
                                                <div class="corner-wrap">
                                                <div class="corner"></div>
                                                <div class="corner-background"></div>
                                                </div> <!-- corner-wrap -->
                                                </div> <!-- javo-left-overlay -->
                                                </div> <!-- pic -->
                                                </div> <!-- javo_somw_info -->
                                            </script>
                                            <?php
                                        } else {
                                            get_template_part('content', 'free');
                                            ?>
                                            <script type="text/template" id="javo_map_tmp_<?php the_ID(); ?>">
                                                <div class="javo_somw_info panel">
                                                <div class="des">
                                                <h5><?php echo javo_str_cut(get_the_title(), 30); ?></h5>
                                                <ul class="list-unstyled">
                                                <li>
                                                <?php
                                                echo $javo_meta_query->get('(');
                                                echo $javo_meta_query->get('ddd1');
                                                echo $javo_meta_query->get(')');
                                                echo $javo_meta_query->get(' ');
                                                echo $javo_meta_query->get('phone1');
                                                ?>
                                                </li>

                                                <li><?php echo $javo_meta_query->get('website'); ?></li>
                                                <li><?php echo $javo_meta_query->get('email'); ?></li>
                                                </ul>
                                                <a class="btn btn-dark javo-this-go-more" href="<?php the_permalink(); ?>"><?php _e('More', 'javo_fr'); ?></a>
                                                </div> <!-- des -->
                                                <div class="pics">
                                                <div class="thumb">
                                                <a href="<?php the_permalink(); ?>">
                                                <?php
                                                if ($logotipo[0]) {
                                                    printf('<img src="%s" class="wp-post-image" style="width:130px; height:130px;">', $url[0]);
                                                } else {
                                                    printf('<img src="%s" style="width:130px; height:130px;">', $javo_tso->get('no_image', JAVO_IMG_DIR . '/no-image.png'));
                                                };
                                                ?>
                                                </a>
                                                </div> <!-- thumb --> <div class="img-in-text"><?php echo $javo_meta_query->cat('item_category', __('Sem Categoria', 'javo_fr')); ?></div>
                                                <div class="javo-left-overlay">
                                                <div class="javo-txt-meta-area"><?php echo $javo_meta_query->cat('item_location', __('Sem Localização', 'javo_fr')); ?></div>
                                                <div class="corner-wrap">
                                                <div class="corner"></div>
                                                <div class="corner-background"></div>
                                                </div> <!-- corner-wrap -->
                                                </div> <!-- javo-left-overlay -->
                                                </div> <!-- pic -->
                                                </div> <!-- javo_somw_info -->
                                            </script>
                                            <?php
                                        }
                                    } // End While