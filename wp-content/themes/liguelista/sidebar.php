﻿<?php
/**
 * The sidebar containing the main widget area
 *
 * If no active widgets are in the sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Javo_Directory
 * @since Javo Themes 1.0
 */
// Get global post object
global $post;

// Variable initialize
$javo_sidebar_lr = "right";

// Post object exist?
if (!empty($post->ID)) {

    // Get post object id
    $post_id = $post->ID;
    // Get display post sidebar option meta.
    $javo_sidebar_lr = trim((string) get_post_meta($post_id, 'javo_sidebar_type', true));
    // Set not exist meta value to default 'Right'
    $javo_sidebar_lr = !empty($javo_sidebar_lr) ? $javo_sidebar_lr : "right";
};
$template_name = is_page() ? basename(get_page_template()) : null;
?>

<div class="col-md-3 sidebar-<?php echo $javo_sidebar_lr; ?>">
    <?php
    $javo_meta = new javo_get_meta($post->ID);

    if (is_archive()) {
        echo '<div class="row">';
        get_template_part('templates/parts/part', 'archive-sidebar');
        echo '</div>';
    };
    ?>

    <?php
    if (is_singular("item") || $template_name == "tp-item.php" || is_singular("post") || $template_name == "tp-blogs.php") {
        if ($javo_meta->get('face') != '' || $javo_meta->get('twitter') != '' || $javo_meta->get('insta') != '' ||
                $javo_meta->get('youtube') != '' || $javo_meta->get('vimeo') != '' || $javo_meta->get('lnkdin') != '' ||
                $javo_meta->get('pint') != '' || $javo_meta->get('github') != '' || $javo_meta->get('fckr') != '' ||
                $javo_meta->get('behance') != '' || $javo_meta->get('tumblr') != '' || $javo_meta->get('4sqr') != '') {
            ?>
            <!-- Início Redes Sociais -->
            <div class="col-lg-12 siderbar-inner" style="margin-bottom: 10px;">
                <div class="row widgets-wraps">
                    <div class="widgettitle_wrap col-md-12">
                        <h2 class="widgettitle"><span>Redes Sociais</div></h2>
                    </div>
                    <div class="widget_posts_wrap">
                        <ul class="redes-sociais">
                            <?php if ($javo_meta->get('face') != '') { ?>
                                <li>
                                    <div class="img-wrap-shadow">
                                        <a href="<?php echo $javo_meta->get('face'); ?>">
                                            <img width="40" height="40" src="https://cdn4.iconfinder.com/data/icons/flat-icon-social-media/128/Facebook.png" alt="Facebook" title="Facebook">
                                        </a>
                                    </div>
                                </li>
                            <?php } if ($javo_meta->get('twitter') != '') { ?>
                                <li >
                                    <div class="img-wrap-shadow">
                                        <a href="<?php echo $javo_meta->get('twitter'); ?>">
                                            <img width="40" height="40" src="https://cdn4.iconfinder.com/data/icons/flat-icon-social-media/128/Twitter.png" alt="Twitter" title="Twitter">
                                        </a>
                                    </div>
                                </li>
                            <?php } if ($javo_meta->get('insta') != '') { ?>
                                <li >
                                    <div class="img-wrap-shadow">
                                        <a href="<?php echo $javo_meta->get('insta'); ?>">
                                            <img width="40" height="40" src="https://cdn4.iconfinder.com/data/icons/flat-icon-social-media/128/Instagram.png" alt="Instagram" title="Instagram">
                                        </a>
                                    </div>
                                </li>
                            <?php } if ($javo_meta->get('youtube') != '') { ?>
                                <li>
                                    <div class="img-wrap-shadow">
                                        <a href="<?php echo $javo_meta->get('youtube'); ?>">
                                            <img width="40" height="40" src="https://cdn4.iconfinder.com/data/icons/flat-icon-social-media/128/Youtube.png" alt="Youtube" title="Youtube">
                                        </a>
                                    </div>
                                </li>
                            <?php } if ($javo_meta->get('vimeo') != '') { ?>
                                <li>
                                    <div class="img-wrap-shadow">
                                        <a href="<?php echo $javo_meta->get('vimeo'); ?>">
                                            <img width="40" height="40" src="https://cdn4.iconfinder.com/data/icons/flat-icon-social-media/128/Vimeo.png" alt="Vimeo" title="Vimeo">
                                        </a>
                                    </div>
                                </li>
                            <?php } if ($javo_meta->get('lnkdin') != '') { ?>
                                <li>
                                    <div class="img-wrap-shadow">
                                        <a href="<?php echo $javo_meta->get('lnkdin'); ?>">
                                            <img width="40" height="40" src="https://cdn4.iconfinder.com/data/icons/flat-icon-social-media/128/Linkedin.png" alt="LinkedIn" title="LinkedIn">
                                        </a>
                                    </div>
                                </li>
                            <?php } if ($javo_meta->get('pint') != '') { ?>
                                <li>
                                    <div class="img-wrap-shadow">
                                        <a href="<?php echo $javo_meta->get('pint'); ?>">
                                            <img width="40" height="40" src="https://cdn4.iconfinder.com/data/icons/flat-icon-social-media/128/Pinterest.png" alt="Pinterest" title="Pinterest">
                                        </a>
                                    </div>
                                </li>
                            <?php } if ($javo_meta->get('github') != '') { ?>
                                <li>
                                    <div class="img-wrap-shadow">
                                        <a href="<?php echo $javo_meta->get('github'); ?>">
                                            <img width="40" height="40" src="https://cdn4.iconfinder.com/data/icons/flat-icon-social-media/128/Github.png" alt="Github" title="Github">
                                        </a>
                                    </div>
                                </li>
                            <?php } if ($javo_meta->get('fckr') != '') { ?>
                                <li>
                                    <div class="img-wrap-shadow">
                                        <a href="<?php echo $javo_meta->get('fckr'); ?>">
                                            <img width="40" height="40" src="https://cdn4.iconfinder.com/data/icons/flat-icon-social-media/128/Flickr.png" alt="Flickr" title="Flickr">
                                        </a>
                                    </div>
                                </li>
                            <?php } if ($javo_meta->get('behance') != '') { ?>
                                <li>
                                    <div class="img-wrap-shadow">
                                        <a href="<?php echo $javo_meta->get('behance'); ?>">
                                            <img width="40" height="40" src="https://cdn4.iconfinder.com/data/icons/flat-icon-social-media/128/Behance.png" alt="Behance" title="Behance">
                                        </a>
                                    </div>
                                </li>
                            <?php } if ($javo_meta->get('tumblr') != '') { ?>
                                <li>
                                    <div class="img-wrap-shadow">
                                        <a href="<?php echo $javo_meta->get('tumblr'); ?>">
                                            <img width="40" height="40" src="https://cdn4.iconfinder.com/data/icons/flat-icon-social-media/128/Tumblr.png" alt="Tumblr" title="Tumblr">
                                        </a>
                                    </div>
                                </li>
                            <?php } if ($javo_meta->get('4sqr') != '') { ?>
                                <li>
                                    <div class="img-wrap-shadow">
                                        <a href="<?php echo $javo_meta->get('4sqr'); ?>">
                                            <img width="40" height="40" src="https://cdn4.iconfinder.com/data/icons/flat-icon-social-media/128/Foursquare.png" alt="Foursquare" title="Foursquare">
                                        </a>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            <?php } ?>
            <!-- Fim Redes Sociais -->

            <!-- Início Galaria de Imagens -->
            <?php
            $javo_related_photos = (Array) @unserialize(get_post_meta($post->ID, "related_images", true));
            $javo_related_photos[] = get_post_thumbnail_id($post->ID);
            if ($javo_related_photos[0]) {
                ?>
                <div class="col-lg-12 siderbar-inner" style="margin-bottom: 10px;">
                    <div class="row widgets-wraps">
                        <div class="widgettitle_wrap col-md-12">
                            <h2 class="widgettitle"><span>Galeria de Imagens</span></h2>
                        </div>
                        <div class="widget_posts_wrap popup-gallery">
                            <ul class="galeria">
                                <?php
                                foreach ($javo_related_photos as $index => $image) {
                                    $url = wp_get_attachment_image_src($image, 'thumbnail');
                                    if (!$url)
                                        continue;
                                    $javo_this_image_meta = wp_get_attachment_image_src($image, 'full');
                                    $javo_this_image = $javo_this_image_meta[0];
                                    ?>
                                    <li class="img-wrap-shadow">
                                        <span class="thumb">
                                            <a href="<?php echo $javo_this_image; ?>" style="cursor:pointer;">
                                                <div class="img-wrap-shadow">
                                                    <?php
                                                    printf('<img src="%s" class="wp-post-image" style="width:80px; height:80px;">', $url[0]);
                                                    ?>
                                                </div>
                                            </a>
                                        </span>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div> <!-- pp-siderbar inner -->
                <!-- Fim Galaria de Imagens -->
                <div class="col-lg-12 siderbar-inner" style="margin-bottom: 10px;">
                    <?php
                };
            }
            //$template_name = is_page() ? basename(get_page_template()) : null;
            if (
                    is_singular("item") ||
                    $template_name == "tp-item.php"
            ) {
                dynamic_sidebar("item Sidebar");
            } elseif (
                    is_singular("post") ||
                    $template_name == "tp-blogs.php"
            ) {
                dynamic_sidebar("Blog Sidebar");
            } else {
                dynamic_sidebar("default Sidebar");
            };
            ?>
        </div> <!-- pp-siderbar inner -->
    </div><!-- Side bar -->