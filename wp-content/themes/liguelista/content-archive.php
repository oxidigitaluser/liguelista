﻿<?php
/**
 * The template for displaying posts in the Status post format
 *
 * @package WordPress
 * @subpackage Javo_Directory
 * @since Javo Themes 1.0
 */
global
$javo_tso
, $javo_custom_item_tab;

$javo_meta_query = new javo_GET_META(get_the_ID());
$javo_item_location = wp_get_post_terms(get_the_ID(), 'item_location', array('order' => 'DESC', 'orderby' => 'parent'));
$cidade = $javo_meta_query->get('city') != '' ? $javo_meta_query->get('city') : $javo_item_location[0]->name . " - " . get_sigla_estado($javo_item_location[1]->name);
$logotipo = (Array) @unserialize(get_post_meta(get_the_ID(), "logotipo_images", true));
$url = wp_get_attachment_image_src($logotipo[0], Array(130, 130));
?>

<!-- MINISITE -->
<div class="col-md-12" style="padding: 0px;">
    <div id="post-<?php the_ID(); ?>" <?php post_class('minisite_borda'); ?>>
        <div class="row" style="padding: 0px;margin-right: 0px;margin-left: 0px;">
            <div class="col-md-12 javo-archive-list" style="padding: 0px;">
                <div class="media">
                    <div class="pull-left archive-thumb">
                        <a href="<?php the_permalink(); ?>">
                            <?php
                            if ($logotipo[0]) {
                                printf('<img src="%s" class="wp-post-image" style="width:130px; height:130px;">', $url[0]);
                            } else {
                                printf('<img src="%s" style="width:130px; height:130px;">', $javo_tso->get('no_image', JAVO_IMG_DIR . '/no-image.png'));
                            };
                            ?>
                        </a>
                    </div><!-- /.pull-left -->
                    <div class="media-body">
                        <div class="row archive-line-title-wrap">
                            <div class="col-md-9" style="width: 100%;">
                                <div class="row">
                                    <div class="pull-left archive-line-titles">
                                        <h3 class="media-heading">
                                            <a href="<?php the_permalink(); ?>" style="color: #1e73be;font-size:24;"><?php the_title(); ?></a>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-md-12 javo-archive-list-excerpt">
                                <a href="<?php the_permalink(); ?>" style="text-decoration: none;">
                                    <div class="javo-archive-list-inner-excerpt" style="text-align: left;text-indent: 0;text-decoration: none;font-size: 14px;line-height: 18.2px;color: #333333;font-style: normal;font-weight: normal;font-family: Arial, 'Helvetica Neue', Helvetica, serif;height:95px;">
                                        <?php
                                        echo the_content();
                                        ?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="minisite_rodape">
            <div class="row" style="width: 100%;padding: 0px;margin-right: 0px;margin-left: 0px;">
                <address>
                    <div class="resultadopesquisa_minisite" style="width: 30%;">

                        <?php if ($javo_meta_query->get('address') != '') { ?>
                            <div style="margin-top:5px;">
                                <?php echo $javo_meta_query->get('address'); ?>
                            </div>
                        <?php } ?>
                        <?php if ($javo_meta_query->get('complement') != '') { ?>
                            <div style="margin-top:5px;">
                                <?php echo $javo_meta_query->get('complement'); ?>
                            </div>
                        <?php } ?>
                        <?php if ($javo_meta_query->get('district') != '' && $javo_meta_query->get('district') != ' ') { ?>
                            <div style="margin-top:5px;">
                                <?php echo $javo_meta_query->get('district'); ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="resultadopesquisa_minisite" style="width: 25%;">
                        <?php if ($cidade != '') { ?>
                            <div style="margin-top:5px;">
                                <?php echo $cidade; ?>
                            </div>
                        <?php } ?>

                        <?php if ($javo_meta_query->get('cep') != '') { ?>
                            <div style="margin-top:5px;">
                                <span style="font-weight: 300;"><?php _e('CEP: ', 'javo_fr'); ?></span>
                                <?php echo $javo_meta_query->get('cep'); ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="resultadopesquisa_minisite" style="text-align: left;width: 30%;word-wrap: break-word;">
                        <div>
                            <?php if ($javo_meta_query->get('website') != '') { ?>
                                <div style="margin-top:5px;">
                                    <span style="font-weight: 300;">
                                        <a href="http://<?php echo $javo_meta_query->get('website'); ?>" target="_blank" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"><?php echo $javo_meta_query->get('website'); ?></a>
                                    </span>
                                </div>
                            <?php } ?>
                            <?php if ($javo_meta_query->get('email') != '') { ?>
                                <div style="margin-top:5px;">
                                    <span style="font-weight: 300;">
                                        <?php echo $javo_meta_query->get('email'); ?>
                                    </span>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="resultadopesquisa_minisite" style="text-align: right;padding-right: 5px;width: 15%;">
                        <div class="versite">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" style="color: #fff;text-decoration: none;font-weight: 300;">VER MAIS</a>
                        </div>

                        <div style="float: right;padding-left: 5px;">
                            <?php if ($javo_meta_query->get('phone1') != '' && $javo_meta_query->get('phone1') != ', ') { ?><div style="margin-top:5px;"><span class="quebratelefone"><?php echo $javo_meta_query->get('phone1'); ?></span></div><?php } ?>
                            <?php if ($javo_meta_query->get('phone2') != '' && $javo_meta_query->get('phone2') != ', ') { ?><div style="margin-top:5px;"><span class="quebratelefone"><?php echo $javo_meta_query->get('phone2'); ?></span></div><?php } ?>
                            <?php if ($javo_meta_query->get('phone3') != '' && $javo_meta_query->get('phone3') != ', ') { ?><div style="margin-top:5px;"><span class="quebratelefone"><?php echo $javo_meta_query->get('phone3'); ?></span></div><?php } ?>
                        </div>

                        <div style="float: right;">
                            <?php if ($javo_meta_query->get('phone1') != '' && $javo_meta_query->get('phone1') != ', ') { ?><div style="margin-top:5px;"><span class="quebraddd"><?php
                            echo ('(');
                            echo $javo_meta_query->get('ddd1');
                            echo (')');
                                ?></span></div><?php } ?>
                            <?php if ($javo_meta_query->get('phone2') != '' && $javo_meta_query->get('phone2') != ', ') { ?><div style="margin-top:5px;"><span class="quebraddd"><?php
                            echo ('(');
                            echo $javo_meta_query->get('ddd2');
                            echo (')');
                                ?></span></div><?php } ?>
                            <?php if ($javo_meta_query->get('phone3') != '' && $javo_meta_query->get('phone3') != ', ') { ?><div style="margin-top:5px;"><span class="quebraddd"><?php
                            echo ('(');
                            echo $javo_meta_query->get('ddd3');
                            echo (')');
                                ?></span></div><?php } ?>
                        </div>

                        <div style="float: right;width: 100%;" class="quebratelefone">
                            <?php if ($javo_meta_query->get('phone0800') != '' && $javo_meta_query->get('phone0800') != ', ') { ?><div style="margin-top:5px;"><span class="quebratelefone"><?php echo $javo_meta_query->get('phone0800'); ?></span></div><?php } ?>
                        </div>
                    </div>
                </address>
            </div>
        </div> 
    </div>
</div>
