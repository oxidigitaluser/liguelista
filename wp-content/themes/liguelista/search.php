﻿<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Javo_Directory
 * @since Javo Themes 1.0
 */
global $wp_query, $wpd;
 
get_header();

?>
<div class="javo-archive-header-container">
    <div class="javo-search-map-area"></div>
    <div class="javo-archive-header-search-bar">
        <?php echo do_shortcode('[javo_search_form]'); ?>
    </div>
</div>
<div class="container">
    <!-- RESULTADO PESQUISA-->
    <div class="resultadopesquisa" style="margin-top: -50px;">
        <div class="col-md-9 main-content-wrap">
            <div class="row" style="width: 100%;padding: 0px;margin-right: 0px;margin-left: 0px;">
                <div style="height: 20px;width:100%"></div>
                <div class="col-md-12" style="padding: 0px;">
                    <?php
                    $javo_this_posts_return = Array();
                    if (have_posts()) {
                        ?>
                        <div class="col-md-12" style="padding: 0px;">
                            <div class="panel-group" id="accordion" style="margin-bottom: 5px;">
                                <div id="content-search" class="javo-output">
                                    <?php while (have_posts()) {
                                        the_post();
                                        $javo_this_latlng = @unserialize(get_post_meta(get_the_ID(), 'latlng', true));
                                        $plano_comercial = get_post_meta(get_the_ID(), "plano_comercial", true);
                                        $javo_this_posts_return[get_the_ID()]['latlng'] = $javo_this_latlng;
                                        $javo_meta_query = new javo_get_meta(get_the_ID());
                                        $javo_set_icon = '';
                                        $javo_marker_term_id = wp_get_post_terms(get_the_ID(), 'item_category');
                                        $javo_item_location = wp_get_post_terms(get_the_ID(), 'item_location', array('order' => 'ASC', 'orderby' => 'parent'));
                                        $logotipo = (Array) @unserialize(get_post_meta(get_the_ID(), "logotipo_images", true));
                                        $url = wp_get_attachment_image_src($logotipo[0], Array(130, 130));

                                        if (!empty($javo_marker_term_id)) {
                                            $javo_set_icon = get_option('javo_item_category_' . $javo_marker_term_id[0]->term_id . '_marker', '');
                                            if ($javo_set_icon == '') {
                                                $javo_set_icon = $javo_tso->get('map_marker', '');
                                            };
                                            $javo_this_posts_return[get_the_ID()]['icon'] = $javo_set_icon;
                                        };

                                        /* PLANO MINISITE */
                                        if ($plano_comercial == '0') { //MINISITE
                                            get_template_part('content', 'archive');
                                        } else if ($plano_comercial == '1') { //MDVIT
                                            get_template_part('content', 'mdvit');
                                        } else if ($plano_comercial == '2') { //MDL
                                            get_template_part('content', 'mdl');
                                        } else if ($plano_comercial == '3') { //MDN
                                            get_template_part('content', 'mdn');
                                        } else {
                                            get_template_part('content', 'free');
                                        }
                                        ?>
                                        <script type="text/template" id="javo_map_tmp_<?php the_ID(); ?>">
                                            <div class="javo_somw_info panel">
                                            <div class="des">
                                            <h5><?php echo javo_str_cut(get_the_title(), 30); ?></h5>
                                            <ul class="list-unstyled">

                                            <?php if ($javo_meta_query->get('phone1') != '') { ?>
                                                <li><?php
                                                echo ('(');
                                                echo $javo_meta_query->get('ddd1');
                                                echo (')');
                                                echo (' ');
                                                echo $javo_meta_query->get('phone1');
                                                ?></li>
                                            <?php } if ($javo_meta_query->get('address') != '') { ?>
                                                <li><?php echo $javo_meta_query->get('address'); ?></li>
                                            <?php } if ($javo_meta_query->cat('item_location') != '') { ?>
                                                <li><?php echo $javo_item_location[0]->name . " - " . get_sigla_estado($javo_item_location[1]->name); ?></li>
                                            <?php } ?>
                                            </ul>
                                            </div>

                                            <div class="pics">
                                            <div class="thumb">

                                            <?php
                                            if ($logotipo[0]) {
                                                printf('<img src="%s" class="wp-post-image" style="width:130px; height:130px;">', $url[0]);
                                            } else {
                                                printf('<img src="%s" style="width:130px; height:130px;">', $javo_tso->get('no_image', JAVO_IMG_DIR . '/no-image.png'));
                                            };
                                            ?>

                                            </div> <!-- thumb -->
                                            <div class="img-in-text"><?php echo $javo_meta_query->cat('item_category', __('Sem Categoria', 'javo_fr')); ?></div>
                                            <div class="javo-left-overlay">
                                            <div class="javo-txt-meta-area"><?php echo $javo_meta_query->cat('item_location', __('Sem Localização', 'javo_fr')); ?></div>
                                            <div class="corner-wrap">
                                            <div class="corner"></div>
                                            <div class="corner-background"></div>
                                            </div> <!-- corner-wrap -->
                                            </div> <!-- javo-left-overlay -->
                                            </div> <!-- pic -->
                                            </div> <!-- javo_somw_info -->
                                        </script>
                                        <?php
                                    } // End While

                                    if (!empty($_GET['s'])) {
                                        $loc = get_term($_GET['location'], "item_location");
                                        
                                        printf("<input type='hidden' name='javo-page-title' value='%s'>", $wp_query->found_posts . " resultados para " . $_GET['s'] . " em " . $loc->name);
                                    } else {
                                        $loc = get_term($_GET['location'], "item_location");
                                        $cat = get_term($_GET['category'], "item_category");

                                        printf("<input type='hidden' name='javo-page-title' value='%s'>", $wp_query->found_posts . " resultados para " . $cat->name . " em " . $loc->name);
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding: 0px;">
                                <div class="javo_pagination">
                                    <?php
                                    global $wp_query;

                                    $big = 999999999; // need an unlikely integer
                                    echo paginate_links(array(
                                        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                                        'format' => '?paged=%#%',
                                        'current' => max(1, get_query_var('paged')),
                                        'total' => $wp_query->max_num_pages
                                    ));
                                    ?>
                                </div><!-- javo_pagination -->
                            </div><!-- /.col-md-12 -->
                        </div><!-- /.row -->
                        <?php
                    } else {
                        ?>
                        <h3 class="page-header margin-top-12"><?php _e('No result found. Please try again', 'javo_fr'); ?></h3>
                        <?php
                    }; // End IF
                    printf("<input type='hidden' name='javo-this-term-posts-latlng' value='%s'>", json_encode($javo_this_posts_return));
                    wp_reset_query();
                    ?>
                </div>
            </div><!-- row -->
        </div><!-- col-md-9 -->
    </div><!-- RESULTADO PESQUISA-->
    <!-- BARRA LATERAL -->
    <div class="barralateral">
        <div class="col-md-3 sidebar-right">
            <hr>
            <p style="text-align: center;font-size: 16px;line-height: 30px;margin-top: 10px;">
                Foram encontrados <span style="color:#f45f00;"><?php printf('%s', $wp_query->found_posts); ?></span> anúncios para a sua pesquisa!
            </p>
            <hr>
            <h2 style="text-align: center;text-transform: uppercase;font-size: 20px;line-height: 30px;">Categorias relacionadas a sua pesquisa</h2>
            <style>
                .sidebar-box {
                    max-height: 350px;
                    position: relative;

                    overflow: hidden;
                }
                .sidebar-box .read-more {
                    position: absolute;
                    bottom: 0; left: 0;
                    width: 100%;
                    text-align: center;
                    margin: 0;
                    padding: 20px 0 20px 0px;
                    background-color: #ffffff;
                }
            </style>
            <div class="sidebar-box">
                <div class="categorias_resultado" style="margin-bottom: 30px;">
                    <?php
                    echo '<ul id="triple" style="padding: 0px;">';
                    $added = array();
                    while (have_posts()) {
                        the_post();
                        $categories = wp_get_post_terms(get_the_ID(), 'item_category');

                        foreach ($categories as $category) {
                            if (search_array($category->name, $added) || $category->term_id == 1015)
                                continue;

                            array_push($added, array("nome" => $category->name, "codigo" => $category->term_id));
                        }
                    }
                    asort($added);

                    foreach ($added as $category2) {
                        $url = split('&', $_SERVER['REQUEST_URI']);
                        $urlNova = $url[0] . '&' . "category=" . $category2["codigo"] . '&' . $url[2] . '&s=';
                        echo "<li><a href='" . $urlNova . "'>" . $category2["nome"] . "</a></li>";
                    }
                    echo '</ul>';
                    ?>
                </div>

                <div style="height: 30px;width:100%"></div>
                <div class="read-more"><a href="#" class="button">VER MAIS CATEGORIAS</a></div>
            </div>
            <hr>
        </div>
    </div><!-- FIM BARRA LATERAL -->

    <fieldset>
        <input type="hidden" javo-map-distance-unit value="<?php echo $javo_tso_map->get('distance_unit', __('km', 'javo_fr')); ?>">
        <input type="hidden" javo-map-distance-max value="<?php echo (float) $javo_tso_map->get('distance_max', '500'); ?>">
    </fieldset>
    <script type="text/javascript">
        jQuery(function ($) {
            var $el, $ps, $up, totalHeight;

            $(".sidebar-box .button").click(function () {
                // IE 7 doesn't even get this far. I didn't feel like dicking with it.
                totalHeight = 0

                $el = $(this);
                $p = $el.parent();
                $up = $p.parent();
                $ps = $up.find("div:not('.read-more')");

                // measure how tall inside should be by adding together heights of all inside paragraphs (except read-more paragraph)
                $ps.each(function () {
                    totalHeight += $(this).outerHeight();
                    // FAIL totalHeight += $(this).css("margin-bottom");
                });

                $up.css({
                    // Set height to prevent instant jumpdown when max height is removed
                    "height": $up.height("100%"),
                    "max-height": 9999
                }).animate({
                    "height": totalHeight
                });

                // fade out read-more
                $p.fadeOut();

                // prevent jump-down
                return false;
            });
        });

        jQuery(function ($) {
            var javo_archive_list = {
                /*****************************************
                 **
                 ** Variables
                 **
                 *****************************************/
                el: $('.javo-search-map-area'),
                distance_unit: $('[javo-map-distance-unit]').val(),
                distance: $('[javo-map-distance-unit]').val() == 'mile' ? 1609.344 : 1000,
                distance_max: $('[javo-map-distance-max]').val(),
                ob_ib: null,
                markers: null,
                bound: new google.maps.LatLngBounds(),
                sanitize_marker: JSON.parse($('input[name="javo-this-term-posts-latlng"]').val()),
                options: {
                    /* InfoBubble Option */
                    info_bubble: {
                        minWidth: 362,
                        minHeight: 180,
                        overflow: true,
                        shadowStyle: 1,
                        padding: 5,
                        borderRadius: 10,
                        arrowSize: 20,
                        borderWidth: 1,
                        disableAutoPan: false,
                        hideCloseButton: false,
                        arrowPosition: 50,
                        arrowStyle: 0
                    }
                    /* Display Ratings */
                    ,
                    raty: {
                        starOff: '<?php echo JAVO_IMG_DIR ?>/star-off-s.png',
                        starOn: '<?php echo JAVO_IMG_DIR ?>/star-on-s.png',
                        starHalf: '<?php echo JAVO_IMG_DIR ?>/star-half-s.png',
                        half: true,
                        readOnly: true
                    }
                    /* Map */
                    ,
                    map_init: {
                        map: {
                            options: {
                                center: new google.maps.LatLng(0, 0)
                            }
                        },
                        marker: {
                            events: {}
                            /*
                             ,
                             cluster: {
                             radius: 100,
                             0: {
                             content: '<div class="javo-map-cluster admin-color-setting">CLUSTER_COUNT</div>',
                             width: 52,
                             height: 52
                             },
                             events: {
                             click: function(c, e, d) {
                             c.main.map.setZoom(c.main.map.getZoom() + 1);
                             c.main.map.panTo(d.data.latLng);

                             }
                             }
                             }
                             */
                        }
                    }
                }
                /*****************************************
                 **
                 ** Main Funciton
                 **
                 *****************************************/
                ,
                init: function () {
                    /* Get Self Oboject */
                    var $object = this;

                    /* Define InfoBubble Plug-in */
                    this.ob_ib = new InfoBubble(this.options.info_bubble);

                    /* Set Marker Variable */
                    this.markers = new Array();

                    /* Get Marker Informations */
                    $.each(this.sanitize_marker, function (i, k) {
                        if (k.latlng.lat == "" || k.latlng.lng == "") {
                            return;
                        }
                        ;
                        $object.markers.push({
                            id: '#javo_map_tmp_' + i,
                            latLng: [k.latlng.lat, k.latlng.lng],
                            options: {
                                icon: k.icon
                            }
                        });
                        $object.bound.extend(new google.maps.LatLng(k.latlng.lat, k.latlng.lng));
                    });

                    /* Set bind Markers */
                    this.options.map_init.marker.values = this.markers;
                    this.options.map_init.marker.events.click = this.marker_click;

                    /* Define Google Map for Div Element */
                    this.el.height(500).gmap3(this.options.map_init);

                    this.map = this.el.gmap3('get');

                    if (this.markers.length > 0) {
                        this.map.fitBounds(this.bound);
                    }

                    /* Set Ratings */
                    $('.javo_archive_list_rating').each(function () {
                        $object.options.raty.score = $(this).data('score');
                        $(this).raty($object.options.raty).width('');
                    });
                    var javo_search_position_slide_option = {
                        start: [300],
                        step: 1,
                        range: {
                            min: [1],
                            max: [parseInt($object.distance_max)]
                        },
                        serialization: {
                            lower: [
                                $.Link({
                                    target: $('[javo-wide-map-round]'),
                                    format: {
                                        decimals: 0
                                    }
                                }), $.Link({
                                    target: '-tooltip-<div class="javo-slider-tooltip"></div>',
                                    method: function (v) {
                                        $(this).html('<span>' + v + '&nbsp;' + $object.distance_unit + '</span>');
                                    },
                                    format: {
                                        decimals: 0,
                                        thousand: ','
                                    }
                                })
                            ]
                        }
                    };
                    /*
                     Geo Location Slider Block

                     $('[data-javo-search-form]')
                     .find(".javo-position-slider")
                     .noUiSlider(javo_search_position_slide_option)
                     .on('set', $object.geolocation);
                     */
                },
                geolocation: function () {
                    var $this = $(this);
                    var $object = javo_archive_list;
                    var $radius = $('[javo-wide-map-round]').val();

                    $object.el.gmap3({
                        getgeoloc: {
                            callback: function (latlng) {
                                if (!latlng) {
                                    $.javo_msg({
                                        content: 'Your position access failed.'
                                    });
                                    return false;
                                }
                                ;
                                $(this).gmap3({
                                    clear: 'circle'
                                });
                                $(this).gmap3({
                                    map: {
                                        options: {
                                            center: latlng,
                                            zoom: 12
                                        }
                                    },
                                    circle: {
                                        options: {
                                            center: latlng,
                                            radius: $radius * parseFloat($object.distance),
                                            fillColor: '#464646',
                                            strockColor: '#000000'
                                        }
                                    }
                                });
                                $(this).gmap3({
                                    get: {
                                        name: 'circle',
                                        callback: function (c) {

                                            $(this).gmap3('get').fitBounds(c.getBounds());
                                        }
                                    }
                                });
                            }
                        }
                    });
                },
                marker_click: function (m, e, c) {
                    var $object = javo_archive_list;

                    var $this_map = $(this).gmap3('get');
                    $object.ob_ib.setContent($(c.id).html());
                    $object.ob_ib.open($this_map, m);
                    $this_map.setCenter(m.getPosition());
                }
            };
            
            javo_archive_list.init();

            $(document).attr("title", jQuery('[name="javo-page-title"]').val() + " | LigueLista");
        });
    </script>
</div>
<?php get_footer(); ?>