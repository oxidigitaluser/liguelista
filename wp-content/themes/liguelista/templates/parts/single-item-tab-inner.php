<?php
$javo_directory_query = new javo_get_meta(get_the_ID());
$javo_rating = new javo_Rating(get_the_ID());
global
$javo_custom_field
, $post
, $javo_custom_item_label
, $javo_custom_item_tab
, $javo_tso;
$javo_this_author = get_userdata($post->post_author);
$javo_this_author_avatar_id = get_the_author_meta('avatar');
$javo_directory_query = new javo_get_meta(get_the_ID());
$javo_rating = new javo_Rating(get_the_ID());
?>

<style>
    /** item tabs **/
    .nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus {background:rgba(0,0,0,0); border-radius:0px;}
    #javo-single-tab {border:1px #ddd solid; background:#fff; padding:20px; border-top:0;}
    .tabs-wrap {margin:3.7em 0.3em;}
    .single-item-tab #single-tabs li {list-style-type: none;background: #fff;border-top: 1px #ddd solid;;border-bottom: 1px #ddd solid;}
    .single-item-tab #single-tabs li.active {background: #0083db; border-top:1px #0083db solid;border-bottom:1px #0083db solid;}
    .single-item-tab ul#single-tabs {border-left:1px #ddd solid; border-right:1px #ddd solid;}
    .single-item-tab .nav>li>a:hover {border-radius:0; background: #fff; color:#0083db;}
    .single-item-tab .nav>li.active>a:hover {background: #0083db; color:#fff !important;}

    ul#single-tabs li.active{ background: <?php echo $javo_tso->get('total_button_color'); ?> !important; border-color: <?php echo $javo_tso->get('total_button_color'); ?> !important;}
    ul#single-tabs li.active a:hover{ color:#ddd !important; background: <?php echo $javo_tso->get('total_button_color'); ?> !important; }
    ul#single-tabs li a:hover{ color: <?php echo $javo_tso->get('total_button_color'); ?> !important; }
</style>

<div class="tabs-wrap">
    <ul id="single-tabs" class="nav nav-pills nav-justified" data-tabs="single-tabs">
        <li class="active">
            <a href="#item-detail" data-toggle="tab">
                <span class="glyphicon glyphicon-home"></span>&nbsp;
                <?php _e($javo_custom_item_label->get('about', 'About Us'), 'javo_fr'); ?>
            </a>
        </li>
        <?php if ($javo_custom_item_tab->get('location', '') == ''): ?>
            <li>
                <a href="#item-location" data-toggle="tab">
                    <span class="glyphicon glyphicon-map-marker"></span>&nbsp;
                    <?php _e($javo_custom_item_label->get('location', 'Location'), 'javo_fr'); ?>
                </a>
            </li>
        <?php endif; ?>

        <?php if ($javo_custom_item_tab->get('events', '') == ''): ?>
            <li>
                <a href="#item-events" data-toggle="tab">
                    <span class="glyphicon glyphicon-heart-empty"></span>&nbsp;
                    <?php _e($javo_custom_item_label->get('events', 'Events'), 'javo_fr'); ?>
                </a>
            </li>
        <?php endif; ?>

        <?php if ($javo_custom_item_tab->get('ratings', '') == ''): ?>
            <li>
                <a href="#item-ratings" data-toggle="tab">
                    <span class="glyphicon glyphicon-star"></span>&nbsp;
                    <?php _e($javo_custom_item_label->get('ratings', 'Ratings'), 'javo_fr'); ?>
                </a>
            </li>
        <?php endif; ?>

        <?php if ($javo_custom_item_tab->get('reviews', '') == ''): ?>
            <li>
                <a href="#item-reviews" data-toggle="tab">
                    <span class="glyphicon glyphicon-comment"></span>&nbsp;
                    <?php _e($javo_custom_item_label->get('reviews', 'Reviews'), 'javo_fr'); ?>
                </a>
            </li>
        <?php endif; ?>
    </ul>
    <div id="javo-single-tab" class="tab-content">
        <div class="tab-pane active" id="item-detail">
            <?php get_template_part('templates/parts/part', 'single-detail-tab'); ?>
        </div>
        <?php if ($javo_custom_item_tab->get('location', '') == ''): ?>
            <div class="tab-pane" id="item-location">
                <?php get_template_part('templates/parts/part', 'single-maps'); ?>
                <p>&nbsp;</p>
                <?php get_template_part('templates/parts/part', 'single-contact'); ?>
            </div>
        <?php endif; ?>

        <?php if ($javo_custom_item_tab->get('events', '') == ''): ?>
            <div class="tab-pane" id="item-events">
                <?php get_template_part('templates/parts/part', 'single-events'); ?>
            </div>
        <?php endif; ?>

        <?php if ($javo_custom_item_tab->get('ratings', '') == ''): ?>
            <div class="tab-pane" id="item-ratings">
                <?php get_template_part('templates/parts/part', 'single-ratings-tab'); ?>
            </div>
        <?php endif; ?>

        <?php if ($javo_custom_item_tab->get('reviews', '') == ''): ?>
            <div class="tab-pane" id="item-reviews">
                <?php get_template_part('templates/parts/part', 'single-reviews'); ?>
            </div>
        <?php endif; ?>


    </div>
</div> <!-- tabs-wrap -->

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#single-tabs').tab();
        // link to specific single-tabs	
        var hash = location.hash
                , hashPieces = hash.split('?')
                , activeTab = hashPieces[0] != '' ? $('[href=' + hashPieces[0] + ']') : null;
        activeTab && activeTab.tab('show');
    });
</script>




<?php
// This post exists to latlng meta then,
if (!empty($latlng['lat']) && !empty($latlng['lng'])) {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            "use strict";
            var option = {
                map: {
                    options: {
                        center: new google.maps.LatLng(<?php echo $latlng['lat']; ?>, <?php echo $latlng['lng']; ?>),
                        zoom: 15,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        mapTypeControl: false,
                        navigationControl: true,
                        scrollwheel: false,
                        streetViewControl: true
                    }
                },
                marker: {
                    latLng: [<?php echo $latlng['lat']; ?>, <?php echo $latlng['lng']; ?>],
                    draggable: true
                }
            };
            var header_option = {
                map: {
                    options: {
                        center: new google.maps.LatLng(<?php echo $latlng['lat']; ?>, <?php echo $latlng['lng']; ?>),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        navigationControl: true,
                        streetViewControl: true
                    }
                }, streetviewpanorama: {
                    options: {
                        container: $(".map_area.header")
                        , opts: {
                            position: new google.maps.LatLng(<?php echo $latlng['lat']; ?>, <?php echo $latlng['lng']; ?>)
                            , pov: {heading: 34, pitch: 10, zoom: 1}
                        }
                    }
                }
            };
            $(".javo-single-map-area").css("height", $(".javo-single-map-area").parent().outerHeight() + 'px').gmap3(option);
        });
    </script>


<?php }; ?>