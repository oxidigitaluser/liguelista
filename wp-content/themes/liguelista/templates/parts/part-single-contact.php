<?php
global $javo_custom_field, $javo_tso, $post;
$javo_this_author				= get_userdata($post->post_author);
$javo_this_author_avatar_id		= get_user_meta($javo_this_author->ID, 'avatar', true);
$javo_directory_query			= new javo_get_meta( get_the_ID() );
$javo_item_location = wp_get_post_terms(get_the_ID(), 'item_location', array('order' => 'DESC', 'orderby' => 'parent'));


echo apply_filters('javo_shortcode_title', __('Localização / Contato', 'javo_fr'), get_the_title() );
?>

<!-- total contact start -->
<div class="row single-contact-wrap">
	<div class="col-md-6 javo-animation x2 javo-left-to-right-999">
		<div class="single-contact-info">
		<ul class="inner-items">
					<?php if ($javo_directory_query->get('address') != '') { ?>
                        <li>
                            <?php echo $javo_directory_query->get('address'); echo (' '); ?>
                        </li>
                    <?php } if ($javo_directory_query->get('complement') != '') { ?>
                        <li>
                            <?php echo $javo_directory_query->get('complement'); echo (' '); ?>
                        </li>
                    <?php } if ($javo_directory_query->get('district') != '') { ?>
                        <li>
                            <?php echo $javo_directory_query->get('district'); echo (' '); ?>
                        </li>
                    <?php } if ($javo_directory_query->get('cep') != '') { ?>
                        <li>
                    		<?php echo $javo_directory_query->get('cep'); echo (' '); ?>
                        </li>
                    <?php } ?>
                    	<li>
                    		<?php echo $javo_item_location[0]->name . " - " . get_sigla_estado($javo_item_location[1]->name); ?>
                    	</li>


			</ul>

		</div> <!-- single-contact-info -->
	</div> <!-- col-md-6 -->


<div class="col-md-6 javo-animation x2 javo-left-to-right-999">
		<div class="single-contact-info">
		<ul class="inner-items">

			<?php if ($javo_directory_query->get('phone1') != '' && $javo_directory_query->get('phone1') != ', ') { ?>
                        <li>
                            <?php
                            echo ('(');echo $javo_directory_query->get('ddd1');echo (') ');
                            echo $javo_directory_query->get('phone1');
                            ?>
                        </li>
                    <?php } ?>
                    <?php if ($javo_directory_query->get('phone2') != '' && $javo_directory_query->get('phone2') != ', ') { ?>
                        <li>
                            <?php
                            echo ('(');echo $javo_directory_query->get('ddd2');echo (') ');
                            echo $javo_directory_query->get('phone2');
                            ?>
                        </li>
                    <?php } ?>
                    <?php if ($javo_directory_query->get('phone3') != '' && $javo_directory_query->get('phone3') != ', ') { ?>
                        <li>
                            <?php
                            echo ('(');echo $javo_directory_query->get('ddd3');echo (') ');
                            echo $javo_directory_query->get('phone3');
                            ?>
                        </li>
                    <?php } ?>
                    <?php if ($javo_directory_query->get('phone0800') != '' && $javo_directory_query->get('phone0800') != ', ') { ?>
                        <li>
                            <?php echo $javo_directory_query->get('phone0800'); ?>
                        </li>
                    <?php } ?>
                    <?php
                    if ($javo_directory_query->get('email', NULL) != NULL) {
                        printf('<li><span>%s</span><a href="mailto:%s" target="_self">%s</a></li>'
                                , __('', 'javo_fr')
                                , $javo_directory_query->get('email', NULL)
                                , $javo_directory_query->get('email', NULL)
                        );
                    }
                    if ($javo_directory_query->get('website', NULL) != NULL) {
                        printf('<li><span>%s</span><a href="%s" target="_blank">%s</a></li>'
                                , __('', 'javo_fr')
                                , ( strpos($javo_directory_query->get('website'), 'http://') !== false ?
                                        $javo_directory_query->get('website') :
                                        'http://' . $javo_directory_query->get('website')
                                )
                                , $javo_directory_query->get('website', NULL)
                        );
                    }
                    ?>
			</ul>

		</div> 
	</div> 





</div>


<!-- total contact end -->
