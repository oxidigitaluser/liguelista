<?php
global $javo_tso;
$javo_query = new javo_ARRAY( $_POST );
$item_id = $javo_query->get('item_id', 0);
?>
<div class="javo-add-item-step-3">
	<?php 
	if( $javo_query->get('free', null) != null ){
		?>
		<div class="row">
			<div class="col-md-12">
				<?php
				if( (int)$javo_query->get('post_id', 0) > 0 ){
					$javo_this_post = get_post( (int)$javo_query->get('post_id') );
					?>
					
					<h4><?php _e('Selected Item', 'javo_fr');?></h4>
					<div class="well">
						<div class="row">
							<div class="col-md-4">
								<?php _e('Item Name', 'javo_fr');?>
							</div>
							<div class="col-md-8">
								<?php _e('Free', 'javo_fr');?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<?php _e('Title', 'javo_fr');?>
							</div>
							<div class="col-md-8">
								<?php echo $javo_this_post->post_title;?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<?php _e('Author', 'javo_fr');?>
							</div>
							<div class="col-md-8">
								<?php echo get_userdata( $javo_this_post->post_author)->display_name;?>
							</div>
						</div>
					</div><!-- /.well -->
					<?php if( (int)$javo_tso->get('page_item_active') > 0){ ?>
						<div class="row">
							<div class="col-md-12">
								<form method="post" action="<?php echo get_permalink($javo_tso->get('page_item_active'));?>">
									<input type="hidden" name="post_id" value="<?php echo $javo_query->get('post_id');?>">
									<input type="hidden" name="amount" value="0">
									<input type="hidden" name="payment_status" value="free">
									<input class="btn btn-default btn-block" type="submit" value="<?php _e('Continue', 'javo_fr');?>">
								</form>
							</div><!-- /.col-md-12 -->
						</div><!-- /.row -->
					<?php }else{ ?>
						<div class="alert alert-warning text-left">
							<strong><?php _e('Warning', 'javo_fr');?></strong>
							<p><?php _e('Please page connection setting. Admin > Theme settings > item page > Payment Success Page', 'javo_fr');?></p>
						</div>
					<?php
					}
				}else{
					?>
					<h2 class="page-header"><?php _e('Payment Method', 'javo_fr');?></h2>
					<div class="row">
						<div class="col-md-12 text-center">
							<a href="<?php echo home_url(JAVO_ADDITEM_SLUG.'/'. wp_get_current_user()->user_login);?>" class="btn btn-default">
								<?php _e('Add a Free Item', 'javo_fr');?>
							</a>
						</div><!-- /.col-md-12 -->
					</div><!-- /.row -->
					<?php
				} ?>
			</div><!-- /.col-md-12 -->
		</div><!-- /.row -->




		<?php


		/*

		
			/*
			$post_id = wp_update_post(Array(
				'ID'			=> $javo_query->get('post_id')
				, 'post_status'	=> 'publish'				
			));
			update_post_meta( $post_id, 'post_pay_status', 'free');?>
			<div class="row">
				<div class="col-md-12">
					<?php _e('ok', 'javo_fr');?>
					<a class="btn btn-default btn-sm" href="<?php echo get_permalink($javo_query->get('post_id'));?>">
						<?php _e('Done', 'javo_fr');?>
					</a>
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
			<?php
			
		}else{
			?>
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-header"><?php _e('Bed Post ID' ,'javo_fr');?></h3>
					<a class="btn btn-default btn-sm" href="<?php echo home_url('my-item-list/'.wp_get_current_user()->user_login);?>">
						<?php _e('Done', 'javo_fr');?>
					</a>
					<a class="btn btn-default btn-sm" href="<?php echo home_url('my-item-list/'.wp_get_current_user()->user_login);?>">
						<?php _e('Done', 'javo_fr');?>
					</a>
				</div><!-- /.col-md-12 -->
			</div><!-- row -->
			<?php
			* /
		}*/
	}else{
		if( (int)$javo_tso->get('page_item_active') > 0){ ?>
			<div class="row javo-add-payment">
				<div class="col-md-6">
					<h2 class="page-header"><?php _e('Payment Method', 'javo_fr');?></h2>
					<h4><?php _e('Selected Item', 'javo_fr');?></h4>
					<div class="well">
						<div class="row">
							<div class="col-md-4">
								<?php _e('Item Name', 'javo_fr');?>
							</div>
							<div class="col-md-8">
								<?php echo $javo_tso->get('payment_item'.$item_id.'_name');?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<?php _e('Item Price', 'javo_fr');?>
							</div>
							<div class="col-md-8">
								<?php echo number_format((int)$javo_tso->get("payment_item".$item_id."_price", 0));?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<?php _e('Item Detail', 'javo_fr');?>

							</div>
							<div class="col-md-8">
								<?php
									printf('%s %s / %s %s'
										, $javo_tso->get('payment_item'.$item_id.'_posts', 0)
										, __('Posts', 'javo_fr')
										, $javo_tso->get('payment_item'.$item_id.'_days', 0)
										, __('Days', 'javo_fr')
								);?>
							</div>
						</div>
					</div><!-- /.well -->
					<h3><?php _e('Please select payment method', 'javo_fr');?></h3>
				</div><!-- 6 Columns Close ( Detail part ) -->
				<div class="col-md-6 text-center">
					<?php
					$javo_payment_active = 0;
					// Paypal exists
					if($javo_tso->get('paypal_enable', '') == 'use'){
						if($javo_tso->get('paypal_email') != null){
							$javo_payment_active++;
							$paypal_page = get_permalink($javo_tso->get('page_item_active'));
							$paypal_page_part = Array(
								"cancel_return"=> $paypal_page."?cancel",
								"notify_url"=> $paypal_page."?notify",
								"return"=> $paypal_page,
							);?>

							<form method="post" action="<?php echo $javo_tso->get('paypal_mode') == 'normal'? 'https://www.paypal.com/cgi-bin/webscr':'https://www.sandbox.paypal.com/cgi-bin/webscr';?>">
								<input type="hidden" name="custom" value="<?php echo $_POST['param'];?>">
								<input type="hidden" name="notify_url" value="<?php echo $paypal_page_part['notify_url'];?>">
								<input type="hidden" name="cancel_return" value="<?php echo $paypal_page_part['cancel_return'];?>">
								<input type="hidden" name="return" value="<?php echo $paypal_page_part['return'];?>">
								<input type="hidden" name="item_name" value="<?php echo $javo_tso->get('payment_item'.$item_id.'_name');?>">
								<input type="hidden" name="business" value="<?php echo $javo_tso->get('paypal_email');?>">
								<input type="hidden" name="rm" value="2">
								<input type="hidden" name="cmd" value="_xclick">
								<input type="hidden" name="amount" value="<?php echo (int)$javo_tso->get("payment_item".$item_id."_price", 0);?>">
								<input type="hidden" name="currency_code" id="currency_code" value="<?php echo strtoupper($javo_tso->get('paypal_produce_price_prefix', 'USD'));?>">
								<input class="btn btn-danger btn-lg" type="submit" value="<?php _e('Use Paypal System', 'javo_fr');?>">
							</form>

							<?php
						}else{
							printf('<div class="alert alert-info"><strong>%s</strong> %s</div>'
								, __('ALERT', 'javo_fr')
								, __('Please insert administrator paypal email. Admin > Theme settings > Payment > Company(work place) Email','javo_fr')
							);

						};
					};

					// Direct bank exists
					if($javo_tso->get('bank_enable', '') == 'use'){
						$javo_payment_active++;?>
						<form method="post" action="<?php echo get_permalink($javo_tso->get('page_item_active'));?>">
							<input type="hidden" name="item_name" value="<?php echo $javo_tso->get('payment_item'.$item_id.'_name');?>">
							<input type="hidden" name="amount" value="<?php echo (int)$javo_tso->get("payment_item".$item_id."_price", 0);?>">
							<input type="hidden" name="currency" value="<?php echo strtoupper($javo_tso->get('paypal_produce_price_prefix', 'USD'));?>">
							<input type="hidden" name="payment_status" value="bank">
							<input type="hidden" name="custom" value="<?php echo $_POST['param'];?>">
							<input class="btn btn-warning btn-lg" type="submit" value="<?php _e('Direct Bank transfer', 'javo_fr');?>">
						</form>
						<?php
					};

					// Payment system all off then
					if( $javo_payment_active == 0){?>
						<div class="alert alert-warning text-left">
							<strong><?php _e('Warning', 'javo_fr');?></strong>
							<p>
								<?php _e('No payment method has been selected. Please contact administrator.', 'javo_fr');?></p>
						</div>
						<?php
					};?>
				</div>
			</div><!-- Row Close -->
		<?php }else{ ?>
			<div class="alert alert-warning text-left">
				<strong><?php _e('Warning', 'javo_fr');?></strong>
				<p><?php _e('Please page connection setting. Admin > Theme settings > item page > Payment Success Page', 'javo_fr');?></p>
			</div>
		<?php
		}; // Endif
	}; // End if ?>
</div><!-- Container -->