<?php
$javo_meta_query = new javo_GET_META(get_the_ID());
$javo_item_location = wp_get_post_terms(get_the_ID(), 'item_location', array('order' => 'DESC', 'orderby' => 'parent'));

get_header();

?>

<div class="container">
    <div style="height: 20px;width:100%"></div>
    <div class="col-md-12" style="padding: 0px;">
        <div class="panel">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo get_the_ID(); ?>">
                <div class="panel-heading-free-post">
                    <h4 class="panel-title">
                        <div class="row">
                            <div class="col-md-10 col-sm-10">
                                <span style="font-weight: 300;"><?php the_title(); ?></span> -
                                <span style="font-weight: 300;"class="cidade-estado"><?php echo $javo_item_location[0]->name . " - " . get_sigla_estado($javo_item_location[1]->name); ?></span>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <div class="pull-right icon-plus">
                                </div>
                            </div>
                        </div>
                    </h4>
                </div>
                <div id="collapse<?php echo get_the_ID(); ?>" class="panel-collapse collapse">
                    <div class="panel-body panel-free-post">
                        <div class="col-md-12" style="padding: 0px;">
                            <div class="row">
                                <address>
                                    <div class="resultado" style="width: 30%;">
                                        <?php if ($javo_meta_query->get('address') != '') { ?>
                                        <div style="margin-top:5px;">
                                            <?php echo $javo_meta_query->get('address'); ?>
                                        </div>
                                        <?php } ?>
                                        <?php if ($javo_meta_query->get('complement') != '') { ?>
                                        <div style="margin-top:5px;">
                                            <?php echo $javo_meta_query->get('complement'); ?>
                                        </div>
                                        <?php } ?>
                                        <?php if ($javo_meta_query->get('district') != '' && $javo_meta_query->get('district') != ' ') { ?>
                                        <div style="margin-top:5px;">
                                            <?php echo $javo_meta_query->get('district'); ?>
                                        </div>
                                        <?php } ?>
                                    </div>

                                    <div class="resultado" style="width: 25%;">
                                        <?php if ($javo_meta_query->cat('item_location') != '') { ?>
                                        <div style="margin-top:5px;">
                                            <?php echo $javo_item_location[0]->name . " / " . get_sigla_estado($javo_item_location[1]->name); ?>
                                        </div>
                                        <?php } ?>
                                        <?php if ($javo_meta_query->get('cep') != '') { ?>
                                        <div style="margin-top:5px;">
                                            <span style="font-weight: 300;"><?php _e('CEP: ', 'javo_fr'); ?></span>
                                            <?php echo $javo_meta_query->get('cep'); ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="resultado" style="text-align: left;width: 30%;word-wrap: break-word;">
                                        <div>
                                            <?php if ($javo_meta_query->get('website') != '') { ?>
                                            <div style="margin-top:5px;">
                                                <span style="font-weight: 300;">
                                                    <a href="http://<?php echo $javo_meta_query->get('website'); ?>" target="_blank" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"><?php echo $javo_meta_query->get('website'); ?></a>
                                                </span>
                                            </div>
                                            <?php } ?>
                                            <?php if ($javo_meta_query->get('email') != '') { ?>
                                            <div style="margin-top:5px;">
                                                <span style="font-weight: 300;">
                                                    <?php echo $javo_meta_query->get('email'); ?>
                                                </span>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="resultado" style="text-align: right;padding-right: 5px;width: 15%;">
                                        <div style="float: right;padding-left: 5px;">
                                            <?php if ($javo_meta_query->get('phone1') != '' && $javo_meta_query->get('phone1') != ', ') { ?><div style="margin-top:5px;"><span class="quebratelefone"><?php echo $javo_meta_query->get('phone1');?></span></div><?php } ?>
                                            <?php if ($javo_meta_query->get('phone2') != '' && $javo_meta_query->get('phone2') != ', ') { ?><div style="margin-top:5px;"><span class="quebratelefone"><?php echo $javo_meta_query->get('phone2');?></span></div><?php } ?>
                                            <?php if ($javo_meta_query->get('phone3') != '' && $javo_meta_query->get('phone3') != ', ') { ?><div style="margin-top:5px;"><span class="quebratelefone"><?php echo $javo_meta_query->get('phone3');?></span></div><?php } ?>
                                        </div>
                                        <div style="float: right;">
                                            <?php if ($javo_meta_query->get('phone1') != '' && $javo_meta_query->get('phone1') != ', ') { ?><div style="margin-top:5px;"><span class="quebraddd"><?php echo ('(');echo $javo_meta_query->get('ddd1');echo (')'); ?></span></div><?php } ?>
                                            <?php if ($javo_meta_query->get('phone2') != '' && $javo_meta_query->get('phone2') != ', ') { ?><div style="margin-top:5px;"><span class="quebraddd"><?php echo ('(');echo $javo_meta_query->get('ddd2');echo (')'); ?></span></div><?php } ?>
                                            <?php if ($javo_meta_query->get('phone3') != '' && $javo_meta_query->get('phone3') != ', ') { ?><div style="margin-top:5px;"><span class="quebraddd"><?php echo ('(');echo $javo_meta_query->get('ddd3');echo (')'); ?></span></div><?php } ?>
                                        </div>
                                        <div style="float: right;width: 100%;" class="quebratelefone">
                                            <?php if ($javo_meta_query->get('phone0800') != '' && $javo_meta_query->get('phone0800') != ', ') { ?><div style="margin-top:5px;"><span class="quebratelefone"><?php echo $javo_meta_query->get('phone0800');?></span></div><?php } ?>
                                        </div>
                                    </div>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div style="height: 20px;width:100%"></div>
</div>

<?php
get_footer();
