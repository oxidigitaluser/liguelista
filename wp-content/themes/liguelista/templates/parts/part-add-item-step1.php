﻿<?php
global
$javo_tso
, $edit
, $javo_tso_map;
if (isset($_GET["edit"])) {
    $user_id = get_current_user_id();
    $edit = get_post($_GET["edit"]);
    $javo_meta = new javo_get_meta($edit->ID);
    if (
            ($user_id != $edit->post_author) &&
            (!current_user_can("manage_options"))
    ) {
        printf("<script>alert('%s');location.href='%s';</script>", __("Access Rejected", "javo_fr"), get_site_url());
    };
    $detail_images = @unserialize(get_post_meta($edit->ID, "detail_images", true));
    $related_images = @unserialize(get_post_meta($edit->ID, "related_images", true));
    $logotipo_images = @unserialize(get_post_meta($edit->ID, "logotipo_images", true));
    $banner_images = @unserialize(get_post_meta($post->ID, "banner_images", true));
};
$latlng = !empty($edit) ? @unserialize(get_post_meta($edit->ID, "latlng", true)) : Array();
$latlng = new javo_ARRAY($latlng);
$plano_comercial = get_post_meta($edit->ID, 'plano_comercial', true);
?>
<script type="text/javascript">
    (function () {
        "use strict";
        window.transmission = false;
        jQuery("form").submit(function () {
            window.transmission = true;
        });
        window.onbeforeunload = function () {
            if (!window.transmission)
                return "";
        };
    })();
    (function ($) {
        "use strict";
        $("body").on("keyup", ".only_number", function () {
            this.value = this.value.replace(/[^0-9]/g, '');
        });
    })(jQuery);
</script>

<div class="row">
    <div class="col-md-12">
        <form role="form" class="form-horizontal" method="post" id="frm_item">
            <div class="row">
                <div class="col-md-8 col-sm-12 form-left">
                    <div class="line-title-bigdots">
                        <h2><span><?php _e("Title", "javo_fr"); ?></span></h2>
                    </div>
                    <div class="form-group">
                        <div  class="col-md-12">
                            <input name="txt_title" type="text" class="form-control" value="<?php echo isset($edit) ? $edit->post_title : NULL ?>">
                        </div> <!-- col-md-12 -->
                    </div>
                    <div class="line-title-bigdots">
                        <h2><span><?php _e("Description", "javo_fr"); ?></span></h2>
                    </div>
                    <div class="form-group">
                        <div  class="col-md-12">
                            <textarea name="txt_content" data-provide="markdown" rows="10"><?php echo!empty($edit) ? $edit->post_content : ''; ?></textarea>
                        </div> <!-- col-md-12 -->
                    </div>

                    <div class="line-title-bigdots">
                        <h2><span><?php _e("Features", "javo_fr"); ?></span></h2>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <select name="sel_category[]" class="form-control">
                                <option value=""><?php _e("Category", "javo_fr"); ?></option>
                                <?php
                                $terms = get_terms("item_category", Array("hide_empty" => 0));
                                $cats = isset($edit) ? wp_get_post_terms($edit->ID, "item_category") : NULL;
                                echo apply_filters('javo_get_selbox_child_term_lists', 'item_category', null, 'select', (!empty($cats[0]) ? $cats[0]->term_id : 0));
                                ?>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <select name="sel_location[]" class="form-control">
                                <option value=""><?php _e("Location", "javo_fr"); ?></option>
                                <?php
                                $terms = get_terms("item_location", Array("hide_empty" => 0));
                                $cats = isset($edit) ? wp_get_post_terms($edit->ID, "item_location") : NULL;
                                echo apply_filters('javo_get_selbox_child_term_lists', 'item_location', null, 'select', (!empty($cats[0]) ? $cats[0]->term_id : 0));
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Código", "javo_fr"); ?></span>
                                <label name="javo_meta[codigo]" class="form-control"> <?php echo $edit->ID; ?> </label>
                            </div> <!-- input-group -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Contrato", "javo_fr"); ?></span>
                                <input name="javo_meta[contract]" type="text" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('contract') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("PF / PJ", "javo_fr"); ?></span>
                                <select name="sel_pfpj[pfpj]" class="form-control">
                                    <option value="pf" <?php echo isset($edit) ? selected($javo_meta->get('pfpj') == 'pf') : ''; ?> > <?php _e("Pessoa Física", "javo_fr"); ?></option>
                                    <option value="pj" <?php echo isset($edit) ? selected($javo_meta->get('pfpj') == 'pj') : ''; ?> ><?php _e("Pessoa Jurídica", "javo_fr"); ?></option>
                                </select>
                            </div> <!-- input-group -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("CPF / CNPJ", "javo_fr"); ?></span>
                                <input name="javo_meta[cpfcnpj]" type="text" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('cpfcnpj') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Plano Comercial", "javo_fr"); ?></span>
                                <select name="sel_plano_comercial[plano_comercial]" class="form-control">
                                    <option value=""><?php _e("Selecione", "javo_fr"); ?></option>
                                    <option value="MINISITE" <?php echo isset($edit) ? selected($plano_comercial == '0') : ''; ?>><?php _e("MINI SITE", "javo_fr"); ?></option>
                                    <option value="MDVIT" <?php echo isset($edit) ? selected($plano_comercial == '1') : ''; ?>><?php _e("MDVIT - Módulo Vitrine", "javo_fr"); ?></option>
                                    <option value="MDL" <?php echo isset($edit) ? selected($plano_comercial == '2') : ''; ?>><?php _e("MDL - Módulo com Logomarca", "javo_fr"); ?></option>
                                    <option value="MDN" <?php echo isset($edit) ? selected($plano_comercial == '3') : ''; ?>><?php _e("MDN - Módulo Negrito", "javo_fr"); ?></option>
                                    <option value="GRATUITO" <?php echo isset($edit) ? selected($plano_comercial == '4') : ''; ?>><?php _e("GRATUITO", "javo_fr"); ?></option>
                                </select>
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Endereço", "javo_fr"); ?></span>
                                <input name="javo_meta[address]" type="text" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('address') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Complemento", "javo_fr"); ?></span>
                                <input name="javo_meta[complement]" type="text" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('complement') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Bairro", "javo_fr"); ?></span>
                                <input name="javo_meta[district]" type="text" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('district') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("CEP", "javo_fr"); ?></span>
                                <input name="javo_meta[cep]" type="text" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('cep') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Telefone 1", "javo_fr"); ?></span>
                                <input name="javo_meta[ddd1]" type="text" class="form-control" style="width: 25%" value="<?php echo isset($edit) ? $javo_meta->get('ddd1') : null; ?>">
                                <input name="javo_meta[phone1]" type="text" class="form-control" style="width: 75%" value="<?php echo isset($edit) ? $javo_meta->get('phone1') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Telefone 2", "javo_fr"); ?></span>
                                <input name="javo_meta[ddd2]" type="text" class="form-control" style="width: 25%" value="<?php echo isset($edit) ? $javo_meta->get('ddd2') : null; ?>">
                                <input name="javo_meta[phone2]" type="text" class="form-control" style="width: 75%" value="<?php echo isset($edit) ? $javo_meta->get('phone2') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Telefone 3", "javo_fr"); ?></span>
                                <input name="javo_meta[ddd3]" type="text" class="form-control" style="width: 25%" value="<?php echo isset($edit) ? $javo_meta->get('ddd3') : null; ?>">
                                <input name="javo_meta[phone3]" type="text" class="form-control" style="width: 75%" value="<?php echo isset($edit) ? $javo_meta->get('phone3') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Telefone 0800", "javo_fr"); ?></span>
                                <input name="javo_meta[phone0800]" type="text" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('phone0800') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Email", "javo_fr"); ?></span>
                                <input name="javo_meta[email]" type="text" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('email') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Email", "javo_fr"); ?></span>
                                <input name="javo_meta[email]" type="text" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('email') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Website", "javo_fr"); ?></span>
                                <input name="javo_meta[website]" type="text" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('website') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="line-title-bigdots">
                        <h2><span><?php _e("Forma de Pagamento", "javo_fr"); ?></span></h2>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <strong><?php _e('Cartões de Crédito', 'javo_fr'); ?></strong>
                                <ul class="list-unstyled">
                                    <li>
                                        <input name="javo_meta[visa]" id="visa" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('visa') == 'y') : ''; ?>>
                                        <label for="visa"> <?php _e('Visa', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[master]" id="master" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('master') == 'y') : ''; ?>>
                                        <label for="master"><?php _e('MarterCard', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[amex]" id="amex" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('amex') == 'y') : ''; ?>>
                                        <label for="amex"><?php _e('American Express', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[dinclub]" id="dinclub" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('dinclub') == 'y') : ''; ?>>
                                        <label for="dinclub"><?php _e('Diners Club', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[hipercard]" id="hipercard" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('hipercard') == 'y') : ''; ?>>
                                        <label for="hipercard"><?php _e('Hipercard', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[elo]" id="elo" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('elo') == 'y') : ''; ?>>
                                        <label for="elo"><?php _e('Elo', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[aura]" id="aura" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('aura') == 'y') : ''; ?>>
                                        <label for="aura"><?php _e('Aura', 'javo_fr'); ?></label>
                                    </li>
                                </ul>
                            </div>
                        </div>  
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <strong><?php _e('Cartões de Débito', 'javo_fr'); ?></strong>
                                <ul class="list-unstyled">
                                    <li>
                                        <input name="javo_meta[visael]" id="visael" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('visael') == 'y') : ''; ?>>
                                        <label for="visael"> <?php _e('Visa Electron', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[maestro]" id="maestro" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('maestro') == 'y') : ''; ?>>
                                        <label for="maestro"><?php _e('Maestro', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[itau]" id="itau" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('itau') == 'y') : ''; ?>>
                                        <label for="itau"><?php _e('Itaú', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[bradesco]" id="bradesco" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('bradesco') == 'y') : ''; ?>>
                                        <label for="bradesco"><?php _e('Bradesco', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[bb]" id="bb" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('bb') == 'y') : ''; ?>>
                                        <label for="bb"><?php _e('Banco do Brasil', 'javo_fr'); ?></label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <strong><?php _e('Vale Refeição', 'javo_fr'); ?></strong>
                                <ul class="list-unstyled">
                                    <li>
                                        <input name="javo_meta[ticket]" id="ticket" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('ticket') == 'y') : ''; ?>>
                                        <label for="ticket"><?php _e('Ticket Restaurante', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[vale]" id="vale" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('vale') == 'y') : ''; ?>>
                                        <label for="vale"><?php _e('Vale Refeição', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[sodexo]" id="sodexo" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('sodexo') == 'y') : ''; ?>>
                                        <label for="sodexo"><?php _e('Sodexo', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[vvalelo]" id="vvalelo" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('vvalelo') == 'y') : ''; ?>>
                                        <label for="vvalelo"><?php _e('Visa Vale - Alelo', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[vcard]" id="vcard" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('vcard') == 'y') : ''; ?>>
                                        <label for="vcard"><?php _e('Vale Card', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[nutricash]" id="nutricash" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('nutricash') == 'y') : ''; ?>>
                                        <label for="nutricash"><?php _e('Nutricash', 'javo_fr'); ?></label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <strong><?php _e('Outros', 'javo_fr'); ?></strong>
                                <ul class="list-unstyled">
                                    <li>
                                        <input name="javo_meta[din]" id="din" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('din') == 'y') : ''; ?>>
                                        <label for="din"><?php _e('Dinheiro', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[bol]" id="bol" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('bol') == 'y') : ''; ?>>
                                        <label for="bol"><?php _e('Boleto Bancário', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[dep]" id="dep" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('dep') == 'y') : ''; ?>>
                                        <label for="dep"><?php _e('Depósito / Transferência', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[pagseg]" id="pagseg" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('pagseg') == 'y') : ''; ?>>
                                        <label for="pagseg"><?php _e('PagSeguro', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[pp]" id="pp" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('pp') == 'y') : ''; ?>>
                                        <label for="pp"><?php _e('PayPal', 'javo_fr'); ?></label>
                                    </li>
                                    <li>
                                        <input name="javo_meta[moip]" id="moip" value="y" type="checkbox"<?php echo isset($edit) ? checked($javo_meta->get('moip') == 'y') : ''; ?>>
                                        <label for="moip"><?php _e('MoIP', 'javo_fr'); ?></label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> <!-- input-group -->
                    <div class="line-title-bigdots">
                        <h2><span><?php _e("Redes Sociais", "javo_fr"); ?></span></h2>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Facebook", "javo_fr"); ?></span>
                                <input name="javo_meta[face]" id="facebook" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('face') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Twitter", "javo_fr"); ?></span>
                                <input name="javo_meta[twitter]" id="twitter" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('twitter') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Instagram", "javo_fr"); ?></span>
                                <input name="javo_meta[insta]" id="insta" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('insta') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Google Plus", "javo_fr"); ?></span>
                                <input name="javo_meta[plus]" id="plus" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('plus') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Youtube", "javo_fr"); ?></span>
                                <input name="javo_meta[youtube]" id="youtube" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('youtube') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Vimeo", "javo_fr"); ?></span>
                                <input name="javo_meta[vimeo]" id="vimeo" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('vimeo') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Pinterest", "javo_fr"); ?></span>
                                <input name="javo_meta[pint]" id="pinterest" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('pint') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("LinkedIn", "javo_fr"); ?></span>
                                <input name="javo_meta[lnkdin]" id="linkedin" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('lnkdin') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Foursquare", "javo_fr"); ?></span>
                                <input name="javo_meta[4sqr]" id="4sqr" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('4sqr') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Tumblr", "javo_fr"); ?></span>
                                <input name="javo_meta[tumblr]" id="tumblr" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('tumblr') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Flickr", "javo_fr"); ?></span>
                                <input name="javo_meta[fckr]" id="fckr" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('fckr') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Behance", "javo_fr"); ?></span>
                                <input name="javo_meta[behance]" id="behance" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('behance') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="input-group">
                                <span class="input-group-addon"><?php _e("Github", "javo_fr"); ?></span>
                                <input name="javo_meta[github]" id="github" class="form-control" value="<?php echo isset($edit) ? $javo_meta->get('github') : null; ?>">
                            </div> <!-- input-group -->
                        </div>
                    </div>
                    <div class="line-title-bigdots">
                        <h2><span><?php echo $javo_tso->get('field_caption', __('Aditional Information', 'javo_fr')); ?></span></h2>
                    </div><!-- /.line-title-bigdots -->

                    <div class="form-group">
                        <div class="col-md-12">
                            <?php
                            global $javo_custom_field;
                            echo $javo_custom_field->form();
                            ?>
                        </div>
                    </div><!-- /.form-group -->

                    <div class="line-title-bigdots">
                        <h2><span><?php _e("Featured Image", "javo_fr"); ?></span></h2>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <a class="btn btn-primary btn-sm javo-fileupload" data-title="<?php _e('Upload Featured Image', 'javo_fr'); ?>" data-input="input[name='javo_featured_url']" data-preview=".javo-this-item-featured"><?php _e("Upload Detail Images", "javo_fr"); ?></a>
                                </div><!-- col-md-12 -->
                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    $javo_this_item_featued = NULL;
                                    if (!empty($edit)) {
                                        $javo_this_item_featued_meta = wp_get_attachment_image_src(get_post_thumbnail_id($edit->ID), 'javo-box');
                                        $javo_this_item_featued = ' src="' . $javo_this_item_featued_meta[0] . '"';
                                    };
                                    ?>
                                    <img<?php echo $javo_this_item_featued; ?> class="javo-this-item-featured img-responsive">
                                    <input name="javo_featured_url" type="hidden" value="<?php echo isset($edit) ? get_post_thumbnail_id($edit->ID) : NULL; ?>">
                                </div>
                            </div>
                        </div><!-- col-md-12 -->
                    </div><!-- Form Group -->

                    <div class="line-title-bigdots">
                        <h2><span><?php _e("Logotipo", "javo_fr"); ?></span></h2>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="row">	
                                <div class="col-md-12">
                                    <a class="btn btn-primary btn-sm javo-fileupload" data-multiple="true" data-title="<?php _e('Upload Logotipo', 'javo_fr'); ?>" data-preview=".javo_dim_field"><?php _e("Upload Logotipo", "javo_fr"); ?></a>
                                    <div class='javo_dim_field row'>
                                        <!-- Images -->
                                        <?php
                                        if (!empty($logotipo_images)) {
                                            foreach ($logotipo_images as $index => $src) {
                                                $url = wp_get_attachment_image_src($src, "thumbnail");
                                                echo "<div class='col-md-4 javo_dim_div'>";
                                                printf("<div class='row'>
                                                            <div class='col-md-12 javo-dashboard-upload-list'>
                                                                <img src='%s'>
                                                            </div>
							</div>
							<div class='row'>
                                                            <div class='col-md-12' align='center'>
                                                                <input type='hidden' name='javo_dim_logotipo[]' value='%s'>
								<input type='button' value='%s' class='btn btn-danger btn-xs javo_logotipo_image_del'>
                                                            </div>
							</div>", $url[0], $src, __("Delete", "javo_fr"));
                                                echo "</div>";
                                            };
                                        };
                                        ?>
                                    </div>
                                </div><!-- 12 columns -->
                            </div><!-- Row -->
                        </div>
                    </div> <!-- form-group -->

                    <div class="line-title-bigdots">
                        <h2><span><?php _e("Banner", "javo_fr"); ?></span></h2>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="row">	
                                <div class="col-md-12">
                                    <a class="btn btn-primary btn-sm javo-fileupload" data-multiple="true" data-title="<?php _e('Upload Banner', 'javo_fr'); ?>" data-preview=".javo_dim_field"><?php _e("Upload Banner", "javo_fr"); ?></a>
                                    <div class='javo_dim_field row'>
                                        <!-- Images -->
                                        <?php
                                        if (!empty($banner_images)) {
                                            foreach ($banner_images as $index => $src) {
                                                $url = wp_get_attachment_image_src($src, "thumbnail");
                                                echo "<div class='col-md-4 javo_dim_div'>";
                                                printf("<div class='row'>
                                                            <div class='col-md-12 javo-dashboard-upload-list'>
                                                                <img src='%s'>
                                                            </div>
							</div>
							<div class='row'>
                                                            <div class='col-md-12' align='center'>
                                                                <input type='hidden' name='javo_dim_banner[]' value='%s'>
								<input type='button' value='%s' class='btn btn-danger btn-xs javo_banner_image_del'>
                                                            </div>
							</div>", $url[0], $src, __("Delete", "javo_fr"));
                                                echo "</div>";
                                            };
                                        };
                                        ?>
                                    </div>
                                </div><!-- 12 columns -->
                            </div><!-- Row -->
                        </div>
                    </div> <!-- form-group -->

                    <div class="line-title-bigdots">
                        <h2><span><?php _e("Detail Images", "javo_fr"); ?></span></h2>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="row">	
                                <div class="col-md-12">
                                    <a class="btn btn-primary btn-sm javo-fileupload" data-multiple="true" data-title="<?php _e('Upload Detail Images', 'javo_fr'); ?>" data-preview=".javo_dim_field"><?php _e("Upload Detail Images", "javo_fr"); ?></a>
                                    <div class='javo_dim_field row'>
                                        <!-- Images -->
                                        <?php
                                        if (!empty($detail_images)) {
                                            foreach ($detail_images as $index => $src) {
                                                $url = wp_get_attachment_image_src($src, "thumbnail");
                                                echo "<div class='col-md-4 javo_dim_div'>";
                                                printf("<div class='row'>
                                                            <div class='col-md-12 javo-dashboard-upload-list'>
                                                                <img src='%s'>
                                                            </div>
							</div>
							<div class='row'>
                                                            <div class='col-md-12' align='center'>
                                                                <input type='hidden' name='javo_dim_detail[]' value='%s'>
								<input type='button' value='%s' class='btn btn-danger btn-xs javo_detail_image_del'>
                                                            </div>
							</div>", $url[0], $src, __("Delete", "javo_fr"));
                                                echo "</div>";
                                            };
                                        };
                                        ?>
                                    </div>
                                </div><!-- 12 columns -->
                            </div><!-- Row -->
                        </div>
                    </div> <!-- form-group -->

                    <div class="line-title-bigdots">
                        <h2><span><?php _e("Galeria de Imagens", "javo_fr"); ?></span></h2>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="row">	
                                <div class="col-md-12">
                                    <a class="btn btn-primary btn-sm javo-fileupload" data-multiple="true" data-title="<?php _e('Upload Galeria de Imagens', 'javo_fr'); ?>" data-preview=".javo_dim_field"><?php _e("Upload Galeria de Imagens", "javo_fr"); ?></a>
                                    <div class='javo_dim_field row'>
                                        <!-- Images -->
                                        <?php
                                        if (!empty($related_images)) {
                                            foreach ($related_images as $index => $src) {
                                                $url = wp_get_attachment_image_src($src, "thumbnail");
                                                echo "<div class='col-md-4 javo_dim_div'>";
                                                printf("<div class='row'>
                                                            <div class='col-md-12 javo-dashboard-upload-list'>
                                                                <img src='%s'>
                                                            </div>
							</div>
							<div class='row'>
                                                            <div class='col-md-12' align='center'>
                                                                <input type='hidden' name='javo_dim_related[]' value='%s'>
								<input type='button' value='%s' class='btn btn-danger btn-xs javo_related_image_del'>
                                                            </div>
							</div>", $url[0], $src, __("Delete", "javo_fr"));
                                                echo "</div>";
                                            };
                                        };
                                        ?>
                                    </div>

                                </div><!-- 12 columns -->
                            </div><!-- Row -->
                        </div>
                    </div> <!-- form-group -->

                    <div class="line-title-bigdots">
                        <h2><span><?php _e("Video", "javo_fr"); ?></span></h2>
                    </div>
                    <?php
                    $javo_video_portals = Array('youtube', 'vimeo', 'dailymotion', 'yahoo', 'bliptv', 'veoh', 'viddler');
                    $javo_get_video_meta = !empty($edit) ? get_post_meta($edit->ID, 'video', true) : Array();
                    $javo_video_meta = new javo_ARRAY($javo_get_video_meta);

                    $javo_get_video = Array(
                        "portal" => $javo_video_meta->get('portal', NULL)
                        , "video_id" => $javo_video_meta->get('video_id', NULL)
                    );
                    ?>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <select name="javo_video[portal]" class="form-control">
                                <option value=""><?php _e('None', 'javo_fr'); ?></option>
                                <?php
                                foreach ($javo_video_portals as $portal) {
                                    printf('<option value="%s"%s>%s</option>'
                                            , $portal
                                            , ( $portal == $javo_get_video['portal'] ? ' selected' : '')
                                            , __($portal, 'javo_fr')
                                    );
                                };
                                ?>
                            </select>
                        </div><!-- /.col-md-4 -->
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <input class="form-control" name="javo_video[video_id]" value="<?php echo $javo_get_video['video_id']; ?>">						
                        </div><!-- /.col-md-8 -->
                    </div><!-- /.row -->

                </div>
                <div class="col-md-4 col-sm-12 form-right">
                    <div class="form-group">
                        <div class="line-title-bigdots">
                            <h2><span><?php _e("Location", "javo_fr"); ?></span></h2>
                            <div class="input-group">
                                <input class="form-control javo-add-item-map-search" placeholder=<?php _e("Address", "javo_fr"); ?>>
                                <div class="input-group-btn">
                                    <input type="button" value="Find" class="javo-add-item-map-search-find btn btn-dark">
                                </div>
                            </div>
                            <div class="map_area"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon"><?php _e("Latitude", "javo_fr"); ?></span>
                                        <input type="text" name="javo_location[lat]" class="form-control" value="<?php echo $latlng->get('lat', $javo_tso_map->get('default_lat', 40.7143528)); ?>">
                                    </div> <!-- input-group -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon"><?php _e("Longitude", "javo_fr"); ?></span>
                                        <input type="text" name="javo_location[lng]" class="form-control" value="<?php echo $latlng->get('lng', $javo_tso_map->get('default_lng', -74.0059731)); ?>">
                                    </div> <!-- input-group -->
                                </div>
                            </div>
                        </div>
                    </div>						
                </div> <!-- form-group -->
            </div>
    </div><!-- col-md-4 -->
</div><!-- row(form-group) -->

<div class="form-group text-center">
    <div class="row">
        <div class="col-md-12">
            <h2></h2>
            <?php printf("<a class='btn btn-lg btn-info item_submit'>%s</a>", isset($edit) ? __("Edit item", "javo_fr") : __("Save item", "javo_fr")); ?>
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
</div>
<div class="row">&nbsp;</div>
<input type="hidden" name="add_new_post" value="1">
<input type="hidden" name="edit" value="<?php echo isset($edit) ? $edit->ID : NULL; ?>">
<input type="hidden" name="action" value="add_item">
</form>
<form method="post" id="javo_add_item_step1">
    <input type="hidden" name="act2" value="true">
    <input type="hidden" name="post_id" value="">
</form>
<input type="hidden" name="javo_add_item_disabled" value="<?php echo $javo_tso->get('do_not_add_item', 0); ?>">
<input type="hidden" name="javo_add_item_disabled_comment" value="">
<input type="hidden" name="current_user_item_list" value="<?php echo home_url('/'); ?>">

<?php
$alerts = Array(
    "title_null" => __('Please Type Item Title.', 'javo_fr')
    , "content_null" => __('Please Type Item Description.', 'javo_fr')
    , "latlng_null" => __('Please Find Address Or Marker Drag.', 'javo_fr')
    , "item_edit_success" => __('Item Modified Successfully!', 'javo_fr')
    , "item_new_success" => __('Thanks For Posting Your Evnet!', 'javo_fr')
);
?>
<script type="text/javascript">
    (function ($) {
        "use strict";
        var javo_this_location_map = {
            map: $('.map_area')
            , map_attribute: {
                map: {
                    latLng: new google.maps.LatLng(40.7143528, -74.0059731)
                    , options: {zoom: 8}
                    , events: {
                        click: function (m, l) {
                            $(this)
                                    .gmap3({
                                        get: {
                                            name: "marker"
                                            , callback: function (m) {
                                                m.setPosition(l.latLng);
                                                $('input[name="javo_location[lat]"]').val(m.getPosition().lat());
                                                $('input[name="javo_location[lng]"]').val(m.getPosition().lng());
                                            }
                                        }
                                    });
                        }
                    }
                }
                , marker: {
                    latLng: new google.maps.LatLng(40.7143528, -74.0059731)
                    , options: {draggable: true}
                    , events: {
                        dragend: function (m) {
                            $('input[name="javo_location[lat]"]').val(m.getPosition().lat());
                            $('input[name="javo_location[lng]"]').val(m.getPosition().lng());
                        }
                    }
                }
            }
            , init: function () {
                if (
                        $('input[name="javo_location[lat]"]').val() &&
                        $('input[name="javo_location[lng]"]').val()
                        ) {
                    var thisLat = $('input[name="javo_location[lat]"]').val();
                    var thisLng = $('input[name="javo_location[lng]"]').val();
                    this.map_attribute.map.latLng = new google.maps.LatLng(thisLat, thisLng);
                    this.map_attribute.marker.latLng = new google.maps.LatLng(thisLat, thisLng);
                }
                ;

                this.map
                        .height(this.map.closest('.row').height() / 2)
                        .gmap3(this.map_attribute);

                this.events();

            }
            , events: function () {
                var $object = this;
                $('body')
                        .on('click', '.javo-add-item-map-search-find', function () {
                            $object.map.gmap3({
                                getlatlng: {
                                    address: $('.javo-add-item-map-search').val()
                                    , callback: function (result) {
                                        if (!result)
                                            return;

                                        $(this)
                                                .gmap3({
                                                    get: {
                                                        name: "marker"
                                                        , callback: function (marker) {
                                                            var $map = $(this).gmap3('get');
                                                            marker.setPosition(result[0].geometry.location);
                                                            $map.setCenter(result[0].geometry.location);

                                                            $('input[name="javo_location[lat]"]').val(result[0].geometry.location.lat());
                                                            $('input[name="javo_location[lng]"]').val(result[0].geometry.location.lng());
                                                        }
                                                    }
                                                });
                                    }
                                }
                            });

                        }).on('keyup', '.javo-add-item-map-search', function (e) {
                    if (e.keyCode == 13) {
                        $('.javo-add-item-map-search-find').trigger('click');
                    }
                    ;
                });

            }
        };
        javo_this_location_map.init();

        function chk_null(obj, msg, objType) {
            var objType = objType != null ? objType : "input";
            var obj = $(objType + "[name='" + obj + "']");
            if (obj.val() != "")
                return true;
            obj.addClass("isNull").focus();
            $.javo_msg({content: msg, delay: 10000});
            return false;
        }
        ;
        $("input, textarea").on("keydown", function () {
            $(this).removeClass('isNull');
        });
        $("body").on("click", ".item_submit", function (e) {
            e.preventDefault();

            var options = {};
            options.type = "post";
            options.url = "<?php echo admin_url('admin-ajax.php'); ?>";
            options.data = $("#frm_item").serialize();
            options.dataType = "json";
            options.error = function (e) {
                alert("<?php _e('Server Error : ', 'javo_fr'); ?>" + e.state());
                console.log(e.responseText);
            };
            options.success = function (d) {
                if (d.state == true) {
                    var is_paid = <?php echo $javo_tso->get('item_publish', null) == 'paid' ? 'true' : 'false'; ?>;

                    window.transmission = true;

                    switch (d.status) {
                        case "edit":
                            $.javo_msg({content: "<?php echo $alerts['item_edit_success']; ?>", delay: 10000}, function () {
                                location.href = $('[name="current_user_item_list"]').val();
                            });
                            break;
                        case "new":
                        default:
                            if (is_paid) {
                                $("input[name='post_id']").val(d.post_id);
                                $("form#javo_add_item_step1").submit();
                            } else {
                                $.javo_msg({content: "<?php echo $alerts['item_new_success']; ?>", delay: 10000}, function () {
                                    location.href = d.permalink;
                                });
                            }
                    }
                    return false;
                }
                $(this).button('reset');
            };

            if (chk_null('txt_title', "<?php echo $alerts['title_null']; ?>") == false)
                return false;
            //if( chk_null( 'txt_content', "<?php echo $alerts['content_null']; ?>", "textarea") == false ) return false;

            if ($("#javo-item-gpsLatitude, #javo-item-gpsLongitude").val() == "") {
                $("#javo-item-address").addClass("isNull").focus();
                $.javo_msg({content: "<?php echo $alerts['latlng_null']; ?>", delay: 10000});
                return false;
            }
            ;
            $(this).button('loading');
            $.ajax(options);
        });
    })(jQuery);
</script>
</div><!-- container -->
</div><!-- row -->