<?php
$javo_directory_query = new javo_get_meta(get_the_ID());
$javo_rating = new javo_Rating(get_the_ID());
global $javo_custom_field
, $post
, $javo_tso
, $javo_video_query;
$javo_this_author = get_userdata($post->post_author);
$javo_this_author_avatar_id = get_the_author_meta('avatar');
$javo_directory_query = new javo_get_meta(get_the_ID());
$javo_rating = new javo_Rating(get_the_ID());
$javo_this_item_tab_slide_type = 'type2';
$javo_item_location = wp_get_post_terms(get_the_ID(), 'item_location', array('order' => 'DESC', 'orderby' => 'parent'));
?>



<!-- slide -->
<div class="row">
    <div class="col-md-12">
        <?php get_template_part('templates/parts/part', 'single-detail-tab-sliders'); ?>
    </div> <!-- col-md-12 -->
</div> <!-- row -->

<!-- slide end -->
<?php if ($javo_tso->get('claim_use') == 'use') { ?>
    <div class="claim_btn_wrap clearfix">
        <a href="#" data-toggle="modal" data-target="#jv-claim-reveal" class="btn btn-primary javo-tooltip pull-right" title="" data-original-title="Claim This Business"><i class="glyphicon glyphicon-briefcase"></i>&nbsp;<?php _e('Own This Business?', 'javo_fr'); ?></a>
    </div> <!-- claim_btn_wrap -->
<?php } ?>
<div class="row">
    <div class="col-md-12">
        <div class="item-single-details-box">
            <h4 class="detail-titles"><?php _e('Description', 'javo_fr'); ?></h4>
            <div class="javo-left-overlay">
                <div class="javo-txt-meta-area admin-color-setting"><?php _e('Description', 'javo_fr'); ?></div> <!-- javo-txt-meta-area -->
                <div class="corner-wrap">
                    <div class="corner admin-color-setting"></div>
                    <div class="corner-background admin-color-setting"></div>
                </div> <!-- corner-wrap -->
            </div> <!-- javo-left-overlay -->
            <!-- <div class="title-box"><?php _e('Description', 'javo_fr'); ?></div> -->
            <div class="inner-items">
                <?php the_content(); ?>
            </div> <!-- inner-items -->
        </div> <!-- item-single-details-box -->
    </div> <!-- col-md-12 -->
    <?php
    if ($javo_video_query->get('single_position', 'slide') == 'descript') {
        ?>

        <div class="col-md-12"> <!-- video start -->
            <div class="item-single-details-box">
                <h4 class="detail-titles"><?php _e('Video', 'javo_fr'); ?></h4>
                <div class="javo-left-overlay">
                    <div class="javo-txt-meta-area admin-color-setting"><?php _e('Video', 'javo_fr'); ?></div> <!-- javo-txt-meta-area -->
                    <div class="corner-wrap">
                        <div class="corner admin-color-setting"></div>
                        <div class="corner-background admin-color-setting"></div>
                    </div> <!-- corner-wrap -->
                </div> <!-- javo-left-overlay -->
                <!-- <div class="title-box"><?php _e('Description', 'javo_fr'); ?></div> -->
                <div class="inner-items">
                    <?php echo $javo_video_query->get('html'); ?>
                </div> <!-- inner-items -->
            </div> <!-- item-single-details-box -->
        </div> <!-- col-md-12 // video end -->
    <?php }; ?>
    <div class="col-md-12">
        <div class="item-single-details-box">
            <h4 class="detail-titles"><?php _e('Contato', 'javo_fr'); ?></h4>
            <div class="javo-left-overlay">
                <div class="javo-txt-meta-area admin-color-setting"><?php _e('Contato', 'javo_fr'); ?></div> <!-- javo-txt-meta-area -->
                <div class="corner-wrap">
                    <div class="corner admin-color-setting"></div>
                    <div class="corner-background admin-color-setting"></div>
                </div> <!-- corner-wrap -->
            </div> <!-- javo-left-overlay -->
            <div class="inner-items">
                <ul>



                    <?php if ($javo_directory_query->get('phone1') != '' && $javo_directory_query->get('phone1') != ', ') { ?>
                        <li>
                            <span><?php echo __('Telefone:', 'javo_fr') ?> </span>
                            <?php
                            echo ('(');echo $javo_directory_query->get('ddd1');echo (') ');
                            echo $javo_directory_query->get('phone1');
                            ?>
                        </li>
                    <?php } ?>
                    <?php if ($javo_directory_query->get('phone2') != '' && $javo_directory_query->get('phone2') != ', ') { ?>
                        <li>
                            <span><?php echo __('Telefone:', 'javo_fr') ?> </span>
                            <?php
                            echo ('(');echo $javo_directory_query->get('ddd2');echo (') ');
                            echo $javo_directory_query->get('phone2');
                            ?>
                        </li>
                    <?php } ?>
                    <?php if ($javo_directory_query->get('phone3') != '' && $javo_directory_query->get('phone3') != ', ') { ?>
                        <li>
                            <span><?php echo __('Telefone:', 'javo_fr') ?> </span>
                            <?php
                            echo ('(');echo $javo_directory_query->get('ddd3');echo (') ');
                            echo $javo_directory_query->get('phone3');
                            ?>
                        </li>
                    <?php } ?>
                    <?php if ($javo_directory_query->get('phone0800') != '' && $javo_directory_query->get('phone0800') != ', ') { ?>
                        <li>
                            <span><?php echo __('Telefone:', 'javo_fr') ?> </span>
                            <?php echo $javo_directory_query->get('phone0800'); ?>
                        </li>
                    <?php } ?>
                    <?php
                    if ($javo_directory_query->get('email', NULL) != NULL) {
                        printf('<li><span>%s</span><a href="mailto:%s" target="_self">%s</a></li>'
                                , __('Email:', 'javo_fr')
                                , $javo_directory_query->get('email', NULL)
                                , $javo_directory_query->get('email', NULL)
                        );
                    }
                    if ($javo_directory_query->get('website', NULL) != NULL) {
                        printf('<li><span>%s</span><a href="%s" target="_blank">%s</a></li>'
                                , __('Site:', 'javo_fr')
                                , ( strpos($javo_directory_query->get('website'), 'http://') !== false ?
                                        $javo_directory_query->get('website') :
                                        'http://' . $javo_directory_query->get('website')
                                )
                                , $javo_directory_query->get('website', NULL)
                        );
                    }
                    ?>
                    <li><span><?php echo __('Categorias:', 'javo_fr') . '</span> ' . $javo_directory_query->cat('item_category'); ?></li>
                </ul>
            </div>
        </div>
        <div class="item-single-details-box">
            <h4 class="detail-titles"><?php _e('Endereço', 'javo_fr'); ?></h4>
            <div class="javo-left-overlay">
                <div class="javo-txt-meta-area admin-color-setting"><?php _e('Endereço', 'javo_fr'); ?></div> <!-- javo-txt-meta-area -->
                <div class="corner-wrap">
                    <div class="corner admin-color-setting"></div>
                    <div class="corner-background admin-color-setting"></div>
                </div> <!-- corner-wrap -->
            </div> <!-- javo-left-overlay -->


            <div class="inner-items">
                <ul>
                    <?php if ($javo_directory_query->get('address') != '') { ?>
                        <li>
                            <span><?php _e('Logradouro: ', 'javo_fr'); ?></span>
                            <?php echo $javo_directory_query->get('address'); ?>
                        </li>
                    <?php } if ($javo_directory_query->get('complement') != '') { ?>
                        <li>
                            <span><?php _e('Complemento: ', 'javo_fr'); ?></span>
                            <?php echo $javo_directory_query->get('complement'); ?>
                        </li>
                    <?php } if ($javo_directory_query->get('district') != '') { ?>
                        <li>
                            <span><?php _e('Bairro: ', 'javo_fr'); ?> </span>
                            <?php echo $javo_directory_query->get('district'); ?>
                        </li>
                    <?php } if ($javo_directory_query->get('cep') != '') { ?>
                        <li>
                            <span> <?php _e('CEP: ', 'javo_fr'); ?></span>
                            <?php echo $javo_directory_query->get('cep'); ?>
                        </li>
                    <?php } ?>
                    <li>
                        <span><?php echo _e('Cidade', 'javo_fr') ?></span>
                        <?php echo $javo_item_location[0]->name . " - " . get_sigla_estado($javo_item_location[1]->name); ?>
                    </li>
                </ul>
            </div>
        </div>
        <?php
        if ($javo_directory_query->get('visael') == 'y' || $javo_directory_query->get('maestro') == 'y' || $javo_directory_query->get('itau') == 'y' || $javo_directory_query->get('bradesco') == 'y' || $javo_directory_query->get('bb') == 'y' || $javo_directory_query->get('visa') == 'y' || $javo_directory_query->get('master') == 'y' || $javo_directory_query->get('amex') == 'y' || $javo_directory_query->get('dinclub') == 'y' || $javo_directory_query->get('hipercard') == 'y' || $javo_directory_query->get('elo') == 'y' || $javo_directory_query->get('aura') == 'y' || $javo_directory_query->get('ticket') == 'y' || $javo_directory_query->get('vale') == 'y' || $javo_directory_query->get('sodexo') == 'y' || $javo_directory_query->get('vvalelo') == 'y' || $javo_directory_query->get('vcard') == 'y' || $javo_directory_query->get('nutricash') == 'y' || $javo_directory_query->get('din') == 'y' || $javo_directory_query->get('bol') == 'y' || $javo_directory_query->get('dep') == 'y' || $javo_directory_query->get('pagseg') == 'y' || $javo_directory_query->get('pp') == 'y' || $javo_directory_query->get('moip') == 'y') {
            ?>
            <div class="item-single-details-box">
                <h4 class="detail-titles"><?php _e('Formas de Pagamento', 'javo_fr'); ?></h4>
                <div class="javo-left-overlay">
                    <div class="javo-txt-meta-area admin-color-setting"><?php _e('Formas de Pagamento', 'javo_fr'); ?></div> <!-- javo-txt-meta-area -->
                    <div class="corner-wrap">
                        <div class="corner admin-color-setting"></div>
                        <div class="corner-background admin-color-setting"></div>
                    </div> <!-- corner-wrap -->
                </div> <!-- javo-left-overlay -->
                <div class="inner-items">
                    <div class="row">
                        <?php
                        if ($javo_directory_query->get('visa') == 'y' || $javo_directory_query->get('master') == 'y' || $javo_directory_query->get('amex') == 'y' || $javo_directory_query->get('dinclub') == 'y' || $javo_directory_query->get('hipercard') == 'y' || $javo_directory_query->get('elo') == 'y' || $javo_directory_query->get('aura') == 'y') {
                            ?>
                            <div class="col-md-4">
                                <span><?php _e('Cartões de Crédito', 'javo_fr'); ?></span>
                            </div>
                            <div class="col-md-8">
                                <ul class="list-inline cartoes-credito">
                                    <?php if ($javo_directory_query->get('visa') == 'y') { ?>
                                        <li title="Visa" class="visa">
                                        </li>
                                    <?php } if ($javo_directory_query->get('master') == 'y') { ?>
                                        <li title="Mastercard" class="mastercard">
                                        </li>
                                    <?php } if ($javo_directory_query->get('amex') == 'y') { ?>
                                        <li title="American Express" class="american-express">
                                        </li>
                                    <?php } if ($javo_directory_query->get('dinclub') == 'y') { ?>
                                        <li title="Diners Club" class="diners-club">
                                        </li>
                                    <?php } if ($javo_directory_query->get('hipercard') == 'y') { ?>
                                        <li title="Hipercard" class="hipercard">
                                        </li>
                                    <?php } if ($javo_directory_query->get('elo') == 'y') { ?>
                                        <li title="Elo" class="elo">
                                        </li>
                                    <?php } if ($javo_directory_query->get('aura') == 'y') { ?>
                                        <li title="Aura" class="aura">
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>

                        <?php
                        if ($javo_directory_query->get('visael') == 'y' || $javo_directory_query->get('maestro') == 'y' || $javo_directory_query->get('itau') == 'y' || $javo_directory_query->get('bradesco') == 'y' || $javo_directory_query->get('bb') == 'y') {
                            ?>
                            <div class="col-md-4">
                                <span><?php _e('Cartões de Débito', 'javo_fr'); ?></span>
                            </div>
                            <div class="col-md-8">
                                <ul class="list-inline cartoes-debito">
                                    <?php if ($javo_directory_query->get('visael') == 'y') { ?>
                                        <li title="Visa Electron" class="visa-electron">
                                        </li>
                                    <?php } if ($javo_directory_query->get('maestro') == 'y') { ?>
                                        <li title="Maestro" class="maestro">
                                        </li>
                                    <?php } if ($javo_directory_query->get('itau') == 'y') { ?>
                                        <li title="Itaú" class="itau">
                                        </li>
                                    <?php } if ($javo_directory_query->get('bradesco') == 'y') { ?>
                                        <li title="Bradesco" class="bradesco">
                                        </li>
                                    <?php } if ($javo_directory_query->get('bb') == 'y') { ?>
                                        <li title="Banco do Brasil" class="banco-brasil">
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>

                        <?php
                        if ($javo_directory_query->get('ticket') == 'y' || $javo_directory_query->get('vale') == 'y' || $javo_directory_query->get('sodexo') == 'y' || $javo_directory_query->get('vvalelo') == 'y' || $javo_directory_query->get('vcard') == 'y' || $javo_directory_query->get('nutricash') == 'y') {
                            ?>
                            <div class="col-md-4">
                                <span><?php _e('Vale Refeição', 'javo_fr'); ?></span>
                            </div>
                            <div class="col-md-8">
                                <ul class="list-inline vale-refeicao">
                                    <?php if ($javo_directory_query->get('ticket') == 'y') { ?>
                                        <li title="Ticket Restaurante" class="ticket-restaurante">
                                        </li>
                                    <?php } if ($javo_directory_query->get('vale') == 'y') { ?>
                                        <li title="Vale Refeição" class="vale-refeicao">
                                        </li>
                                    <?php } if ($javo_directory_query->get('sodexo') == 'y') { ?>
                                        <li title="Sodexo" class="sodexo">
                                        </li>
                                    <?php } if ($javo_directory_query->get('vvalelo') == 'y') { ?>
                                        <li title="Visa Vale Alelo" class="visa-vale-alelo">
                                        </li>
                                    <?php } if ($javo_directory_query->get('vcard') == 'y') { ?>
                                        <li title="Vale Card" class="vale-card">
                                        </li>
                                    <?php } if ($javo_directory_query->get('nutricash') == 'y') { ?>
                                        <li title="Nutricash" class="nutricash">
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>
                        <?php
                        if ($javo_directory_query->get('din') == 'y' || $javo_directory_query->get('bol') == 'y' || $javo_directory_query->get('dep') == 'y' || $javo_directory_query->get('pagseg') == 'y' || $javo_directory_query->get('pp') == 'y' || $javo_directory_query->get('moip') == 'y') {
                            ?>
                            <div class="col-md-12">
                                <span><?php _e('Outas: ', 'javo_fr'); ?></span>
                                <?php
                                $arrayOutras = array();
                                if ($javo_directory_query->get('din') == 'y') {
                                    array_push($arrayOutras, 'Dinheiro');
                                } if ($javo_directory_query->get('bol') == 'y') {
                                    array_push($arrayOutras, 'Boleto Bancário');
                                } if ($javo_directory_query->get('dep') == 'y') {
                                    array_push($arrayOutras, 'Depósito / Transferência');
                                } if ($javo_directory_query->get('pagseg') == 'y') {
                                    array_push($arrayOutras, 'PagSeguro');
                                } if ($javo_directory_query->get('pp') == 'y') {
                                    array_push($arrayOutras, 'PayPal');
                                } if ($javo_directory_query->get('moip') == 'y') {
                                    array_push($arrayOutras, 'MoIP');
                                }

                                echo implode(', ', $arrayOutras);
                                ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php
    if ($javo_video_query->get('single_position', 'slide') == 'contact') {
        ?>
        <div class="col-md-12"> <!-- video start -->
            <div class="item-single-details-box">
                <h4 class="detail-titles"><?php _e('Video', 'javo_fr'); ?></h4>
                <div class="javo-left-overlay">
                    <div class="javo-txt-meta-area admin-color-setting"><?php _e('Video', 'javo_fr'); ?></div> <!-- javo-txt-meta-area -->
                    <div class="corner-wrap">
                        <div class="corner admin-color-setting"></div>
                        <div class="corner-background admin-color-setting"></div>
                    </div> <!-- corner-wrap -->
                </div> <!-- javo-left-overlay -->
                <!-- <div class="title-box"><?php _e('Description', 'javo_fr'); ?></div> -->
                <div class="inner-items">
                    <?php echo $javo_video_query->get('html'); ?>
                </div> <!-- inner-items -->
            </div> <!-- item-single-details-box -->
        </div> <!-- col-md-12 // video end -->
    <?php }; ?>
    <div class="col-md-12">
        <?php
        $javo_integer = 0;
        $javo_el_childrens = "";
        $javo_custom_field = javo_custom_field::gets();
        if ($javo_custom_field != null) {
            foreach ($javo_custom_field as $field) {
                if ($field['value'] == '')
                    continue;
                $javo_integer++;
                $javo_el_childrens .= sprintf('<li><span>%s</span> %s</li>', $field['label'], $field['value']);
            }; // End Foreach
        }
        if ((int) $javo_integer > 0) {
            ?>
            <div class="item-single-details-box">
                <h4 class="detail-titles"><?php echo $javo_tso->get('field_caption', __('Aditional Information', 'javo_fr')) ?></h4>
                <div class="javo-left-overlay">
                    <div class="javo-txt-meta-area admin-color-setting"><?php echo $javo_tso->get('field_caption', __('Aditional Information', 'javo_fr')) ?></div> <!-- javo-txt-meta-area -->
                    <div class="corner-wrap">
                        <div class="corner admin-color-setting"></div>
                        <div class="corner-background admin-color-setting"></div>
                    </div> <!-- corner-wrap -->
                </div> <!-- javo-left-overlay -->
                <div class="inner-items">
                    <ul><?php echo $javo_el_childrens; ?></ul>
                </div>
            </div>
        <?php }; // End If
        ?>
    </div> <!-- col-md-12 -->
</div> <!-- row -->